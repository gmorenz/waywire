# ! [ feature ( question_mark ) ] # ! [ allow ( unused_assignments ) ]
extern
crate nix ;
pub use nix::Error as NixError;
# [ macro_use ]
extern crate
throw ;
# [ macro_use ]
extern crate cenum ;
use std::os::unix::io::RawFd as fd;
use interfaces:: * ;
use requests:: * ;
pub mod interfaces {
    use nix::Error as NixError;

    use std::os::unix::io::RawFd;
    use {Array, Interface, Fixed, New, interfaces};
    use nix::sys::socket::{send, sendmsg, MsgFlags, ControlMessage};
    use nix::sys::uio::IoVec;
    /// core global object
    ///
    /// The core global object.  This is a special singleton object.  It
    ///  is used for internal Wayland protocol features.
    pub mod display {
#![allow(non_upper_case_globals, non_camel_case_types, unused_imports)]
        use std::os::unix::io::RawFd;
        use {Array, Fixed, New, Interface, interfaces};
        use super::*;

        #[derive(Debug,Clone,Copy,PartialEq,Eq)]
        /// See module documentation
        pub struct Display(pub u32);


        impl Display {
            pub fn interface_name() -> Array {
                Array::from_string("wl_display".into())
            }
            pub fn interface_version() -> u32 {
                1
            }
            pub fn id(self) -> u32 {
                self.0
            }
            pub fn set_id(&mut self, id: u32) {
                self.0 = id
            }
        }
        cenum!{ error u32 {
// server couldn't find object
InvalidObject = 0,
// method doesn't exist on the specified interface
InvalidMethod = 1,
// server is out of memory
NoMemory = 2,
}}

        /// Request: asynchronous roundtrip
        ///
        /// The sync request asks the server to emit the 'done' event
        ///	on the returned wl_callback object.  Since requests are
        ///	handled in-order and events are delivered in-order, this can
        ///	be used as a barrier to ensure all previous requests and the
        ///	resulting events have been handled.
        ///
        ///	The object returned by this request will be destroyed by the
        ///	compositor after the callback is fired and as such the client must not
        ///	attempt to use it after that point.
        ///
        ///	The callback_data passed in the callback is the event serial.
        #[derive(Debug)]
        pub struct Sync {
            pub callback: New<interfaces::Callback>,
        }


        /// Request: get global registry object
        ///
        /// This request creates a registry object that allows the client
        ///	to list and bind the global objects available from the
        ///	compositor.
        #[derive(Debug)]
        pub struct GetRegistry {
            pub registry: New<interfaces::Registry>,
        }


        /// Event: fatal error event
        ///
        /// The error event is sent out when a fatal (non-recoverable)
        ///	error has occurred.  The object_id argument is the object
        ///	where the error occurred, most often in response to a request
        ///	to that object.  The code identifies the error and is defined
        ///	by the object interface.  As such, each interface defines its
        ///	own set of error codes.  The message is an brief description
        ///	of the error, for (debugging) convenience.
        ///
        ///I've changed type of object_id from object to u32 since it
        ///doesn't have an interface. This is temporary until I either
        ///find out it is just a u32, or that I should be treating it
        ///like I do registry.bind and split it into 3 args. Then I can
        ///fix codegen.
        #[derive(Debug)]
        pub struct Error {
            pub object_id: u32,
            pub code: u32,
            pub message: Array, // placeholder for string
        }


        /// Event: acknowledge object ID deletion
        ///
        /// This event is used internally by the object ID management
        ///	logic.  When a client deletes an object, the server will send
        ///	this event to acknowledge that it has seen the delete request.
        ///	When the client receive this event, it will know that it can
        ///	safely reuse the object ID.
        #[derive(Debug)]
        pub struct DeleteId {
            pub id: u32,
        }

    }
    pub use self::display::Display;

    /// global registry object
    ///
    /// The global registry object.  The server has a number of global
    ///  objects that are available to all clients.  These objects
    ///  typically represent an actual object in the server (for example,
    ///  an input device) or they are singleton objects that provide
    ///  extension functionality.
    ///
    ///  When a client creates a registry object, the registry object
    ///  will emit a global event for each global currently in the
    ///  registry.  Globals come and go as a result of device or
    ///  monitor hotplugs, reconfiguration or other events, and the
    ///  registry will send out global and global_remove events to
    ///  keep the client up to date with the changes.  To mark the end
    ///  of the initial burst of events, the client can use the
    ///  wl_display.sync request immediately after calling
    ///  wl_display.get_registry.
    ///
    ///  A client can bind to a global object by using the bind
    ///  request.  This creates a client-side handle that lets the object
    ///  emit events to the client and lets the client invoke requests on
    ///  the object.
    pub mod registry {
#![allow(non_upper_case_globals, non_camel_case_types, unused_imports)]
        use std::os::unix::io::RawFd;
        use {Array, Fixed, New, Interface, interfaces};
        use super::*;

        #[derive(Debug,Clone,Copy,PartialEq,Eq)]
        /// See module documentation
        pub struct Registry(pub u32);


        impl Registry {
            pub fn interface_name() -> Array {
                Array::from_string("wl_registry".into())
            }
            pub fn interface_version() -> u32 {
                1
            }
            pub fn id(self) -> u32 {
                self.0
            }
            pub fn set_id(&mut self, id: u32) {
                self.0 = id
            }
        }

        /// Request: bind an object to the display
        ///
        /// Binds a new, client-created object to the server using the
        ///specified name as the identifier.
        #[derive(Debug)]
        pub struct Bind {
            /// unique name for the object
            pub name: u32,
            pub interface: Array, // placeholder for string
            pub version: u32,
            pub id: New<Interface>,
        }


        /// Event: announce global object
        ///
        /// Notify the client of global objects.
        ///
        ///The event notifies the client that a global object with
        ///the given name is now available, and it implements the
        ///given version of the given interface.
        #[derive(Debug)]
        pub struct Global {
            pub name: u32,
            pub interface: Array, // placeholder for string
            pub version: u32,
        }


        /// Event: announce removal of global object
        ///
        /// Notify the client of removed global objects.
        ///
        ///This event notifies the client that the global identified
        ///by name is no longer available.  If the client bound to
        ///the global using the bind request, the client should now
        ///destroy that object.
        ///
        ///	The object remains valid and requests to the object will be
        ///	ignored until the client destroys it, to avoid races between
        ///	the global going away and a client sending a request to it.
        #[derive(Debug)]
        pub struct GlobalRemove {
            pub name: u32,
        }

    }
    pub use self::registry::Registry;

    /// callback object
    ///
    /// Clients can handle the 'done' event to get notified when
    ///  the related request is done.
    pub mod callback {
#![allow(non_upper_case_globals, non_camel_case_types, unused_imports)]
        use std::os::unix::io::RawFd;
        use {Array, Fixed, New, Interface, interfaces};
        use super::*;

        #[derive(Debug,Clone,Copy,PartialEq,Eq)]
        /// See module documentation
        pub struct Callback(pub u32);


        impl Callback {
            pub fn interface_name() -> Array {
                Array::from_string("wl_callback".into())
            }
            pub fn interface_version() -> u32 {
                1
            }
            pub fn id(self) -> u32 {
                self.0
            }
            pub fn set_id(&mut self, id: u32) {
                self.0 = id
            }
        }

        /// Event: done event
        ///
        /// Notify the client when the related request is done.
        #[derive(Debug)]
        pub struct Done {
            /// request-specific data for the wl_callback
            pub callback_data: u32,
        }

    }
    pub use self::callback::Callback;

    /// the compositor singleton
    ///
    /// A compositor.  This object is a singleton global.  The
    ///  compositor is in charge of combining the contents of multiple
    ///  surfaces into one displayable output.
    pub mod compositor {
#![allow(non_upper_case_globals, non_camel_case_types, unused_imports)]
        use std::os::unix::io::RawFd;
        use {Array, Fixed, New, Interface, interfaces};
        use super::*;

        #[derive(Debug,Clone,Copy,PartialEq,Eq)]
        /// See module documentation
        pub struct Compositor(pub u32);


        impl Compositor {
            pub fn interface_name() -> Array {
                Array::from_string("wl_compositor".into())
            }
            pub fn interface_version() -> u32 {
                4
            }
            pub fn id(self) -> u32 {
                self.0
            }
            pub fn set_id(&mut self, id: u32) {
                self.0 = id
            }
        }

        /// Request: create new surface
        ///
        /// Ask the compositor to create a new surface.
        #[derive(Debug)]
        pub struct CreateSurface {
            pub id: New<interfaces::Surface>,
        }


        /// Request: create new region
        ///
        /// Ask the compositor to create a new region.
        #[derive(Debug)]
        pub struct CreateRegion {
            pub id: New<interfaces::Region>,
        }

    }
    pub use self::compositor::Compositor;

    /// a shared memory pool
    ///
    /// The wl_shm_pool object encapsulates a piece of memory shared
    ///  between the compositor and client.  Through the wl_shm_pool
    ///  object, the client can allocate shared memory wl_buffer objects.
    ///  All objects created through the same pool share the same
    ///  underlying mapped memory. Reusing the mapped memory avoids the
    ///  setup/teardown overhead and is useful when interactively resizing
    ///  a surface or for many small buffers.
    pub mod shm_pool {
#![allow(non_upper_case_globals, non_camel_case_types, unused_imports)]
        use std::os::unix::io::RawFd;
        use {Array, Fixed, New, Interface, interfaces};
        use super::*;

        #[derive(Debug,Clone,Copy,PartialEq,Eq)]
        /// See module documentation
        pub struct ShmPool(pub u32);


        impl ShmPool {
            pub fn interface_name() -> Array {
                Array::from_string("wl_shm_pool".into())
            }
            pub fn interface_version() -> u32 {
                1
            }
            pub fn id(self) -> u32 {
                self.0
            }
            pub fn set_id(&mut self, id: u32) {
                self.0 = id
            }
        }

        /// Request: create a buffer from the pool
        ///
        /// Create a wl_buffer object from the pool.
        ///
        ///	The buffer is created offset bytes into the pool and has
        ///	width and height as specified.  The stride arguments specifies
        ///	the number of bytes from beginning of one row to the beginning
        ///	of the next.  The format is the pixel format of the buffer and
        ///	must be one of those advertised through the wl_shm.format event.
        ///
        ///	A buffer will keep a reference to the pool it was created from
        ///	so it is valid to destroy the pool immediately after creating
        ///	a buffer from it.
        #[derive(Debug)]
        pub struct CreateBuffer {
            pub id: New<interfaces::Buffer>,
            pub offset: i32,
            pub width: i32,
            pub height: i32,
            pub stride: i32,
            pub format: u32,
        }


        /// Request: destroy the pool
        ///
        /// Destroy the shared memory pool.
        ///
        ///	The mmapped memory will be released when all
        ///	buffers that have been created from this pool
        ///	are gone.
        #[derive(Debug)]
        pub struct Destroy;


        /// Request: change the size of the pool mapping
        ///
        /// This request will cause the server to remap the backing memory
        ///	for the pool from the file descriptor passed when the pool was
        ///	created, but using the new size.  This request can only be
        ///	used to make the pool bigger.
        #[derive(Debug)]
        pub struct Resize {
            pub size: i32,
        }

    }
    pub use self::shm_pool::ShmPool;

    /// shared memory support
    ///
    /// A global singleton object that provides support for shared
    ///  memory.
    ///
    ///  Clients can create wl_shm_pool objects using the create_pool
    ///  request.
    ///
    ///  At connection setup time, the wl_shm object emits one or more
    ///  format events to inform clients about the valid pixel formats
    ///  that can be used for buffers.
    pub mod shm {
#![allow(non_upper_case_globals, non_camel_case_types, unused_imports)]
        use std::os::unix::io::RawFd;
        use {Array, Fixed, New, Interface, interfaces};
        use super::*;

        #[derive(Debug,Clone,Copy,PartialEq,Eq)]
        /// See module documentation
        pub struct Shm(pub u32);


        impl Shm {
            pub fn interface_name() -> Array {
                Array::from_string("wl_shm".into())
            }
            pub fn interface_version() -> u32 {
                1
            }
            pub fn id(self) -> u32 {
                self.0
            }
            pub fn set_id(&mut self, id: u32) {
                self.0 = id
            }
        }
        cenum!{ error u32 {
// buffer format is not known
InvalidFormat = 0,
// invalid size or stride during pool or buffer creation
InvalidStride = 1,
// mmapping the file descriptor failed
InvalidFd = 2,
}}
        cenum!{ format u32 {
// 32-bit ARGB format
Argb8888 = 0,
// 32-bit RGB format
Xrgb8888 = 1,
C8 = 538982467,
Rgb332 = 943867730,
Bgr233 = 944916290,
Xrgb4444 = 842093144,
Xbgr4444 = 842089048,
Rgbx4444 = 842094674,
Bgrx4444 = 842094658,
Argb4444 = 842093121,
Abgr4444 = 842089025,
Rgba4444 = 842088786,
Bgra4444 = 842088770,
Xrgb1555 = 892424792,
Xbgr1555 = 892420696,
Rgbx5551 = 892426322,
Bgrx5551 = 892426306,
Argb1555 = 892424769,
Abgr1555 = 892420673,
Rgba5551 = 892420434,
Bgra5551 = 892420418,
Rgb565 = 909199186,
Bgr565 = 909199170,
Rgb888 = 875710290,
Bgr888 = 875710274,
Xbgr8888 = 875709016,
Rgbx8888 = 875714642,
Bgrx8888 = 875714626,
Abgr8888 = 875708993,
Rgba8888 = 875708754,
Bgra8888 = 875708738,
Xrgb2101010 = 808669784,
Xbgr2101010 = 808665688,
Rgbx1010102 = 808671314,
Bgrx1010102 = 808671298,
Argb2101010 = 808669761,
Abgr2101010 = 808665665,
Rgba1010102 = 808665426,
Bgra1010102 = 808665410,
Yuyv = 1448695129,
Yvyu = 1431918169,
Uyvy = 1498831189,
Vyuy = 1498765654,
Ayuv = 1448433985,
Nv12 = 842094158,
Nv21 = 825382478,
Nv16 = 909203022,
Nv61 = 825644622,
Yuv410 = 961959257,
Yvu410 = 961893977,
Yuv411 = 825316697,
Yvu411 = 825316953,
Yuv420 = 842093913,
Yvu420 = 842094169,
Yuv422 = 909202777,
Yvu422 = 909203033,
Yuv444 = 875713881,
Yvu444 = 875714137,
}}

        /// Request: create a shm pool
        ///
        /// Create a new wl_shm_pool object.
        ///
        ///	The pool can be used to create shared memory based buffer
        ///	objects.  The server will mmap size bytes of the passed file
        ///descriptor, to use as backing memory for the pool.
        #[derive(Debug)]
        pub struct CreatePool {
            pub id: New<interfaces::ShmPool>,
            pub fd: RawFd,
            pub size: i32,
        }


        /// Event: pixel format description
        ///
        /// Informs the client about a valid pixel format that
        ///	can be used for buffers. Known formats include
        ///	argb8888 and xrgb8888.
        #[derive(Debug)]
        pub struct Format {
            pub format: ::interfaces::shm::format,
        }

    }
    pub use self::shm::Shm;

    /// content for a wl_surface
    ///
    /// A buffer provides the content for a wl_surface. Buffers are
    ///  created through factory interfaces such as wl_drm, wl_shm or
    ///  similar. It has a width and a height and can be attached to a
    ///  wl_surface, but the mechanism by which a client provides and
    ///  updates the contents is defined by the buffer factory interface.
    pub mod buffer {
#![allow(non_upper_case_globals, non_camel_case_types, unused_imports)]
        use std::os::unix::io::RawFd;
        use {Array, Fixed, New, Interface, interfaces};
        use super::*;

        #[derive(Debug,Clone,Copy,PartialEq,Eq)]
        /// See module documentation
        pub struct Buffer(pub u32);


        impl Buffer {
            pub fn interface_name() -> Array {
                Array::from_string("wl_buffer".into())
            }
            pub fn interface_version() -> u32 {
                1
            }
            pub fn id(self) -> u32 {
                self.0
            }
            pub fn set_id(&mut self, id: u32) {
                self.0 = id
            }
        }

        /// Request: destroy a buffer
        ///
        /// Destroy a buffer. If and how you need to release the backing
        ///	storage is defined by the buffer factory interface.
        ///
        ///	For possible side-effects to a surface, see wl_surface.attach.
        #[derive(Debug)]
        pub struct Destroy;


        /// Event: compositor releases buffer
        ///
        /// Sent when this wl_buffer is no longer used by the compositor.
        ///	The client is now free to re-use or destroy this buffer and its
        ///	backing storage.
        ///
        ///	If a client receives a release event before the frame callback
        ///	requested in the same wl_surface.commit that attaches this
        ///	wl_buffer to a surface, then the client is immediately free to
        ///	re-use the buffer and its backing storage, and does not need a
        ///	second buffer for the next surface content update. Typically
        ///	this is possible, when the compositor maintains a copy of the
        ///	wl_surface contents, e.g. as a GL texture. This is an important
        ///	optimization for GL(ES) compositors with wl_shm clients.
        #[derive(Debug)]
        pub struct Release;

    }
    pub use self::buffer::Buffer;

    /// offer to transfer data
    ///
    /// A wl_data_offer represents a piece of data offered for transfer
    ///  by another client (the source client).  It is used by the
    ///  copy-and-paste and drag-and-drop mechanisms.  The offer
    ///  describes the different mime types that the data can be
    ///  converted to and provides the mechanism for transferring the
    ///  data directly from the source client.
    pub mod data_offer {
#![allow(non_upper_case_globals, non_camel_case_types, unused_imports)]
        use std::os::unix::io::RawFd;
        use {Array, Fixed, New, Interface, interfaces};
        use super::*;

        #[derive(Debug,Clone,Copy,PartialEq,Eq)]
        /// See module documentation
        pub struct DataOffer(pub u32);


        impl DataOffer {
            pub fn interface_name() -> Array {
                Array::from_string("wl_data_offer".into())
            }
            pub fn interface_version() -> u32 {
                3
            }
            pub fn id(self) -> u32 {
                self.0
            }
            pub fn set_id(&mut self, id: u32) {
                self.0 = id
            }
        }
        cenum!{ error u32 {
// finish request was called untimely
InvalidFinish = 0,
// action mask contains invalid values
InvalidActionMask = 1,
// action argument has an invalid value
InvalidAction = 2,
// offer doesn't accept this request
InvalidOffer = 3,
}}

        /// Request: accept one of the offered mime types
        ///
        /// Indicate that the client can accept the given mime type, or
        ///	NULL for not accepted.
        ///
        ///	For objects of version 2 or older, this request is used by the
        ///	client to give feedback whether the client can receive the given
        ///	mime type, or NULL if none is accepted; the feedback does not
        ///	determine whether the drag-and-drop operation succeeds or not.
        ///
        ///	For objects of version 3 or newer, this request determines the
        ///	final result of the drag-and-drop operation. If the end result
        ///	is that no mime types were accepted, the drag-and-drop operation
        ///	will be cancelled and the corresponding drag source will receive
        ///	wl_data_source.cancelled. Clients may still use this event in
        ///	conjunction with wl_data_source.action for feedback.
        #[derive(Debug)]
        pub struct Accept {
            pub serial: u32,
            pub mime_type: Array, // placeholder for string
        }


        /// Request: request that the data is transferred
        ///
        /// To transfer the offered data, the client issues this request
        ///	and indicates the mime type it wants to receive.  The transfer
        ///	happens through the passed file descriptor (typically created
        ///	with the pipe system call).  The source client writes the data
        ///	in the mime type representation requested and then closes the
        ///	file descriptor.
        ///
        ///	The receiving client reads from the read end of the pipe until
        ///	EOF and then closes its end, at which point the transfer is
        ///	complete.
        ///
        ///	This request may happen multiple times for different mimetypes,
        ///	both before and after wl_data_device.drop. Drag-and-drop destination
        ///	clients may preemptively fetch data or examine it more closely to
        ///	determine acceptance.
        #[derive(Debug)]
        pub struct Receive {
            pub mime_type: Array, // placeholder for string
            pub fd: RawFd,
        }


        /// Request: destroy data offer
        ///
        /// Destroy the data offer.
        #[derive(Debug)]
        pub struct Destroy;


        /// Request: the offer will no longer be used
        ///
        /// Notifies the compositor that the drag destination successfully
        ///	finished the drag-and-drop operation.
        ///
        ///	Upon receiving this request, the compositor will emit
        ///	wl_data_source.dnd_finished on the drag source client.
        ///
        ///	It is a client error to perform other requests than
        ///	wl_data_offer.destroy after this one. It is also an error to perform
        ///	this request after a NULL mime type has been set in
        ///	wl_data_offer.accept or no action was received through
        ///	wl_data_offer.action.
        #[derive(Debug)]
        pub struct Finish;


        /// Request: set the available/preferred drag-and-drop actions
        ///
        /// Sets the actions that the destination side client supports for
        ///	this operation. This request may trigger the emission of
        ///	wl_data_source.action and wl_data_offer.action events if the compositor
        ///	need to change the selected action.
        ///
        ///	This request can be called multiple times throughout the
        ///	drag-and-drop operation, typically in response to wl_data_device.enter
        ///	or wl_data_device.motion events.
        ///
        ///	This request determines the final result of the drag-and-drop
        ///	operation. If the end result is that no action is accepted,
        ///	the drag source will receive wl_drag_source.cancelled.
        ///
        ///	The dnd_actions argument must contain only values expressed in the
        ///	wl_data_device_manager.dnd_actions enum, and the preferred_action
        ///	argument must only contain one of those values set, otherwise it
        ///	will result in a protocol error.
        ///
        ///	While managing an "ask" action, the destination drag-and-drop client
        ///	may perform further wl_data_offer.receive requests, and is expected
        ///	to perform one last wl_data_offer.set_actions request with a preferred
        ///	action other than "ask" (and optionally wl_data_offer.accept) before
        ///	requesting wl_data_offer.finish, in order to convey the action selected
        ///	by the user. If the preferred action is not in the
        ///	wl_data_offer.source_actions mask, an error will be raised.
        ///
        ///	If the "ask" action is dismissed (e.g. user cancellation), the client
        ///	is expected to perform wl_data_offer.destroy right away.
        ///
        ///	This request can only be made on drag-and-drop offers, a protocol error
        ///	will be raised otherwise.
        #[derive(Debug)]
        pub struct SetActions {
            pub dnd_actions: u32,
            pub preferred_action: u32,
        }


        /// Event: advertise offered mime type
        ///
        /// Sent immediately after creating the wl_data_offer object.  One
        ///	event per offered mime type.
        #[derive(Debug)]
        pub struct Offer {
            pub mime_type: Array, // placeholder for string
        }


        /// Event: notify the source-side available actions
        ///
        /// This event indicates the actions offered by the data source. It
        ///	will be sent right after wl_data_device.enter, or anytime the source
        ///	side changes its offered actions through wl_data_source.set_actions.
        #[derive(Debug)]
        pub struct SourceActions {
            pub source_actions: u32,
        }


        /// Event: notify the selected action
        ///
        /// This event indicates the action selected by the compositor after
        ///	matching the source/destination side actions. Only one action (or
        ///	none) will be offered here.
        ///
        ///	This event can be emitted multiple times during the drag-and-drop
        ///	operation in response to destination side action changes through
        ///	wl_data_offer.set_actions.
        ///
        ///	This event will no longer be emitted after wl_data_device.drop
        ///	happened on the drag-and-drop destination, the client must
        ///	honor the last action received, or the last preferred one set
        ///	through wl_data_offer.set_actions when handling an "ask" action.
        ///
        ///	Compositors may also change the selected action on the fly, mainly
        ///	in response to keyboard modifier changes during the drag-and-drop
        ///	operation.
        ///
        ///	The most recent action received is always the valid one. Prior to
        ///	receiving wl_data_device.drop, the chosen action may change (e.g.
        ///	due to keyboard modifiers being pressed). At the time of receiving
        ///	wl_data_device.drop the drag-and-drop destination must honor the
        ///	last action received.
        ///
        ///	Action changes may still happen after wl_data_device.drop,
        ///	especially on "ask" actions, where the drag-and-drop destination
        ///	may choose another action afterwards. Action changes happening
        ///	at this stage are always the result of inter-client negotiation, the
        ///	compositor shall no longer be able to induce a different action.
        ///
        ///	Upon "ask" actions, it is expected that the drag-and-drop destination
        ///	may potentially choose different a different action and/or mime type,
        ///	based on wl_data_offer.source_actions and finally chosen by the
        ///	user (e.g. popping up a menu with the available options). The
        ///	final wl_data_offer.set_actions and wl_data_offer.accept requests
        ///	must happen before the call to wl_data_offer.finish.
        #[derive(Debug)]
        pub struct Action {
            pub dnd_action: u32,
        }

    }
    pub use self::data_offer::DataOffer;

    /// offer to transfer data
    ///
    /// The wl_data_source object is the source side of a wl_data_offer.
    ///  It is created by the source client in a data transfer and
    ///  provides a way to describe the offered data and a way to respond
    ///  to requests to transfer the data.
    pub mod data_source {
#![allow(non_upper_case_globals, non_camel_case_types, unused_imports)]
        use std::os::unix::io::RawFd;
        use {Array, Fixed, New, Interface, interfaces};
        use super::*;

        #[derive(Debug,Clone,Copy,PartialEq,Eq)]
        /// See module documentation
        pub struct DataSource(pub u32);


        impl DataSource {
            pub fn interface_name() -> Array {
                Array::from_string("wl_data_source".into())
            }
            pub fn interface_version() -> u32 {
                3
            }
            pub fn id(self) -> u32 {
                self.0
            }
            pub fn set_id(&mut self, id: u32) {
                self.0 = id
            }
        }
        cenum!{ error u32 {
// action mask contains invalid values
InvalidActionMask = 0,
// source doesn't accept this request
InvalidSource = 1,
}}

        /// Request: add an offered mime type
        ///
        /// This request adds a mime type to the set of mime types
        ///	advertised to targets.  Can be called several times to offer
        ///	multiple types.
        #[derive(Debug)]
        pub struct Offer {
            pub mime_type: Array, // placeholder for string
        }


        /// Request: destroy the data source
        ///
        /// Destroy the data source.
        #[derive(Debug)]
        pub struct Destroy;


        /// Request: set the available drag-and-drop actions
        ///
        /// Sets the actions that the source side client supports for this
        ///	operation. This request may trigger wl_data_source.action and
        ///	wl_data_offer.action events if the compositor needs to change the
        ///	selected action.
        ///
        ///	The dnd_actions argument must contain only values expressed in the
        ///	wl_data_device_manager.dnd_actions enum, otherwise it will result
        ///	in a protocol error.
        ///
        ///	This request must be made once only, and can only be made on sources
        ///	used in drag-and-drop, so it must be performed before
        ///	wl_data_device.start_drag. Attempting to use the source other than
        ///	for drag-and-drop will raise a protocol error.
        #[derive(Debug)]
        pub struct SetActions {
            pub dnd_actions: u32,
        }


        /// Event: a target accepts an offered mime type
        ///
        /// Sent when a target accepts pointer_focus or motion events.  If
        ///	a target does not accept any of the offered types, type is NULL.
        ///
        ///	Used for feedback during drag-and-drop.
        #[derive(Debug)]
        pub struct Target {
            pub mime_type: Array, // placeholder for string
        }


        /// Event: send the data
        ///
        /// Request for data from the client.  Send the data as the
        ///	specified mime type over the passed file descriptor, then
        ///	close it.
        #[derive(Debug)]
        pub struct Send {
            pub mime_type: Array, // placeholder for string
            pub fd: RawFd,
        }


        /// Event: selection was cancelled
        ///
        /// This data source is no longer valid. There are several reasons why
        ///	this could happen:
        ///
        ///	- The data source has been replaced by another data source.
        ///	- The drag-and-drop operation was performed, but the drop destination
        ///	  did not accept any of the mimetypes offered through
        ///	  wl_data_source.target.
        ///	- The drag-and-drop operation was performed, but the drop destination
        ///	  did not select any of the actions present in the mask offered through
        ///	  wl_data_source.action.
        ///	- The drag-and-drop operation was performed but didn't happen over a
        ///	  surface.
        ///	- The compositor cancelled the drag-and-drop operation (e.g. compositor
        ///	  dependent timeouts to avoid stale drag-and-drop transfers).
        ///
        ///	The client should clean up and destroy this data source.
        ///
        ///	For objects of version 2 or older, wl_data_source.cancelled will
        ///	only be emitted if the data source was replaced by another data
        ///	source.
        #[derive(Debug)]
        pub struct Cancelled;


        /// Event: the drag-and-drop operation physically finished
        ///
        /// The user performed the drop action. This event does not indicate
        ///	acceptance, wl_data_source.cancelled may still be emitted afterwards
        ///	if the drop destination does not accept any mimetype.
        ///
        ///	However, this event might however not be received if the compositor
        ///	cancelled the drag-and-drop operation before this event could happen.
        ///
        ///	Note that the data_source may still be used in the future and should
        ///	not be destroyed here.
        #[derive(Debug)]
        pub struct DndDropPerformed;


        /// Event: the drag-and-drop operation concluded
        ///
        /// The drop destination finished interoperating with this data
        ///	source, so the client is now free to destroy this data source and
        ///	free all associated data.
        ///
        ///	If the action used to perform the operation was "move", the
        ///	source can now delete the transferred data.
        #[derive(Debug)]
        pub struct DndFinished;


        /// Event: notify the selected action
        ///
        /// This event indicates the action selected by the compositor after
        ///	matching the source/destination side actions. Only one action (or
        ///	none) will be offered here.
        ///
        ///	This event can be emitted multiple times during the drag-and-drop
        ///	operation, mainly in response to destination side changes through
        ///	wl_data_offer.set_actions, and as the data device enters/leaves
        ///	surfaces.
        ///
        ///	It is only possible to receive this event after
        ///	wl_data_source.dnd_drop_performed if the drag-and-drop operation
        ///	ended in an "ask" action, in which case the final wl_data_source.action
        ///	event will happen immediately before wl_data_source.dnd_finished.
        ///
        ///	Compositors may also change the selected action on the fly, mainly
        ///	in response to keyboard modifier changes during the drag-and-drop
        ///	operation.
        ///
        ///	The most recent action received is always the valid one. The chosen
        ///	action may change alongside negotiation (e.g. an "ask" action can turn
        ///	into a "move" operation), so the effects of the final action must
        ///	always be applied in wl_data_offer.dnd_finished.
        ///
        ///	Clients can trigger cursor surface changes from this point, so
        ///	they reflect the current action.
        #[derive(Debug)]
        pub struct Action {
            pub dnd_action: u32,
        }

    }
    pub use self::data_source::DataSource;

    /// data transfer device
    ///
    /// There is one wl_data_device per seat which can be obtained
    ///  from the global wl_data_device_manager singleton.
    ///
    ///  A wl_data_device provides access to inter-client data transfer
    ///  mechanisms such as copy-and-paste and drag-and-drop.
    pub mod data_device {
#![allow(non_upper_case_globals, non_camel_case_types, unused_imports)]
        use std::os::unix::io::RawFd;
        use {Array, Fixed, New, Interface, interfaces};
        use super::*;

        #[derive(Debug,Clone,Copy,PartialEq,Eq)]
        /// See module documentation
        pub struct DataDevice(pub u32);


        impl DataDevice {
            pub fn interface_name() -> Array {
                Array::from_string("wl_data_device".into())
            }
            pub fn interface_version() -> u32 {
                3
            }
            pub fn id(self) -> u32 {
                self.0
            }
            pub fn set_id(&mut self, id: u32) {
                self.0 = id
            }
        }
        cenum!{ error u32 {
// given wl_surface has another role
Role = 0,
}}

        /// Request: start drag-and-drop operation
        ///
        /// This request asks the compositor to start a drag-and-drop
        ///	operation on behalf of the client.
        ///
        ///	The source argument is the data source that provides the data
        ///	for the eventual data transfer. If source is NULL, enter, leave
        ///	and motion events are sent only to the client that initiated the
        ///	drag and the client is expected to handle the data passing
        ///	internally.
        ///
        ///	The origin surface is the surface where the drag originates and
        ///	the client must have an active implicit grab that matches the
        ///	serial.
        ///
        ///	The icon surface is an optional (can be NULL) surface that
        ///	provides an icon to be moved around with the cursor.  Initially,
        ///	the top-left corner of the icon surface is placed at the cursor
        ///	hotspot, but subsequent wl_surface.attach request can move the
        ///	relative position. Attach requests must be confirmed with
        ///	wl_surface.commit as usual. The icon surface is given the role of
        ///	a drag-and-drop icon. If the icon surface already has another role,
        ///	it raises a protocol error.
        ///
        ///	The current and pending input regions of the icon wl_surface are
        ///	cleared, and wl_surface.set_input_region is ignored until the
        ///	wl_surface is no longer used as the icon surface. When the use
        ///	as an icon ends, the current and pending input regions become
        ///	undefined, and the wl_surface is unmapped.
        #[derive(Debug)]
        pub struct StartDrag {
            pub source: interfaces::DataSource,
            pub origin: interfaces::Surface,
            pub icon: interfaces::Surface,
            /// serial of the implicit grab on the origin
            pub serial: u32,
        }


        /// Request: copy data to the selection
        ///
        /// This request asks the compositor to set the selection
        ///	to the data from the source on behalf of the client.
        ///
        ///	To unset the selection, set the source to NULL.
        #[derive(Debug)]
        pub struct SetSelection {
            pub source: interfaces::DataSource,
            /// serial of the event that triggered this request
            pub serial: u32,
        }


        /// Request: destroy data device
        ///
        /// This request destroys the data device.
        #[derive(Debug)]
        pub struct Release;


        /// Event: introduce a new wl_data_offer
        ///
        /// The data_offer event introduces a new wl_data_offer object,
        ///	which will subsequently be used in either the
        ///	data_device.enter event (for drag-and-drop) or the
        ///	data_device.selection event (for selections).  Immediately
        ///	following the data_device_data_offer event, the new data_offer
        ///	object will send out data_offer.offer events to describe the
        ///	mime types it offers.
        #[derive(Debug)]
        pub struct DataOffer {
            pub id: New<interfaces::DataOffer>,
        }


        /// Event: initiate drag-and-drop session
        ///
        /// This event is sent when an active drag-and-drop pointer enters
        ///	a surface owned by the client.  The position of the pointer at
        ///	enter time is provided by the x and y arguments, in surface
        ///	local coordinates.
        #[derive(Debug)]
        pub struct Enter {
            pub serial: u32,
            pub surface: interfaces::Surface,
            pub x: Fixed,
            pub y: Fixed,
            pub id: interfaces::DataOffer,
        }


        /// Event: end drag-and-drop session
        ///
        /// This event is sent when the drag-and-drop pointer leaves the
        ///	surface and the session ends.  The client must destroy the
        ///	wl_data_offer introduced at enter time at this point.
        #[derive(Debug)]
        pub struct Leave;


        /// Event: drag-and-drop session motion
        ///
        /// This event is sent when the drag-and-drop pointer moves within
        ///	the currently focused surface. The new position of the pointer
        ///	is provided by the x and y arguments, in surface local
        ///	coordinates.
        #[derive(Debug)]
        pub struct Motion {
            /// timestamp with millisecond granularity
            pub time: u32,
            pub x: Fixed,
            pub y: Fixed,
        }


        /// Event: end drag-and-drag session successfully
        ///
        /// The event is sent when a drag-and-drop operation is ended
        ///	because the implicit grab is removed.
        ///
        ///	The drag-and-drop destination is expected to honor the last action
        ///	received through wl_data_offer.action, if the resulting action is
        ///	"copy" or "move", the destination can still perform
        ///	wl_data_offer.receive requests, and is expected to end all
        ///	transfers with a wl_data_offer.finish request.
        ///
        ///	If the resulting action is "ask", the action will not be considered
        ///	final. The drag-and-drop destination is expected to perform one last
        ///	wl_data_offer.set_actions request, or wl_data_offer.destroy in order
        ///	to cancel the operation.
        #[derive(Debug)]
        pub struct Drop;


        /// Event: advertise new selection
        ///
        /// The selection event is sent out to notify the client of a new
        ///	wl_data_offer for the selection for this device.  The
        ///	data_device.data_offer and the data_offer.offer events are
        ///	sent out immediately before this event to introduce the data
        ///	offer object.  The selection event is sent to a client
        ///	immediately before receiving keyboard focus and when a new
        ///	selection is set while the client has keyboard focus.  The
        ///	data_offer is valid until a new data_offer or NULL is received
        ///	or until the client loses keyboard focus.  The client must
        ///	destroy the previous selection data_offer, if any, upon receiving
        ///	this event.
        #[derive(Debug)]
        pub struct Selection {
            pub id: interfaces::DataOffer,
        }

    }
    pub use self::data_device::DataDevice;

    /// data transfer interface
    ///
    /// The wl_data_device_manager is a singleton global object that
    ///  provides access to inter-client data transfer mechanisms such as
    ///  copy-and-paste and drag-and-drop.  These mechanisms are tied to
    ///  a wl_seat and this interface lets a client get a wl_data_device
    ///  corresponding to a wl_seat.
    ///
    ///  Depending on the version bound, the objects created from the bound
    ///  wl_data_device_manager object will have different requirements for
    ///  functioning properly. See wl_data_source.set_actions,
    ///  wl_data_offer.accept and wl_data_offer.finish for details.
    pub mod data_device_manager {
#![allow(non_upper_case_globals, non_camel_case_types, unused_imports)]
        use std::os::unix::io::RawFd;
        use {Array, Fixed, New, Interface, interfaces};
        use super::*;

        #[derive(Debug,Clone,Copy,PartialEq,Eq)]
        /// See module documentation
        pub struct DataDeviceManager(pub u32);


        impl DataDeviceManager {
            pub fn interface_name() -> Array {
                Array::from_string("wl_data_device_manager".into())
            }
            pub fn interface_version() -> u32 {
                3
            }
            pub fn id(self) -> u32 {
                self.0
            }
            pub fn set_id(&mut self, id: u32) {
                self.0 = id
            }
        }
        cenum!{ dnd_action u32 {
None = 0,
Copy = 1,
Move = 2,
Ask = 4,
}}
        impl ::std::ops::BitAnd for dnd_action {
            type Output = dnd_action;
            fn bitand(self, rhs: dnd_action) -> dnd_action {
                dnd_action(self.0 & rhs.0)
            }
        }
        impl ::std::ops::BitOr for dnd_action {
            type Output = dnd_action;
            fn bitor(self, rhs: dnd_action) -> dnd_action {
                dnd_action(self.0 | rhs.0)
            }
        }
        impl ::std::ops::BitXor for dnd_action {
            type Output = dnd_action;
            fn bitxor(self, rhs: dnd_action) -> dnd_action {
                dnd_action(self.0 ^ rhs.0)
            }
        }
        /// Request: create a new data source
        ///
        /// Create a new data source.
        #[derive(Debug)]
        pub struct CreateDataSource {
            pub id: New<interfaces::DataSource>,
        }


        /// Request: create a new data device
        ///
        /// Create a new data device for a given seat.
        #[derive(Debug)]
        pub struct GetDataDevice {
            pub id: New<interfaces::DataDevice>,
            pub seat: interfaces::Seat,
        }

    }
    pub use self::data_device_manager::DataDeviceManager;

    /// create desktop-style surfaces
    ///
    /// This interface is implemented by servers that provide
    ///  desktop-style user interfaces.
    ///
    ///  It allows clients to associate a wl_shell_surface with
    ///  a basic surface.
    pub mod shell {
#![allow(non_upper_case_globals, non_camel_case_types, unused_imports)]
        use std::os::unix::io::RawFd;
        use {Array, Fixed, New, Interface, interfaces};
        use super::*;

        #[derive(Debug,Clone,Copy,PartialEq,Eq)]
        /// See module documentation
        pub struct Shell(pub u32);


        impl Shell {
            pub fn interface_name() -> Array {
                Array::from_string("wl_shell".into())
            }
            pub fn interface_version() -> u32 {
                1
            }
            pub fn id(self) -> u32 {
                self.0
            }
            pub fn set_id(&mut self, id: u32) {
                self.0 = id
            }
        }
        cenum!{ error u32 {
// given wl_surface has another role
Role = 0,
}}

        /// Request: create a shell surface from a surface
        ///
        /// Create a shell surface for an existing surface. This gives
        ///	the wl_surface the role of a shell surface. If the wl_surface
        ///	already has another role, it raises a protocol error.
        ///
        ///	Only one shell surface can be associated with a given surface.
        #[derive(Debug)]
        pub struct GetShellSurface {
            pub id: New<interfaces::ShellSurface>,
            pub surface: interfaces::Surface,
        }

    }
    pub use self::shell::Shell;

    /// desktop-style metadata interface
    ///
    /// An interface that may be implemented by a wl_surface, for
    ///  implementations that provide a desktop-style user interface.
    ///
    ///  It provides requests to treat surfaces like toplevel, fullscreen
    ///  or popup windows, move, resize or maximize them, associate
    ///  metadata like title and class, etc.
    ///
    ///  On the server side the object is automatically destroyed when
    ///  the related wl_surface is destroyed.  On client side,
    ///  wl_shell_surface_destroy() must be called before destroying
    ///  the wl_surface object.
    pub mod shell_surface {
#![allow(non_upper_case_globals, non_camel_case_types, unused_imports)]
        use std::os::unix::io::RawFd;
        use {Array, Fixed, New, Interface, interfaces};
        use super::*;

        #[derive(Debug,Clone,Copy,PartialEq,Eq)]
        /// See module documentation
        pub struct ShellSurface(pub u32);


        impl ShellSurface {
            pub fn interface_name() -> Array {
                Array::from_string("wl_shell_surface".into())
            }
            pub fn interface_version() -> u32 {
                1
            }
            pub fn id(self) -> u32 {
                self.0
            }
            pub fn set_id(&mut self, id: u32) {
                self.0 = id
            }
        }
        cenum!{ resize u32 {
None = 0,
Top = 1,
Bottom = 2,
Left = 4,
TopLeft = 5,
BottomLeft = 6,
Right = 8,
TopRight = 9,
BottomRight = 10,
}}
        impl ::std::ops::BitAnd for resize {
            type Output = resize;
            fn bitand(self, rhs: resize) -> resize {
                resize(self.0 & rhs.0)
            }
        }
        impl ::std::ops::BitOr for resize {
            type Output = resize;
            fn bitor(self, rhs: resize) -> resize {
                resize(self.0 | rhs.0)
            }
        }
        impl ::std::ops::BitXor for resize {
            type Output = resize;
            fn bitxor(self, rhs: resize) -> resize {
                resize(self.0 ^ rhs.0)
            }
        }
        cenum!{ transient u32 {
// do not set keyboard focus
Inactive = 1,
}}
        impl ::std::ops::BitAnd for transient {
            type Output = transient;
            fn bitand(self, rhs: transient) -> transient {
                transient(self.0 & rhs.0)
            }
        }
        impl ::std::ops::BitOr for transient {
            type Output = transient;
            fn bitor(self, rhs: transient) -> transient {
                transient(self.0 | rhs.0)
            }
        }
        impl ::std::ops::BitXor for transient {
            type Output = transient;
            fn bitxor(self, rhs: transient) -> transient {
                transient(self.0 ^ rhs.0)
            }
        }
        cenum!{ fullscreen_method u32 {
        // no preference, apply default policy
Default = 0,
        // scale, preserve the surface's aspect ratio and center on output
Scale = 1,
        // switch output mode to the smallest mode that can fit the surface, add black borders to compensate size mismatch
Driver = 2,
        // no upscaling, center on output and add black borders to compensate size mismatch
Fill = 3,
}}

        /// Request: respond to a ping event
        ///
        /// A client must respond to a ping event with a pong request or
        ///	the client may be deemed unresponsive.
        #[derive(Debug)]
        pub struct Pong {
            /// serial of the ping event
            pub serial: u32,
        }


        /// Request: start an interactive move
        ///
        /// Start a pointer-driven move of the surface.
        ///
        ///	This request must be used in response to a button press event.
        ///	The server may ignore move requests depending on the state of
        ///	the surface (e.g. fullscreen or maximized).
        #[derive(Debug)]
        pub struct Move {
            /// the wl_seat whose pointer is used
            pub seat: interfaces::Seat,
            /// serial of the implicit grab on the pointer
            pub serial: u32,
        }


        /// Request: start an interactive resize
        ///
        /// Start a pointer-driven resizing of the surface.
        ///
        ///	This request must be used in response to a button press event.
        ///	The server may ignore resize requests depending on the state of
        ///	the surface (e.g. fullscreen or maximized).
        #[derive(Debug)]
        pub struct Resize {
            /// the wl_seat whose pointer is used
            pub seat: interfaces::Seat,
            /// serial of the implicit grab on the pointer
            pub serial: u32,
            /// which edge or corner is being dragged
            pub edges: ::interfaces::shell_surface::resize,
        }


        /// Request: make the surface a toplevel surface
        ///
        /// Map the surface as a toplevel surface.
        ///
        ///	A toplevel surface is not fullscreen, maximized or transient.
        #[derive(Debug)]
        pub struct SetToplevel;


        /// Request: make the surface a transient surface
        ///
        /// Map the surface relative to an existing surface.
        ///
        ///	The x and y arguments specify the locations of the upper left
        ///	corner of the surface relative to the upper left corner of the
        ///	parent surface, in surface local coordinates.
        ///
        ///	The flags argument controls details of the transient behaviour.
        #[derive(Debug)]
        pub struct SetTransient {
            pub parent: interfaces::Surface,
            pub x: i32,
            pub y: i32,
            pub flags: ::interfaces::shell_surface::transient,
        }


        /// Request: make the surface a fullscreen surface
        ///
        /// Map the surface as a fullscreen surface.
        ///
        ///	If an output parameter is given then the surface will be made
        ///	fullscreen on that output. If the client does not specify the
        ///	output then the compositor will apply its policy - usually
        ///	choosing the output on which the surface has the biggest surface
        ///	area.
        ///
        ///	The client may specify a method to resolve a size conflict
        ///	between the output size and the surface size - this is provided
        ///	through the method parameter.
        ///
        ///	The framerate parameter is used only when the method is set
        ///	to "driver", to indicate the preferred framerate. A value of 0
        ///	indicates that the app does not care about framerate.  The
        ///	framerate is specified in mHz, that is framerate of 60000 is 60Hz.
        ///
        ///	A method of "scale" or "driver" implies a scaling operation of
        ///	the surface, either via a direct scaling operation or a change of
        ///	the output mode. This will override any kind of output scaling, so
        ///	that mapping a surface with a buffer size equal to the mode can
        ///	fill the screen independent of buffer_scale.
        ///
        ///	A method of "fill" means we don't scale up the buffer, however
        ///	any output scale is applied. This means that you may run into
        ///	an edge case where the application maps a buffer with the same
        ///	size of the output mode but buffer_scale 1 (thus making a
        ///	surface larger than the output). In this case it is allowed to
        ///	downscale the results to fit the screen.
        ///
        ///	The compositor must reply to this request with a configure event
        ///	with the dimensions for the output on which the surface will
        ///	be made fullscreen.
        #[derive(Debug)]
        pub struct SetFullscreen {
            pub method: ::interfaces::shell_surface::fullscreen_method,
            pub framerate: u32,
            pub output: interfaces::Output,
        }


        /// Request: make the surface a popup surface
        ///
        /// Map the surface as a popup.
        ///
        ///	A popup surface is a transient surface with an added pointer
        ///	grab.
        ///
        ///	An existing implicit grab will be changed to owner-events mode,
        ///	and the popup grab will continue after the implicit grab ends
        ///	(i.e. releasing the mouse button does not cause the popup to
        ///	be unmapped).
        ///
        ///	The popup grab continues until the window is destroyed or a
        ///	mouse button is pressed in any other clients window. A click
        ///	in any of the clients surfaces is reported as normal, however,
        ///	clicks in other clients surfaces will be discarded and trigger
        ///	the callback.
        ///
        ///	The x and y arguments specify the locations of the upper left
        ///	corner of the surface relative to the upper left corner of the
        ///	parent surface, in surface local coordinates.
        #[derive(Debug)]
        pub struct SetPopup {
            /// the wl_seat whose pointer is used
            pub seat: interfaces::Seat,
            /// serial of the implicit grab on the pointer
            pub serial: u32,
            pub parent: interfaces::Surface,
            pub x: i32,
            pub y: i32,
            pub flags: ::interfaces::shell_surface::transient,
        }


        /// Request: make the surface a maximized surface
        ///
        /// Map the surface as a maximized surface.
        ///
        ///	If an output parameter is given then the surface will be
        ///	maximized on that output. If the client does not specify the
        ///	output then the compositor will apply its policy - usually
        ///	choosing the output on which the surface has the biggest surface
        ///	area.
        ///
        ///	The compositor will reply with a configure event telling
        ///	the expected new surface size. The operation is completed
        ///	on the next buffer attach to this surface.
        ///
        ///	A maximized surface typically fills the entire output it is
        ///	bound to, except for desktop element such as panels. This is
        ///	the main difference between a maximized shell surface and a
        ///	fullscreen shell surface.
        ///
        ///	The details depend on the compositor implementation.
        #[derive(Debug)]
        pub struct SetMaximized {
            pub output: interfaces::Output,
        }


        /// Request: set surface title
        ///
        /// Set a short title for the surface.
        ///
        ///	This string may be used to identify the surface in a task bar,
        ///	window list, or other user interface elements provided by the
        ///	compositor.
        ///
        ///	The string must be encoded in UTF-8.
        #[derive(Debug)]
        pub struct SetTitle {
            pub title: Array, // placeholder for string
        }


        /// Request: set surface class
        ///
        /// Set a class for the surface.
        ///
        ///	The surface class identifies the general class of applications
        ///	to which the surface belongs. A common convention is to use the
        ///	file name (or the full path if it is a non-standard location) of
        ///	the application's .desktop file as the class.
        #[derive(Debug)]
        pub struct SetClass {
            pub class_: Array, // placeholder for string
        }


        /// Event: ping client
        ///
        /// Ping a client to check if it is receiving events and sending
        ///	requests. A client is expected to reply with a pong request.
        #[derive(Debug)]
        pub struct Ping {
            pub serial: u32,
        }


        /// Event: suggest resize
        ///
        /// The configure event asks the client to resize its surface.
        ///
        ///	The size is a hint, in the sense that the client is free to
        ///	ignore it if it doesn't resize, pick a smaller size (to
        ///	satisfy aspect ratio or resize in steps of NxM pixels).
        ///
        ///	The edges parameter provides a hint about how the surface
        ///	was resized. The client may use this information to decide
        ///	how to adjust its content to the new size (e.g. a scrolling
        ///	area might adjust its content position to leave the viewable
        ///	content unmoved).
        ///
        ///	The client is free to dismiss all but the last configure
        ///	event it received.
        ///
        ///	The width and height arguments specify the size of the window
        ///	in surface local coordinates.
        #[derive(Debug)]
        pub struct Configure {
            pub edges: ::interfaces::shell_surface::resize,
            pub width: i32,
            pub height: i32,
        }


        /// Event: popup interaction is done
        ///
        /// The popup_done event is sent out when a popup grab is broken,
        ///	that is, when the user clicks a surface that doesn't belong
        ///	to the client owning the popup surface.
        #[derive(Debug)]
        pub struct PopupDone;

    }
    pub use self::shell_surface::ShellSurface;

    /// an onscreen surface
    ///
    /// A surface is a rectangular area that is displayed on the screen.
    ///  It has a location, size and pixel contents.
    ///
    ///  The size of a surface (and relative positions on it) is described
    ///  in surface local coordinates, which may differ from the buffer
    ///  local coordinates of the pixel content, in case a buffer_transform
    ///  or a buffer_scale is used.
    ///
    ///  A surface without a "role" is fairly useless, a compositor does
    ///  not know where, when or how to present it. The role is the
    ///  purpose of a wl_surface. Examples of roles are a cursor for a
    ///  pointer (as set by wl_pointer.set_cursor), a drag icon
    ///  (wl_data_device.start_drag), a sub-surface
    ///  (wl_subcompositor.get_subsurface), and a window as defined by a
    ///  shell protocol (e.g. wl_shell.get_shell_surface).
    ///
    ///  A surface can have only one role at a time. Initially a
    ///  wl_surface does not have a role. Once a wl_surface is given a
    ///  role, it is set permanently for the whole lifetime of the
    ///  wl_surface object. Giving the current role again is allowed,
    ///  unless explicitly forbidden by the relevant interface
    ///  specification.
    ///
    ///  Surface roles are given by requests in other interfaces such as
    ///  wl_pointer.set_cursor. The request should explicitly mention
    ///  that this request gives a role to a wl_surface. Often, this
    ///  request also creates a new protocol object that represents the
    ///  role and adds additional functionality to wl_surface. When a
    ///  client wants to destroy a wl_surface, they must destroy this 'role
    ///  object' before the wl_surface.
    ///
    ///  Destroying the role object does not remove the role from the
    ///  wl_surface, but it may stop the wl_surface from "playing the role".
    ///  For instance, if a wl_subsurface object is destroyed, the wl_surface
    ///  it was created for will be unmapped and forget its position and
    ///  z-order. It is allowed to create a wl_subsurface for the same
    ///  wl_surface again, but it is not allowed to use the wl_surface as
    ///  a cursor (cursor is a different role than sub-surface, and role
    ///  switching is not allowed).
    pub mod surface {
#![allow(non_upper_case_globals, non_camel_case_types, unused_imports)]
        use std::os::unix::io::RawFd;
        use {Array, Fixed, New, Interface, interfaces};
        use super::*;

        #[derive(Debug,Clone,Copy,PartialEq,Eq)]
        /// See module documentation
        pub struct Surface(pub u32);


        impl Surface {
            pub fn interface_name() -> Array {
                Array::from_string("wl_surface".into())
            }
            pub fn interface_version() -> u32 {
                4
            }
            pub fn id(self) -> u32 {
                self.0
            }
            pub fn set_id(&mut self, id: u32) {
                self.0 = id
            }
        }
        cenum!{ error u32 {
// buffer scale value is invalid
InvalidScale = 0,
// buffer transform value is invalid
InvalidTransform = 1,
}}

        /// Request: delete surface
        ///
        /// Deletes the surface and invalidates its object ID.
        #[derive(Debug)]
        pub struct Destroy;


        /// Request: set the surface contents
        ///
        /// Set a buffer as the content of this surface.
        ///
        ///	The new size of the surface is calculated based on the buffer
        ///	size transformed by the inverse buffer_transform and the
        ///	inverse buffer_scale. This means that the supplied buffer
        ///	must be an integer multiple of the buffer_scale.
        ///
        ///	The x and y arguments specify the location of the new pending
        ///	buffer's upper left corner, relative to the current buffer's upper
        ///	left corner, in surface local coordinates. In other words, the
        ///	x and y, combined with the new surface size define in which
        ///	directions the surface's size changes.
        ///
        ///	Surface contents are double-buffered state, see wl_surface.commit.
        ///
        ///	The initial surface contents are void; there is no content.
        ///	wl_surface.attach assigns the given wl_buffer as the pending
        ///	wl_buffer. wl_surface.commit makes the pending wl_buffer the new
        ///	surface contents, and the size of the surface becomes the size
        ///	calculated from the wl_buffer, as described above. After commit,
        ///	there is no pending buffer until the next attach.
        ///
        ///	Committing a pending wl_buffer allows the compositor to read the
        ///	pixels in the wl_buffer. The compositor may access the pixels at
        ///	any time after the wl_surface.commit request. When the compositor
        ///	will not access the pixels anymore, it will send the
        ///	wl_buffer.release event. Only after receiving wl_buffer.release,
        ///	the client may re-use the wl_buffer. A wl_buffer that has been
        ///	attached and then replaced by another attach instead of committed
        ///	will not receive a release event, and is not used by the
        ///	compositor.
        ///
        ///	Destroying the wl_buffer after wl_buffer.release does not change
        ///	the surface contents. However, if the client destroys the
        ///	wl_buffer before receiving the wl_buffer.release event, the surface
        ///	contents become undefined immediately.
        ///
        ///	If wl_surface.attach is sent with a NULL wl_buffer, the
        ///	following wl_surface.commit will remove the surface content.
        #[derive(Debug)]
        pub struct Attach {
            pub buffer: interfaces::Buffer,
            pub x: i32,
            pub y: i32,
        }


        /// Request: mark part of the surface damaged
        ///
        /// This request is used to describe the regions where the pending
        ///	buffer is different from the current surface contents, and where
        ///	the surface therefore needs to be repainted. The compositor
        ///	ignores the parts of the damage that fall outside of the surface.
        ///
        ///	Damage is double-buffered state, see wl_surface.commit.
        ///
        ///	The damage rectangle is specified in surface local coordinates.
        ///
        ///	The initial value for pending damage is empty: no damage.
        ///	wl_surface.damage adds pending damage: the new pending damage
        ///	is the union of old pending damage and the given rectangle.
        ///
        ///	wl_surface.commit assigns pending damage as the current damage,
        ///	and clears pending damage. The server will clear the current
        ///	damage as it repaints the surface.
        ///
        ///	Alternatively, damage can be posted with wl_surface.damage_buffer
        ///	which uses buffer co-ordinates instead of surface co-ordinates,
        ///	and is probably the preferred and intuitive way of doing this.
        #[derive(Debug)]
        pub struct Damage {
            pub x: i32,
            pub y: i32,
            pub width: i32,
            pub height: i32,
        }


        /// Request: request a frame throttling hint
        ///
        /// Request a notification when it is a good time start drawing a new
        ///	frame, by creating a frame callback. This is useful for throttling
        ///	redrawing operations, and driving animations.
        ///
        ///	When a client is animating on a wl_surface, it can use the 'frame'
        ///	request to get notified when it is a good time to draw and commit the
        ///	next frame of animation. If the client commits an update earlier than
        ///	that, it is likely that some updates will not make it to the display,
        ///	and the client is wasting resources by drawing too often.
        ///
        ///	The frame request will take effect on the next wl_surface.commit.
        ///	The notification will only be posted for one frame unless
        ///	requested again. For a wl_surface, the notifications are posted in
        ///	the order the frame requests were committed.
        ///
        ///	The server must send the notifications so that a client
        ///	will not send excessive updates, while still allowing
        ///	the highest possible update rate for clients that wait for the reply
        ///	before drawing again. The server should give some time for the client
        ///	to draw and commit after sending the frame callback events to let them
        ///	hit the next output refresh.
        ///
        ///	A server should avoid signalling the frame callbacks if the
        ///	surface is not visible in any way, e.g. the surface is off-screen,
        ///	or completely obscured by other opaque surfaces.
        ///
        ///	The object returned by this request will be destroyed by the
        ///	compositor after the callback is fired and as such the client must not
        ///	attempt to use it after that point.
        ///
        ///	The callback_data passed in the callback is the current time, in
        ///	milliseconds, with an undefined base.
        #[derive(Debug)]
        pub struct Frame {
            pub callback: New<interfaces::Callback>,
        }


        /// Request: set opaque region
        ///
        /// This request sets the region of the surface that contains
        ///	opaque content.
        ///
        ///	The opaque region is an optimization hint for the compositor
        ///	that lets it optimize out redrawing of content behind opaque
        ///	regions.  Setting an opaque region is not required for correct
        ///	behaviour, but marking transparent content as opaque will result
        ///	in repaint artifacts.
        ///
        ///	The opaque region is specified in surface local coordinates.
        ///
        ///	The compositor ignores the parts of the opaque region that fall
        ///	outside of the surface.
        ///
        ///	Opaque region is double-buffered state, see wl_surface.commit.
        ///
        ///	wl_surface.set_opaque_region changes the pending opaque region.
        ///	wl_surface.commit copies the pending region to the current region.
        ///	Otherwise, the pending and current regions are never changed.
        ///
        ///	The initial value for opaque region is empty. Setting the pending
        ///	opaque region has copy semantics, and the wl_region object can be
        ///	destroyed immediately. A NULL wl_region causes the pending opaque
        ///	region to be set to empty.
        #[derive(Debug)]
        pub struct SetOpaqueRegion {
            pub region: interfaces::Region,
        }


        /// Request: set input region
        ///
        /// This request sets the region of the surface that can receive
        ///	pointer and touch events.
        ///
        ///	Input events happening outside of this region will try the next
        ///	surface in the server surface stack. The compositor ignores the
        ///	parts of the input region that fall outside of the surface.
        ///
        ///	The input region is specified in surface local coordinates.
        ///
        ///	Input region is double-buffered state, see wl_surface.commit.
        ///
        ///	wl_surface.set_input_region changes the pending input region.
        ///	wl_surface.commit copies the pending region to the current region.
        ///	Otherwise the pending and current regions are never changed,
        ///	except cursor and icon surfaces are special cases, see
        ///	wl_pointer.set_cursor and wl_data_device.start_drag.
        ///
        ///	The initial value for input region is infinite. That means the
        ///	whole surface will accept input. Setting the pending input region
        ///	has copy semantics, and the wl_region object can be destroyed
        ///	immediately. A NULL wl_region causes the input region to be set
        ///	to infinite.
        #[derive(Debug)]
        pub struct SetInputRegion {
            pub region: interfaces::Region,
        }


        /// Request: commit pending surface state
        ///
        /// Surface state (input, opaque, and damage regions, attached buffers,
        ///	etc.) is double-buffered. Protocol requests modify the pending
        ///	state, as opposed to current state in use by the compositor. Commit
        ///	request atomically applies all pending state, replacing the current
        ///	state. After commit, the new pending state is as documented for each
        ///	related request.
        ///
        ///	On commit, a pending wl_buffer is applied first, all other state
        ///	second. This means that all coordinates in double-buffered state are
        ///	relative to the new wl_buffer coming into use, except for
        ///	wl_surface.attach itself. If there is no pending wl_buffer, the
        ///	coordinates are relative to the current surface contents.
        ///
        ///	All requests that need a commit to become effective are documented
        ///	to affect double-buffered state.
        ///
        ///	Other interfaces may add further double-buffered surface state.
        #[derive(Debug)]
        pub struct Commit;


        /// Request: sets the buffer transformation
        ///
        /// This request sets an optional transformation on how the compositor
        ///	interprets the contents of the buffer attached to the surface. The
        ///	accepted values for the transform parameter are the values for
        ///	wl_output.transform.
        ///
        ///	Buffer transform is double-buffered state, see wl_surface.commit.
        ///
        ///	A newly created surface has its buffer transformation set to normal.
        ///
        ///	wl_surface.set_buffer_transform changes the pending buffer
        ///	transformation. wl_surface.commit copies the pending buffer
        ///	transformation to the current one. Otherwise, the pending and current
        ///	values are never changed.
        ///
        ///	The purpose of this request is to allow clients to render content
        ///	according to the output transform, thus permiting the compositor to
        ///	use certain optimizations even if the display is rotated. Using
        ///	hardware overlays and scanning out a client buffer for fullscreen
        ///	surfaces are examples of such optimizations. Those optimizations are
        ///	highly dependent on the compositor implementation, so the use of this
        ///	request should be considered on a case-by-case basis.
        ///
        ///	Note that if the transform value includes 90 or 270 degree rotation,
        ///	the width of the buffer will become the surface height and the height
        ///	of the buffer will become the surface width.
        ///
        ///	If transform is not one of the values from the
        ///	wl_output.transform enum the invalid_transform protocol error
        ///	is raised.
        #[derive(Debug)]
        pub struct SetBufferTransform {
            pub transform: i32,
        }


        /// Request: sets the buffer scaling factor
        ///
        /// This request sets an optional scaling factor on how the compositor
        ///	interprets the contents of the buffer attached to the window.
        ///
        ///	Buffer scale is double-buffered state, see wl_surface.commit.
        ///
        ///	A newly created surface has its buffer scale set to 1.
        ///
        ///	wl_surface.set_buffer_scale changes the pending buffer scale.
        ///	wl_surface.commit copies the pending buffer scale to the current one.
        ///	Otherwise, the pending and current values are never changed.
        ///
        ///	The purpose of this request is to allow clients to supply higher
        ///	resolution buffer data for use on high resolution outputs. Its
        ///	intended that you pick the same	buffer scale as the scale of the
        ///	output that the surface is displayed on.This means the compositor
        ///	can avoid scaling when rendering the surface on that output.
        ///
        ///	Note that if the scale is larger than 1, then you have to attach
        ///	a buffer that is larger (by a factor of scale in each dimension)
        ///	than the desired surface size.
        ///
        ///	If scale is not positive the invalid_scale protocol error is
        ///	raised.
        #[derive(Debug)]
        pub struct SetBufferScale {
            pub scale: i32,
        }


        /// Request: mark part of the surface damaged using buffer co-ordinates
        ///
        /// This request is used to describe the regions where the pending
        ///	buffer is different from the current surface contents, and where
        ///	the surface therefore needs to be repainted. The compositor
        ///	ignores the parts of the damage that fall outside of the surface.
        ///
        ///	Damage is double-buffered state, see wl_surface.commit.
        ///
        ///	The damage rectangle is specified in buffer coordinates.
        ///
        ///	The initial value for pending damage is empty: no damage.
        ///	wl_surface.damage_buffer adds pending damage: the new pending
        ///	damage is the union of old pending damage and the given rectangle.
        ///
        ///	wl_surface.commit assigns pending damage as the current damage,
        ///	and clears pending damage. The server will clear the current
        ///	damage as it repaints the surface.
        ///
        ///	This request differs from wl_surface.damage in only one way - it
        ///	takes damage in buffer co-ordinates instead of surface local
        ///	co-ordinates. While this generally is more intuitive than surface
        ///	co-ordinates, it is especially desirable when using wp_viewport
        ///	or when a drawing library (like EGL) is unaware of buffer scale
        ///	and buffer transform.
        ///
        ///	Note: Because buffer transformation changes and damage requests may
        ///	be interleaved in the protocol stream, It is impossible to determine
        ///	the actual mapping between surface and buffer damage until
        ///	wl_surface.commit time. Therefore, compositors wishing to take both
        ///	kinds of damage into account will have to accumulate damage from the
        ///	two requests separately and only transform from one to the other
        ///	after receiving the wl_surface.commit.
        #[derive(Debug)]
        pub struct DamageBuffer {
            pub x: i32,
            pub y: i32,
            pub width: i32,
            pub height: i32,
        }


        /// Event: surface enters an output
        ///
        /// This is emitted whenever a surface's creation, movement, or resizing
        ///	results in some part of it being within the scanout region of an
        ///	output.
        ///
        ///	Note that a surface may be overlapping with zero or more outputs.
        #[derive(Debug)]
        pub struct Enter {
            pub output: interfaces::Output,
        }


        /// Event: surface leaves an output
        ///
        /// This is emitted whenever a surface's creation, movement, or resizing
        ///	results in it no longer having any part of it within the scanout region
        ///	of an output.
        #[derive(Debug)]
        pub struct Leave {
            pub output: interfaces::Output,
        }

    }
    pub use self::surface::Surface;

    /// group of input devices
    ///
    /// A seat is a group of keyboards, pointer and touch devices. This
    ///  object is published as a global during start up, or when such a
    ///  device is hot plugged.  A seat typically has a pointer and
    ///  maintains a keyboard focus and a pointer focus.
    pub mod seat {
#![allow(non_upper_case_globals, non_camel_case_types, unused_imports)]
        use std::os::unix::io::RawFd;
        use {Array, Fixed, New, Interface, interfaces};
        use super::*;

        #[derive(Debug,Clone,Copy,PartialEq,Eq)]
        /// See module documentation
        pub struct Seat(pub u32);


        impl Seat {
            pub fn interface_name() -> Array {
                Array::from_string("wl_seat".into())
            }
            pub fn interface_version() -> u32 {
                5
            }
            pub fn id(self) -> u32 {
                self.0
            }
            pub fn set_id(&mut self, id: u32) {
                self.0 = id
            }
        }
        cenum!{ capability u32 {
// The seat has pointer devices
Pointer = 1,
// The seat has one or more keyboards
Keyboard = 2,
// The seat has touch devices
Touch = 4,
}}
        impl ::std::ops::BitAnd for capability {
            type Output = capability;
            fn bitand(self, rhs: capability) -> capability {
                capability(self.0 & rhs.0)
            }
        }
        impl ::std::ops::BitOr for capability {
            type Output = capability;
            fn bitor(self, rhs: capability) -> capability {
                capability(self.0 | rhs.0)
            }
        }
        impl ::std::ops::BitXor for capability {
            type Output = capability;
            fn bitxor(self, rhs: capability) -> capability {
                capability(self.0 ^ rhs.0)
            }
        }
        /// Request: return pointer object
        ///
        /// The ID provided will be initialized to the wl_pointer interface
        ///	for this seat.
        ///
        ///	This request only takes effect if the seat has the pointer
        ///	capability, or has had the pointer capability in the past.
        ///	It is a protocol violation to issue this request on a seat that has
        ///	never had the pointer capability.
        #[derive(Debug)]
        pub struct GetPointer {
            pub id: New<interfaces::Pointer>,
        }


        /// Request: return keyboard object
        ///
        /// The ID provided will be initialized to the wl_keyboard interface
        ///	for this seat.
        ///
        ///	This request only takes effect if the seat has the keyboard
        ///	capability, or has had the keyboard capability in the past.
        ///	It is a protocol violation to issue this request on a seat that has
        ///	never had the keyboard capability.
        #[derive(Debug)]
        pub struct GetKeyboard {
            pub id: New<interfaces::Keyboard>,
        }


        /// Request: return touch object
        ///
        /// The ID provided will be initialized to the wl_touch interface
        ///	for this seat.
        ///
        ///	This request only takes effect if the seat has the touch
        ///	capability, or has had the touch capability in the past.
        ///	It is a protocol violation to issue this request on a seat that has
        ///	never had the touch capability.
        #[derive(Debug)]
        pub struct GetTouch {
            pub id: New<interfaces::Touch>,
        }


        /// Request: release the seat object
        ///
        /// Using this request client can tell the server that it is not going to
        ///	use the seat object anymore.
        #[derive(Debug)]
        pub struct Release;


        /// Event: seat capabilities changed
        ///
        /// This is emitted whenever a seat gains or loses the pointer,
        ///	keyboard or touch capabilities.  The argument is a capability
        ///	enum containing the complete set of capabilities this seat has.
        ///
        ///	When the pointer capability is added, a client may create a
        ///	wl_pointer object using the wl_seat.get_pointer request. This object
        ///	will receive pointer events until the capability is removed in the
        ///	future.
        ///
        ///	When the pointer capability is removed, a client should destroy the
        ///	wl_pointer objects associated with the seat where the capability was
        ///	removed, using the wl_pointer.release request. No further pointer
        ///	events will be received on these objects.
        ///
        ///	In some compositors, if a seat regains the pointer capability and a
        ///	client has a previously obtained wl_pointer object of version 4 or
        ///	less, that object may start sending pointer events again. This
        ///	behavior is considered a misinterpretation of the intended behavior
        ///	and must not be relied upon by the client. wl_pointer objects of
        ///	version 5 or later must not send events if created before the most
        ///	recent event notifying the client of an added pointer capability.
        ///
        ///	The above behavior also applies to wl_keyboard and wl_touch with the
        ///	keyboard and touch capabilities, respectively.
        #[derive(Debug)]
        pub struct Capabilities {
            pub capabilities: ::interfaces::seat::capability,
        }


        /// Event: unique identifier for this seat
        ///
        /// In a multiseat configuration this can be used by the client to help
        ///	identify which physical devices the seat represents. Based on
        ///	the seat configuration used by the compositor.
        #[derive(Debug)]
        pub struct Name {
            pub name: Array, // placeholder for string
        }

    }
    pub use self::seat::Seat;

    /// pointer input device
    ///
    /// The wl_pointer interface represents one or more input devices,
    ///  such as mice, which control the pointer location and pointer_focus
    ///  of a seat.
    ///
    ///  The wl_pointer interface generates motion, enter and leave
    ///  events for the surfaces that the pointer is located over,
    ///  and button and axis events for button presses, button releases
    ///  and scrolling.
    pub mod pointer {
#![allow(non_upper_case_globals, non_camel_case_types, unused_imports)]
        use std::os::unix::io::RawFd;
        use {Array, Fixed, New, Interface, interfaces};
        use super::*;

        #[derive(Debug,Clone,Copy,PartialEq,Eq)]
        /// See module documentation
        pub struct Pointer(pub u32);


        impl Pointer {
            pub fn interface_name() -> Array {
                Array::from_string("wl_pointer".into())
            }
            pub fn interface_version() -> u32 {
                5
            }
            pub fn id(self) -> u32 {
                self.0
            }
            pub fn set_id(&mut self, id: u32) {
                self.0 = id
            }
        }
        cenum!{ error u32 {
// given wl_surface has another role
Role = 0,
}}
        cenum!{ button_state u32 {
// The button is not pressed
Released = 0,
// The button is pressed
Pressed = 1,
}}
        cenum!{ axis u32 {
VerticalScroll = 0,
HorizontalScroll = 1,
}}
        cenum!{ axis_source u32 {
// A physical wheel
Wheel = 0,
// Finger on a touch surface
Finger = 1,
// Continuous coordinate space
Continuous = 2,
}}

        /// Request: set the pointer surface
        ///
        /// Set the pointer surface, i.e., the surface that contains the
        ///	pointer image (cursor). This request gives the surface the role
        ///	of a cursor. If the surface already has another role, it raises
        ///	a protocol error.
        ///
        ///	The cursor actually changes only if the pointer
        ///	focus for this device is one of the requesting client's surfaces
        ///	or the surface parameter is the current pointer surface. If
        ///	there was a previous surface set with this request it is
        ///	replaced. If surface is NULL, the pointer image is hidden.
        ///
        ///	The parameters hotspot_x and hotspot_y define the position of
        ///	the pointer surface relative to the pointer location. Its
        ///	top-left corner is always at (x, y) - (hotspot_x, hotspot_y),
        ///	where (x, y) are the coordinates of the pointer location, in surface
        ///	local coordinates.
        ///
        ///	On surface.attach requests to the pointer surface, hotspot_x
        ///	and hotspot_y are decremented by the x and y parameters
        ///	passed to the request. Attach must be confirmed by
        ///	wl_surface.commit as usual.
        ///
        ///	The hotspot can also be updated by passing the currently set
        ///	pointer surface to this request with new values for hotspot_x
        ///	and hotspot_y.
        ///
        ///	The current and pending input regions of the wl_surface are
        ///	cleared, and wl_surface.set_input_region is ignored until the
        ///	wl_surface is no longer used as the cursor. When the use as a
        ///	cursor ends, the current and pending input regions become
        ///	undefined, and the wl_surface is unmapped.
        #[derive(Debug)]
        pub struct SetCursor {
            /// serial of the enter event
            pub serial: u32,
            pub surface: interfaces::Surface,
            /// x coordinate in surface-relative coordinates
            pub hotspot_x: i32,
            /// y coordinate in surface-relative coordinates
            pub hotspot_y: i32,
        }


        /// Request: release the pointer object
        ///
        /// Using this request client can tell the server that it is not going to
        ///	use the pointer object anymore.
        ///
        ///	This request destroys the pointer proxy object, so user must not call
        ///	wl_pointer_destroy() after using this request.
        #[derive(Debug)]
        pub struct Release;


        /// Event: enter event
        ///
        /// Notification that this seat's pointer is focused on a certain
        ///	surface.
        ///
        ///	When an seat's focus enters a surface, the pointer image
        ///	is undefined and a client should respond to this event by setting
        ///	an appropriate pointer image with the set_cursor request.
        #[derive(Debug)]
        pub struct Enter {
            pub serial: u32,
            pub surface: interfaces::Surface,
            /// x coordinate in surface-relative coordinates
            pub surface_x: Fixed,
            /// y coordinate in surface-relative coordinates
            pub surface_y: Fixed,
        }


        /// Event: leave event
        ///
        /// Notification that this seat's pointer is no longer focused on
        ///	a certain surface.
        ///
        ///	The leave notification is sent before the enter notification
        ///	for the new focus.
        #[derive(Debug)]
        pub struct Leave {
            pub serial: u32,
            pub surface: interfaces::Surface,
        }


        /// Event: pointer motion event
        ///
        /// Notification of pointer location change. The arguments
        ///	surface_x and surface_y are the location relative to the
        ///	focused surface.
        #[derive(Debug)]
        pub struct Motion {
            /// timestamp with millisecond granularity
            pub time: u32,
            /// x coordinate in surface-relative coordinates
            pub surface_x: Fixed,
            /// y coordinate in surface-relative coordinates
            pub surface_y: Fixed,
        }


        /// Event: pointer button event
        ///
        /// Mouse button click and release notifications.
        ///
        ///	The location of the click is given by the last motion or
        ///	enter event.
        ///The time argument is a timestamp with millisecond
        ///granularity, with an undefined base.
        #[derive(Debug)]
        pub struct Button {
            pub serial: u32,
            /// timestamp with millisecond granularity
            pub time: u32,
            pub button: u32,
            pub state: ::interfaces::pointer::button_state,
        }


        /// Event: axis event
        ///
        /// Scroll and other axis notifications.
        ///
        ///	For scroll events (vertical and horizontal scroll axes), the
        ///	value parameter is the length of a vector along the specified
        ///	axis in a coordinate space identical to those of motion events,
        ///	representing a relative movement along the specified axis.
        ///
        ///	For devices that support movements non-parallel to axes multiple
        ///	axis events will be emitted.
        ///
        ///	When applicable, for example for touch pads, the server can
        ///	choose to emit scroll events where the motion vector is
        ///	equivalent to a motion event vector.
        ///
        ///	When applicable, clients can transform its view relative to the
        ///	scroll distance.
        #[derive(Debug)]
        pub struct Axis {
            /// timestamp with millisecond granularity
            pub time: u32,
            pub axis: ::interfaces::pointer::axis,
            pub value: Fixed,
        }


        /// Event: end of a pointer event sequence
        ///
        /// Indicates the end of a set of events that logically belong together.
        ///	A client is expected to accumulate the data in all events within the
        ///	frame before proceeding.
        ///
        ///	All wl_pointer events before a wl_pointer.frame event belong
        ///	logically together. For example, in a diagonal scroll motion the
        ///	compositor will send an optional wl_pointer.axis_source event, two
        ///	wl_pointer.axis events (horizontal and vertical) and finally a
        ///	wl_pointer.frame event. The client may use this information to
        ///	calculate a diagonal vector for scrolling.
        ///
        ///	When multiple wl_pointer.axis events occur within the same frame,
        ///	the motion vector is the combined motion of all events.
        ///	When a wl_pointer.axis and a wl_pointer.axis_stop event occur within
        ///	the same frame, this indicates that axis movement in one axis has
        ///	stopped but continues in the other axis.
        ///	When multiple wl_pointer.axis_stop events occur within in the same
        ///	frame, this indicates that these axes stopped in the same instance.
        ///
        ///	A wl_pointer.frame event is sent for every logical event group,
        ///	even if the group only contains a single wl_pointer event.
        ///	Specifically, a client may get a sequence: motion, frame, button,
        ///	frame, axis, frame, axis_stop, frame.
        ///
        ///	The wl_pointer.enter and wl_pointer.leave events are logical events
        ///	generated by the compositor and not the hardware. These events are
        ///	also grouped by a wl_pointer.frame. When a pointer moves from one
        ///	surface to the another, a compositor should group the
        ///	wl_pointer.leave event within the same wl_pointer.frame.
        ///	However, a client must not rely on wl_pointer.leave and
        ///	wl_pointer.enter being in the same wl_pointer.frame.
        ///	Compositor-specific policies may require the wl_pointer.leave and
        ///	wl_pointer.enter event being split across multiple wl_pointer.frame
        ///	groups.
        #[derive(Debug)]
        pub struct Frame;


        /// Event: axis source event
        ///
        /// Source information for scroll and other axes.
        ///
        ///	This event does not occur on its own. It is sent before a
        ///	wl_pointer.frame event and carries the source information for
        ///	all events within that frame.
        ///
        ///	The source specifies how this event was generated. If the source is
        ///	wl_pointer.axis_source.finger, a wl_pointer.axis_stop event will be
        ///	sent when the user lifts the finger off the device.
        ///
        ///	If the source is wl_pointer axis_source.wheel or
        ///	wl_pointer.axis_source.continuous, a wl_pointer.axis_stop event may
        ///	or may not be sent. Whether a compositor sends a axis_stop event
        ///	for these sources is hardware-specific and implementation-dependent;
        ///	clients must not rely on receiving an axis_stop event for these
        ///	scroll sources and should treat scroll sequences from these scroll
        ///	sources as unterminated by default.
        ///
        ///	This event is optional. If the source is unknown for a particular
        ///	axis event sequence, no event is sent.
        ///	Only one wl_pointer.axis_source event is permitted per frame.
        ///
        ///	The order of wl_pointer.axis_discrete and wl_pointer.axis_source is
        ///	not guaranteed.
        #[derive(Debug)]
        pub struct AxisSource {
            pub axis_source: ::interfaces::pointer::axis_source,
        }


        /// Event: axis stop event
        ///
        /// Stop notification for scroll and other axes.
        ///
        ///	For some wl_pointer.axis_source types, a wl_pointer.axis_stop event
        ///	is sent to notify a client that the axis sequence has terminated.
        ///	This enables the client to implement kinetic scrolling.
        ///	See the wl_pointer.axis_source documentation for information on when
        ///	this event may be generated.
        ///
        ///	Any wl_pointer.axis events with the same axis_source after this
        ///	event should be considered as the start of a new axis motion.
        ///
        ///	The timestamp is to be interpreted identical to the timestamp in the
        ///	wl_pointer.axis event. The timestamp value may be the same as a
        ///	preceeding wl_pointer.axis event.
        #[derive(Debug)]
        pub struct AxisStop {
            /// timestamp with millisecond granularity
            pub time: u32,
            /// the axis stopped with this event
            pub axis: ::interfaces::pointer::axis,
        }


        /// Event: axis click event
        ///
        /// Discrete step information for scroll and other axes.
        ///
        ///	This event carries the axis value of the wl_pointer.axis event in
        ///	discrete steps (e.g. mouse wheel clicks).
        ///
        ///	This event does not occur on its own, it is coupled with a
        ///	wl_pointer.axis event that represents this axis value on a
        ///	continuous scale. The protocol guarantees that each axis_discrete
        ///	event is always followed by exactly one axis event with the same
        ///	axis number within the same wl_pointer.frame. Note that the protocol
        ///	allows for other events to occur between the axis_discrete and
        ///	its coupled axis event, including other axis_discrete or axis
        ///	events.
        ///
        ///	This event is optional; continuous scrolling devices
        ///	like two-finger scrolling on touchpads do not have discrete
        ///	steps and do not generate this event.
        ///
        ///	The discrete value carries the directional information. e.g. a value
        ///	of -2 is two steps towards the negative direction of this axis.
        ///
        ///	The axis number is identical to the axis number in the associate
        ///	axis event.
        ///
        ///	The order of wl_pointer.axis_discrete and wl_pointer.axis_source is
        ///	not guaranteed.
        #[derive(Debug)]
        pub struct AxisDiscrete {
            pub axis: ::interfaces::pointer::axis,
            pub discrete: i32,
        }

    }
    pub use self::pointer::Pointer;

    /// keyboard input device
    ///
    /// The wl_keyboard interface represents one or more keyboards
    ///  associated with a seat.
    pub mod keyboard {
#![allow(non_upper_case_globals, non_camel_case_types, unused_imports)]
        use std::os::unix::io::RawFd;
        use {Array, Fixed, New, Interface, interfaces};
        use super::*;

        #[derive(Debug,Clone,Copy,PartialEq,Eq)]
        /// See module documentation
        pub struct Keyboard(pub u32);


        impl Keyboard {
            pub fn interface_name() -> Array {
                Array::from_string("wl_keyboard".into())
            }
            pub fn interface_version() -> u32 {
                5
            }
            pub fn id(self) -> u32 {
                self.0
            }
            pub fn set_id(&mut self, id: u32) {
                self.0 = id
            }
        }
        cenum!{ keymap_format u32 {
        // no keymap; client must understand how to interpret the raw keycode
NoKeymap = 0,
        // libxkbcommon compatible; to determine the xkb keycode, clients must add 8 to the key event keycode
XkbV1 = 1,
}}
        cenum!{ key_state u32 {
// key is not pressed
Released = 0,
// key is pressed
Pressed = 1,
}}

        /// Request: release the keyboard object
        ///
        /// 
        #[derive(Debug)]
        pub struct Release;


        /// Event: keyboard mapping
        ///
        /// This event provides a file descriptor to the client which can be
        ///	memory-mapped to provide a keyboard mapping description.
        #[derive(Debug)]
        pub struct Keymap {
            pub format: ::interfaces::keyboard::keymap_format,
            pub fd: RawFd,
            pub size: u32,
        }


        /// Event: enter event
        ///
        /// Notification that this seat's keyboard focus is on a certain
        ///	surface.
        #[derive(Debug)]
        pub struct Enter {
            pub serial: u32,
            pub surface: interfaces::Surface,
            /// the currently pressed keys
            pub keys: Array,
        }


        /// Event: leave event
        ///
        /// Notification that this seat's keyboard focus is no longer on
        ///	a certain surface.
        ///
        ///	The leave notification is sent before the enter notification
        ///	for the new focus.
        #[derive(Debug)]
        pub struct Leave {
            pub serial: u32,
            pub surface: interfaces::Surface,
        }


        /// Event: key event
        ///
        /// A key was pressed or released.
        ///The time argument is a timestamp with millisecond
        ///granularity, with an undefined base.
        #[derive(Debug)]
        pub struct Key {
            pub serial: u32,
            /// timestamp with millisecond granularity
            pub time: u32,
            pub key: u32,
            pub state: ::interfaces::keyboard::key_state,
        }


        /// Event: modifier and group state
        ///
        /// Notifies clients that the modifier and/or group state has
        ///	changed, and it should update its local state.
        #[derive(Debug)]
        pub struct Modifiers {
            pub serial: u32,
            pub mods_depressed: u32,
            pub mods_latched: u32,
            pub mods_locked: u32,
            pub group: u32,
        }


        /// Event: repeat rate and delay
        ///
        /// Informs the client about the keyboard's repeat rate and delay.
        ///
        ///This event is sent as soon as the wl_keyboard object has been created,
        ///and is guaranteed to be received by the client before any key press
        ///event.
        ///
        ///Negative values for either rate or delay are illegal. A rate of zero
        ///will disable any repeating (regardless of the value of delay).
        ///
        ///This event can be sent later on as well with a new value if necessary,
        ///so clients should continue listening for the event past the creation
        ///of wl_keyboard.
        #[derive(Debug)]
        pub struct RepeatInfo {
            /// the rate of repeating keys in characters per second
            pub rate: i32,
            /// delay in milliseconds since key down until repeating starts
            pub delay: i32,
        }

    }
    pub use self::keyboard::Keyboard;

    /// touchscreen input device
    ///
    /// The wl_touch interface represents a touchscreen
    ///  associated with a seat.
    ///
    ///  Touch interactions can consist of one or more contacts.
    ///  For each contact, a series of events is generated, starting
    ///  with a down event, followed by zero or more motion events,
    ///  and ending with an up event. Events relating to the same
    ///  contact point can be identified by the ID of the sequence.
    pub mod touch {
#![allow(non_upper_case_globals, non_camel_case_types, unused_imports)]
        use std::os::unix::io::RawFd;
        use {Array, Fixed, New, Interface, interfaces};
        use super::*;

        #[derive(Debug,Clone,Copy,PartialEq,Eq)]
        /// See module documentation
        pub struct Touch(pub u32);


        impl Touch {
            pub fn interface_name() -> Array {
                Array::from_string("wl_touch".into())
            }
            pub fn interface_version() -> u32 {
                5
            }
            pub fn id(self) -> u32 {
                self.0
            }
            pub fn set_id(&mut self, id: u32) {
                self.0 = id
            }
        }

        /// Request: release the touch object
        ///
        /// 
        #[derive(Debug)]
        pub struct Release;


        /// Event: touch down event and beginning of a touch sequence
        ///
        /// A new touch point has appeared on the surface. This touch point is
        ///	assigned a unique @id. Future events from this touchpoint reference
        ///	this ID. The ID ceases to be valid after a touch up event and may be
        ///	re-used in the future.
        #[derive(Debug)]
        pub struct Down {
            pub serial: u32,
            /// timestamp with millisecond granularity
            pub time: u32,
            pub surface: interfaces::Surface,
            /// the unique ID of this touch point
            pub id: i32,
            /// x coordinate in surface-relative coordinates
            pub x: Fixed,
            /// y coordinate in surface-relative coordinates
            pub y: Fixed,
        }


        /// Event: end of a touch event sequence
        ///
        /// The touch point has disappeared. No further events will be sent for
        ///	this touchpoint and the touch point's ID is released and may be
        ///	re-used in a future touch down event.
        #[derive(Debug)]
        pub struct Up {
            pub serial: u32,
            /// timestamp with millisecond granularity
            pub time: u32,
            /// the unique ID of this touch point
            pub id: i32,
        }


        /// Event: update of touch point coordinates
        ///
        /// A touchpoint has changed coordinates.
        #[derive(Debug)]
        pub struct Motion {
            /// timestamp with millisecond granularity
            pub time: u32,
            /// the unique ID of this touch point
            pub id: i32,
            /// x coordinate in surface-relative coordinates
            pub x: Fixed,
            /// y coordinate in surface-relative coordinates
            pub y: Fixed,
        }


        /// Event: end of touch frame event
        ///
        /// Indicates the end of a contact point list.
        #[derive(Debug)]
        pub struct Frame;


        /// Event: touch session cancelled
        ///
        /// Sent if the compositor decides the touch stream is a global
        ///	gesture. No further events are sent to the clients from that
        ///	particular gesture. Touch cancellation applies to all touch points
        ///	currently active on this client's surface. The client is
        ///	responsible for finalizing the touch points, future touch points on
        ///	this surface may re-use the touch point ID.
        #[derive(Debug)]
        pub struct Cancel;

    }
    pub use self::touch::Touch;

    /// compositor output region
    ///
    /// An output describes part of the compositor geometry.  The
    ///  compositor works in the 'compositor coordinate system' and an
    ///  output corresponds to rectangular area in that space that is
    ///  actually visible.  This typically corresponds to a monitor that
    ///  displays part of the compositor space.  This object is published
    ///  as global during start up, or when a monitor is hotplugged.
    pub mod output {
#![allow(non_upper_case_globals, non_camel_case_types, unused_imports)]
        use std::os::unix::io::RawFd;
        use {Array, Fixed, New, Interface, interfaces};
        use super::*;

        #[derive(Debug,Clone,Copy,PartialEq,Eq)]
        /// See module documentation
        pub struct Output(pub u32);


        impl Output {
            pub fn interface_name() -> Array {
                Array::from_string("wl_output".into())
            }
            pub fn interface_version() -> u32 {
                2
            }
            pub fn id(self) -> u32 {
                self.0
            }
            pub fn set_id(&mut self, id: u32) {
                self.0 = id
            }
        }
        cenum!{ subpixel u32 {
Unknown = 0,
None = 1,
HorizontalRgb = 2,
HorizontalBgr = 3,
VerticalRgb = 4,
VerticalBgr = 5,
}}
        cenum!{ transform u32 {
Normal = 0,
_90 = 1,
_180 = 2,
_270 = 3,
Flipped = 4,
Flipped90 = 5,
Flipped180 = 6,
Flipped270 = 7,
}}
        cenum!{ mode u32 {
// indicates this is the current mode
Current = 1,
// indicates this is the preferred mode
Preferred = 2,
}}
        impl ::std::ops::BitAnd for mode {
            type Output = mode;
            fn bitand(self, rhs: mode) -> mode {
                mode(self.0 & rhs.0)
            }
        }
        impl ::std::ops::BitOr for mode {
            type Output = mode;
            fn bitor(self, rhs: mode) -> mode {
                mode(self.0 | rhs.0)
            }
        }
        impl ::std::ops::BitXor for mode {
            type Output = mode;
            fn bitxor(self, rhs: mode) -> mode {
                mode(self.0 ^ rhs.0)
            }
        }
        /// Event: properties of the output
        ///
        /// The geometry event describes geometric properties of the output.
        ///	The event is sent when binding to the output object and whenever
        ///	any of the properties change.
        #[derive(Debug)]
        pub struct Geometry {
            /// x position within the global compositor space
            pub x: i32,
            /// y position within the global compositor space
            pub y: i32,
            /// width in millimeters of the output
            pub physical_width: i32,
            /// height in millimeters of the output
            pub physical_height: i32,
            /// subpixel orientation of the output
            pub subpixel: ::interfaces::output::subpixel,
            /// textual description of the manufacturer
            pub make: Array, // placeholder for string
            /// textual description of the model
            pub model: Array, // placeholder for string
            /// transform that maps framebuffer to output
            pub transform: ::interfaces::output::transform,
        }


        /// Event: advertise available modes for the output
        ///
        /// The mode event describes an available mode for the output.
        ///
        ///	The event is sent when binding to the output object and there
        ///	will always be one mode, the current mode.  The event is sent
        ///	again if an output changes mode, for the mode that is now
        ///	current.  In other words, the current mode is always the last
        ///	mode that was received with the current flag set.
        ///
        ///	The size of a mode is given in physical hardware units of
        ///the output device. This is not necessarily the same as
        ///the output size in the global compositor space. For instance,
        ///the output may be scaled, as described in wl_output.scale,
        ///or transformed , as described in wl_output.transform.
        #[derive(Debug)]
        pub struct Mode {
            /// bitfield of mode flags
            pub flags: ::interfaces::output::mode,
            /// width of the mode in hardware units
            pub width: i32,
            /// height of the mode in hardware units
            pub height: i32,
            /// vertical refresh rate in mHz
            pub refresh: i32,
        }


        /// Event: sent all information about output
        ///
        /// This event is sent after all other properties has been
        ///sent after binding to the output object and after any
        ///other property changes done after that. This allows
        ///changes to the output properties to be seen as
        ///atomic, even if they happen via multiple events.
        #[derive(Debug)]
        pub struct Done;


        /// Event: output scaling properties
        ///
        /// This event contains scaling geometry information
        ///that is not in the geometry event. It may be sent after
        ///binding the output object or if the output scale changes
        ///later. If it is not sent, the client should assume a
        ///	scale of 1.
        ///
        ///	A scale larger than 1 means that the compositor will
        ///	automatically scale surface buffers by this amount
        ///	when rendering. This is used for very high resolution
        ///	displays where applications rendering at the native
        ///	resolution would be too small to be legible.
        ///
        ///	It is intended that scaling aware clients track the
        ///	current output of a surface, and if it is on a scaled
        ///	output it should use wl_surface.set_buffer_scale with
        ///	the scale of the output. That way the compositor can
        ///	avoid scaling the surface, and the client can supply
        ///	a higher detail image.
        #[derive(Debug)]
        pub struct Scale {
            /// scaling factor of output
            pub factor: i32,
        }

    }
    pub use self::output::Output;

    /// region interface
    ///
    /// A region object describes an area.
    ///
    ///  Region objects are used to describe the opaque and input
    ///  regions of a surface.
    pub mod region {
#![allow(non_upper_case_globals, non_camel_case_types, unused_imports)]
        use std::os::unix::io::RawFd;
        use {Array, Fixed, New, Interface, interfaces};
        use super::*;

        #[derive(Debug,Clone,Copy,PartialEq,Eq)]
        /// See module documentation
        pub struct Region(pub u32);


        impl Region {
            pub fn interface_name() -> Array {
                Array::from_string("wl_region".into())
            }
            pub fn interface_version() -> u32 {
                1
            }
            pub fn id(self) -> u32 {
                self.0
            }
            pub fn set_id(&mut self, id: u32) {
                self.0 = id
            }
        }

        /// Request: destroy region
        ///
        /// Destroy the region.  This will invalidate the object ID.
        #[derive(Debug)]
        pub struct Destroy;


        /// Request: add rectangle to region
        ///
        /// Add the specified rectangle to the region.
        #[derive(Debug)]
        pub struct Add {
            pub x: i32,
            pub y: i32,
            pub width: i32,
            pub height: i32,
        }


        /// Request: subtract rectangle from region
        ///
        /// Subtract the specified rectangle from the region.
        #[derive(Debug)]
        pub struct Subtract {
            pub x: i32,
            pub y: i32,
            pub width: i32,
            pub height: i32,
        }

    }
    pub use self::region::Region;

    /// sub-surface compositing
    ///
    /// The global interface exposing sub-surface compositing capabilities.
    ///  A wl_surface, that has sub-surfaces associated, is called the
    ///  parent surface. Sub-surfaces can be arbitrarily nested and create
    ///  a tree of sub-surfaces.
    ///
    ///  The root surface in a tree of sub-surfaces is the main
    ///  surface. The main surface cannot be a sub-surface, because
    ///  sub-surfaces must always have a parent.
    ///
    ///  A main surface with its sub-surfaces forms a (compound) window.
    ///  For window management purposes, this set of wl_surface objects is
    ///  to be considered as a single window, and it should also behave as
    ///  such.
    ///
    ///  The aim of sub-surfaces is to offload some of the compositing work
    ///  within a window from clients to the compositor. A prime example is
    ///  a video player with decorations and video in separate wl_surface
    ///  objects. This should allow the compositor to pass YUV video buffer
    ///  processing to dedicated overlay hardware when possible.
    pub mod subcompositor {
#![allow(non_upper_case_globals, non_camel_case_types, unused_imports)]
        use std::os::unix::io::RawFd;
        use {Array, Fixed, New, Interface, interfaces};
        use super::*;

        #[derive(Debug,Clone,Copy,PartialEq,Eq)]
        /// See module documentation
        pub struct Subcompositor(pub u32);


        impl Subcompositor {
            pub fn interface_name() -> Array {
                Array::from_string("wl_subcompositor".into())
            }
            pub fn interface_version() -> u32 {
                1
            }
            pub fn id(self) -> u32 {
                self.0
            }
            pub fn set_id(&mut self, id: u32) {
                self.0 = id
            }
        }
        cenum!{ error u32 {
// the to-be sub-surface is invalid
BadSurface = 0,
}}

        /// Request: unbind from the subcompositor interface
        ///
        /// Informs the server that the client will not be using this
        ///	protocol object anymore. This does not affect any other
        ///	objects, wl_subsurface objects included.
        #[derive(Debug)]
        pub struct Destroy;


        /// Request: give a surface the role sub-surface
        ///
        /// Create a sub-surface interface for the given surface, and
        ///	associate it with the given parent surface. This turns a
        ///	plain wl_surface into a sub-surface.
        ///
        ///	The to-be sub-surface must not already have another role, and it
        ///	must not have an existing wl_subsurface object. Otherwise a protocol
        ///	error is raised.
        #[derive(Debug)]
        pub struct GetSubsurface {
            /// the new subsurface object id
            pub id: New<interfaces::Subsurface>,
            /// the surface to be turned into a sub-surface
            pub surface: interfaces::Surface,
            /// the parent surface
            pub parent: interfaces::Surface,
        }

    }
    pub use self::subcompositor::Subcompositor;

    /// sub-surface interface to a wl_surface
    ///
    /// An additional interface to a wl_surface object, which has been
    ///  made a sub-surface. A sub-surface has one parent surface. A
    ///  sub-surface's size and position are not limited to that of the parent.
    ///  Particularly, a sub-surface is not automatically clipped to its
    ///  parent's area.
    ///
    ///  A sub-surface becomes mapped, when a non-NULL wl_buffer is applied
    ///  and the parent surface is mapped. The order of which one happens
    ///  first is irrelevant. A sub-surface is hidden if the parent becomes
    ///  hidden, or if a NULL wl_buffer is applied. These rules apply
    ///  recursively through the tree of surfaces.
    ///
    ///  The behaviour of wl_surface.commit request on a sub-surface
    ///  depends on the sub-surface's mode. The possible modes are
    ///  synchronized and desynchronized, see methods
    ///  wl_subsurface.set_sync and wl_subsurface.set_desync. Synchronized
    ///  mode caches the wl_surface state to be applied when the parent's
    ///  state gets applied, and desynchronized mode applies the pending
    ///  wl_surface state directly. A sub-surface is initially in the
    ///  synchronized mode.
    ///
    ///  Sub-surfaces have also other kind of state, which is managed by
    ///  wl_subsurface requests, as opposed to wl_surface requests. This
    ///  state includes the sub-surface position relative to the parent
    ///  surface (wl_subsurface.set_position), and the stacking order of
    ///  the parent and its sub-surfaces (wl_subsurface.place_above and
    ///  .place_below). This state is applied when the parent surface's
    ///  wl_surface state is applied, regardless of the sub-surface's mode.
    ///  As the exception, set_sync and set_desync are effective immediately.
    ///
    ///  The main surface can be thought to be always in desynchronized mode,
    ///  since it does not have a parent in the sub-surfaces sense.
    ///
    ///  Even if a sub-surface is in desynchronized mode, it will behave as
    ///  in synchronized mode, if its parent surface behaves as in
    ///  synchronized mode. This rule is applied recursively throughout the
    ///  tree of surfaces. This means, that one can set a sub-surface into
    ///  synchronized mode, and then assume that all its child and grand-child
    ///  sub-surfaces are synchronized, too, without explicitly setting them.
    ///
    ///  If the wl_surface associated with the wl_subsurface is destroyed, the
    ///  wl_subsurface object becomes inert. Note, that destroying either object
    ///  takes effect immediately. If you need to synchronize the removal
    ///  of a sub-surface to the parent surface update, unmap the sub-surface
    ///  first by attaching a NULL wl_buffer, update parent, and then destroy
    ///  the sub-surface.
    ///
    ///  If the parent wl_surface object is destroyed, the sub-surface is
    ///  unmapped.
    pub mod subsurface {
#![allow(non_upper_case_globals, non_camel_case_types, unused_imports)]
        use std::os::unix::io::RawFd;
        use {Array, Fixed, New, Interface, interfaces};
        use super::*;

        #[derive(Debug,Clone,Copy,PartialEq,Eq)]
        /// See module documentation
        pub struct Subsurface(pub u32);


        impl Subsurface {
            pub fn interface_name() -> Array {
                Array::from_string("wl_subsurface".into())
            }
            pub fn interface_version() -> u32 {
                1
            }
            pub fn id(self) -> u32 {
                self.0
            }
            pub fn set_id(&mut self, id: u32) {
                self.0 = id
            }
        }
        cenum!{ error u32 {
// wl_surface is not a sibling or the parent
BadSurface = 0,
}}

        /// Request: remove sub-surface interface
        ///
        /// The sub-surface interface is removed from the wl_surface object
        ///	that was turned into a sub-surface with
        ///	wl_subcompositor.get_subsurface request. The wl_surface's association
        ///	to the parent is deleted, and the wl_surface loses its role as
        ///	a sub-surface. The wl_surface is unmapped.
        #[derive(Debug)]
        pub struct Destroy;


        /// Request: reposition the sub-surface
        ///
        /// This schedules a sub-surface position change.
        ///	The sub-surface will be moved so, that its origin (top-left
        ///	corner pixel) will be at the location x, y of the parent surface
        ///	coordinate system. The coordinates are not restricted to the parent
        ///	surface area. Negative values are allowed.
        ///
        ///	The scheduled coordinates will take effect whenever the state of the
        ///	parent surface is applied. When this happens depends on whether the
        ///	parent surface is in synchronized mode or not. See
        ///	wl_subsurface.set_sync and wl_subsurface.set_desync for details.
        ///
        ///	If more than one set_position request is invoked by the client before
        ///	the commit of the parent surface, the position of a new request always
        ///	replaces the scheduled position from any previous request.
        ///
        ///	The initial position is 0, 0.
        #[derive(Debug)]
        pub struct SetPosition {
            /// coordinate in the parent surface
            pub x: i32,
            /// coordinate in the parent surface
            pub y: i32,
        }


        /// Request: restack the sub-surface
        ///
        /// This sub-surface is taken from the stack, and put back just
        ///	above the reference surface, changing the z-order of the sub-surfaces.
        ///	The reference surface must be one of the sibling surfaces, or the
        ///	parent surface. Using any other surface, including this sub-surface,
        ///	will cause a protocol error.
        ///
        ///	The z-order is double-buffered. Requests are handled in order and
        ///	applied immediately to a pending state. The final pending state is
        ///	copied to the active state the next time the state of the parent
        ///	surface is applied. When this happens depends on whether the parent
        ///	surface is in synchronized mode or not. See wl_subsurface.set_sync and
        ///	wl_subsurface.set_desync for details.
        ///
        ///	A new sub-surface is initially added as the top-most in the stack
        ///	of its siblings and parent.
        #[derive(Debug)]
        pub struct PlaceAbove {
            /// the reference surface
            pub sibling: interfaces::Surface,
        }


        /// Request: restack the sub-surface
        ///
        /// The sub-surface is placed just below of the reference surface.
        ///	See wl_subsurface.place_above.
        #[derive(Debug)]
        pub struct PlaceBelow {
            /// the reference surface
            pub sibling: interfaces::Surface,
        }


        /// Request: set sub-surface to synchronized mode
        ///
        /// Change the commit behaviour of the sub-surface to synchronized
        ///	mode, also described as the parent dependent mode.
        ///
        ///	In synchronized mode, wl_surface.commit on a sub-surface will
        ///	accumulate the committed state in a cache, but the state will
        ///	not be applied and hence will not change the compositor output.
        ///	The cached state is applied to the sub-surface immediately after
        ///	the parent surface's state is applied. This ensures atomic
        ///	updates of the parent and all its synchronized sub-surfaces.
        ///	Applying the cached state will invalidate the cache, so further
        ///	parent surface commits do not (re-)apply old state.
        ///
        ///	See wl_subsurface for the recursive effect of this mode.
        #[derive(Debug)]
        pub struct SetSync;


        /// Request: set sub-surface to desynchronized mode
        ///
        /// Change the commit behaviour of the sub-surface to desynchronized
        ///	mode, also described as independent or freely running mode.
        ///
        ///	In desynchronized mode, wl_surface.commit on a sub-surface will
        ///	apply the pending state directly, without caching, as happens
        ///	normally with a wl_surface. Calling wl_surface.commit on the
        ///	parent surface has no effect on the sub-surface's wl_surface
        ///	state. This mode allows a sub-surface to be updated on its own.
        ///
        ///	If cached state exists when wl_surface.commit is called in
        ///	desynchronized mode, the pending state is added to the cached
        ///	state, and applied as whole. This invalidates the cache.
        ///
        ///	Note: even if a sub-surface is set to desynchronized, a parent
        ///	sub-surface may override it to behave as synchronized. For details,
        ///	see wl_subsurface.
        ///
        ///	If a surface's parent surface behaves as desynchronized, then
        ///	the cached state is applied on set_desync.
        #[derive(Debug)]
        pub struct SetDesync;

    }
    pub use self::subsurface::Subsurface;
    #[allow(unused_mut, unused_variables)]
    impl Display {
        /// Event: fatal error event
        ///
        /// The error event is sent out when a fatal (non-recoverable)
        ///	error has occurred.  The object_id argument is the object
        ///	where the error occurred, most often in response to a request
        ///	to that object.  The code identifies the error and is defined
        ///	by the object interface.  As such, each interface defines its
        ///	own set of error codes.  The message is an brief description
        ///	of the error, for (debugging) convenience.
        ///
        ///I've changed type of object_id from object to u32 since it
        ///doesn't have an interface. This is temporary until I either
        ///find out it is just a u32, or that I should be treating it
        ///like I do registry.bind and split it into 3 args. Then I can
        ///fix codegen.
        pub fn error(self,
                     con_fd: RawFd,
                     object_id: u32,
                     code: u32,
                     message: Array /* placeholder for string */)
                     -> Result<(), NixError> {
            let mut len = 5;
            len += if message.contents.len() % 4 == 0 {
                message.contents.len() / 4
            } else {
                1 + (message.contents.len() / 4)
            };
            let mut vec_buf = Vec::with_capacity(len);
            unsafe { vec_buf.set_len(len) };
            let mut buf: Box<[u32]> = vec_buf.into_boxed_slice();

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            buf[ind] = object_id as u32;
            ind += 1;

            buf[ind] = code as u32;
            ind += 1;

            buf[ind] = message.contents.len() as u32;
            unsafe {
                let dst_ptr = buf[ind + 1..].as_mut_ptr() as *mut u8;
                let src_ptr = message.contents.as_ptr();
                ::std::ptr::copy_nonoverlapping(src_ptr, dst_ptr, message.contents.len());
            };
            if message.contents.len() % 4 == 0 {
                ind += 1 + (message.contents.len() / 4);
            } else {
                ind += 2 + (message.contents.len() / 4);
            }

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: acknowledge object ID deletion
        ///
        /// This event is used internally by the object ID management
        ///	logic.  When a client deletes an object, the server will send
        ///	this event to acknowledge that it has seen the delete request.
        ///	When the client receive this event, it will know that it can
        ///	safely reuse the object ID.
        pub fn delete_id(self, con_fd: RawFd, id: u32) -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 1 as u32;

            let mut ind = 2;

            buf[ind] = id as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl Registry {
        /// Event: announce global object
        ///
        /// Notify the client of global objects.
        ///
        ///The event notifies the client that a global object with
        ///the given name is now available, and it implements the
        ///given version of the given interface.
        pub fn global(self,
                      con_fd: RawFd,
                      name: u32,
                      interface: Array, // placeholder for string
                      version: u32)
                      -> Result<(), NixError> {
            let mut len = 5;
            len += if interface.contents.len() % 4 == 0 {
                interface.contents.len() / 4
            } else {
                1 + (interface.contents.len() / 4)
            };
            let mut vec_buf = Vec::with_capacity(len);
            unsafe { vec_buf.set_len(len) };
            let mut buf: Box<[u32]> = vec_buf.into_boxed_slice();

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            buf[ind] = name as u32;
            ind += 1;

            buf[ind] = interface.contents.len() as u32;
            unsafe {
                let dst_ptr = buf[ind + 1..].as_mut_ptr() as *mut u8;
                let src_ptr = interface.contents.as_ptr();
                ::std::ptr::copy_nonoverlapping(src_ptr, dst_ptr, interface.contents.len());
            };
            if interface.contents.len() % 4 == 0 {
                ind += 1 + (interface.contents.len() / 4);
            } else {
                ind += 2 + (interface.contents.len() / 4);
            }

            buf[ind] = version as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: announce removal of global object
        ///
        /// Notify the client of removed global objects.
        ///
        ///This event notifies the client that the global identified
        ///by name is no longer available.  If the client bound to
        ///the global using the bind request, the client should now
        ///destroy that object.
        ///
        ///	The object remains valid and requests to the object will be
        ///	ignored until the client destroys it, to avoid races between
        ///	the global going away and a client sending a request to it.
        pub fn global_remove(self, con_fd: RawFd, name: u32) -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 1 as u32;

            let mut ind = 2;

            buf[ind] = name as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl Callback {
        /// Event: done event
        ///
        /// Notify the client when the related request is done.
        pub fn done(self, con_fd: RawFd, callback_data: u32) -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            buf[ind] = callback_data as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl Compositor {}
    #[allow(unused_mut, unused_variables)]
    impl ShmPool {}
    #[allow(unused_mut, unused_variables)]
    impl Shm {
        /// Event: pixel format description
        ///
        /// Informs the client about a valid pixel format that
        ///	can be used for buffers. Known formats include
        ///	argb8888 and xrgb8888.
        pub fn format(self,
                      con_fd: RawFd,
                      format: ::interfaces::shm::format)
                      -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            buf[ind] = format.0 as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl Buffer {
        /// Event: compositor releases buffer
        ///
        /// Sent when this wl_buffer is no longer used by the compositor.
        ///	The client is now free to re-use or destroy this buffer and its
        ///	backing storage.
        ///
        ///	If a client receives a release event before the frame callback
        ///	requested in the same wl_surface.commit that attaches this
        ///	wl_buffer to a surface, then the client is immediately free to
        ///	re-use the buffer and its backing storage, and does not need a
        ///	second buffer for the next surface content update. Typically
        ///	this is possible, when the compositor maintains a copy of the
        ///	wl_surface contents, e.g. as a GL texture. This is an important
        ///	optimization for GL(ES) compositors with wl_shm clients.
        pub fn release(self, con_fd: RawFd) -> Result<(), NixError> {
            let len = 2;
            let mut buf: [u32; 2] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl DataOffer {
        /// Event: advertise offered mime type
        ///
        /// Sent immediately after creating the wl_data_offer object.  One
        ///	event per offered mime type.
        pub fn offer(self,
                     con_fd: RawFd,
                     mime_type: Array /* placeholder for string */)
                     -> Result<(), NixError> {
            let mut len = 3;
            len += if mime_type.contents.len() % 4 == 0 {
                mime_type.contents.len() / 4
            } else {
                1 + (mime_type.contents.len() / 4)
            };
            let mut vec_buf = Vec::with_capacity(len);
            unsafe { vec_buf.set_len(len) };
            let mut buf: Box<[u32]> = vec_buf.into_boxed_slice();

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            buf[ind] = mime_type.contents.len() as u32;
            unsafe {
                let dst_ptr = buf[ind + 1..].as_mut_ptr() as *mut u8;
                let src_ptr = mime_type.contents.as_ptr();
                ::std::ptr::copy_nonoverlapping(src_ptr, dst_ptr, mime_type.contents.len());
            };
            if mime_type.contents.len() % 4 == 0 {
                ind += 1 + (mime_type.contents.len() / 4);
            } else {
                ind += 2 + (mime_type.contents.len() / 4);
            }

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: notify the source-side available actions
        ///
        /// This event indicates the actions offered by the data source. It
        ///	will be sent right after wl_data_device.enter, or anytime the source
        ///	side changes its offered actions through wl_data_source.set_actions.
        pub fn source_actions(self, con_fd: RawFd, source_actions: u32) -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 1 as u32;

            let mut ind = 2;

            buf[ind] = source_actions as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: notify the selected action
        ///
        /// This event indicates the action selected by the compositor after
        ///	matching the source/destination side actions. Only one action (or
        ///	none) will be offered here.
        ///
        ///	This event can be emitted multiple times during the drag-and-drop
        ///	operation in response to destination side action changes through
        ///	wl_data_offer.set_actions.
        ///
        ///	This event will no longer be emitted after wl_data_device.drop
        ///	happened on the drag-and-drop destination, the client must
        ///	honor the last action received, or the last preferred one set
        ///	through wl_data_offer.set_actions when handling an "ask" action.
        ///
        ///	Compositors may also change the selected action on the fly, mainly
        ///	in response to keyboard modifier changes during the drag-and-drop
        ///	operation.
        ///
        ///	The most recent action received is always the valid one. Prior to
        ///	receiving wl_data_device.drop, the chosen action may change (e.g.
        ///	due to keyboard modifiers being pressed). At the time of receiving
        ///	wl_data_device.drop the drag-and-drop destination must honor the
        ///	last action received.
        ///
        ///	Action changes may still happen after wl_data_device.drop,
        ///	especially on "ask" actions, where the drag-and-drop destination
        ///	may choose another action afterwards. Action changes happening
        ///	at this stage are always the result of inter-client negotiation, the
        ///	compositor shall no longer be able to induce a different action.
        ///
        ///	Upon "ask" actions, it is expected that the drag-and-drop destination
        ///	may potentially choose different a different action and/or mime type,
        ///	based on wl_data_offer.source_actions and finally chosen by the
        ///	user (e.g. popping up a menu with the available options). The
        ///	final wl_data_offer.set_actions and wl_data_offer.accept requests
        ///	must happen before the call to wl_data_offer.finish.
        pub fn action(self, con_fd: RawFd, dnd_action: u32) -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 2 as u32;

            let mut ind = 2;

            buf[ind] = dnd_action as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl DataSource {
        /// Event: a target accepts an offered mime type
        ///
        /// Sent when a target accepts pointer_focus or motion events.  If
        ///	a target does not accept any of the offered types, type is NULL.
        ///
        ///	Used for feedback during drag-and-drop.
        pub fn target(self,
                      con_fd: RawFd,
                      mime_type: Array /* placeholder for string */)
                      -> Result<(), NixError> {
            let mut len = 3;
            len += if mime_type.contents.len() % 4 == 0 {
                mime_type.contents.len() / 4
            } else {
                1 + (mime_type.contents.len() / 4)
            };
            let mut vec_buf = Vec::with_capacity(len);
            unsafe { vec_buf.set_len(len) };
            let mut buf: Box<[u32]> = vec_buf.into_boxed_slice();

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            buf[ind] = mime_type.contents.len() as u32;
            unsafe {
                let dst_ptr = buf[ind + 1..].as_mut_ptr() as *mut u8;
                let src_ptr = mime_type.contents.as_ptr();
                ::std::ptr::copy_nonoverlapping(src_ptr, dst_ptr, mime_type.contents.len());
            };
            if mime_type.contents.len() % 4 == 0 {
                ind += 1 + (mime_type.contents.len() / 4);
            } else {
                ind += 2 + (mime_type.contents.len() / 4);
            }

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: send the data
        ///
        /// Request for data from the client.  Send the data as the
        ///	specified mime type over the passed file descriptor, then
        ///	close it.
        pub fn send(self,
                    con_fd: RawFd,
                    mime_type: Array, // placeholder for string
                    fd: RawFd)
                    -> Result<(), NixError> {
            let mut len = 3;
            len += if mime_type.contents.len() % 4 == 0 {
                mime_type.contents.len() / 4
            } else {
                1 + (mime_type.contents.len() / 4)
            };
            let mut vec_buf = Vec::with_capacity(len);
            unsafe { vec_buf.set_len(len) };
            let mut buf: Box<[u32]> = vec_buf.into_boxed_slice();

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 1 as u32;

            let mut ind = 2;

            buf[ind] = mime_type.contents.len() as u32;
            unsafe {
                let dst_ptr = buf[ind + 1..].as_mut_ptr() as *mut u8;
                let src_ptr = mime_type.contents.as_ptr();
                ::std::ptr::copy_nonoverlapping(src_ptr, dst_ptr, mime_type.contents.len());
            };
            if mime_type.contents.len() % 4 == 0 {
                ind += 1 + (mime_type.contents.len() / 4);
            } else {
                ind += 2 + (mime_type.contents.len() / 4);
            }

            let fds = [fd];
            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            let iov = [IoVec::from_slice(buf_8)];
            let cmsg = ControlMessage::ScmRights(&fds);
            sendmsg(con_fd, &iov, &[cmsg], MsgFlags::empty(), None).map(|x| ())
        }

        /// Event: selection was cancelled
        ///
        /// This data source is no longer valid. There are several reasons why
        ///	this could happen:
        ///
        ///	- The data source has been replaced by another data source.
        ///	- The drag-and-drop operation was performed, but the drop destination
        ///	  did not accept any of the mimetypes offered through
        ///	  wl_data_source.target.
        ///	- The drag-and-drop operation was performed, but the drop destination
        ///	  did not select any of the actions present in the mask offered through
        ///	  wl_data_source.action.
        ///	- The drag-and-drop operation was performed but didn't happen over a
        ///	  surface.
        ///	- The compositor cancelled the drag-and-drop operation (e.g. compositor
        ///	  dependent timeouts to avoid stale drag-and-drop transfers).
        ///
        ///	The client should clean up and destroy this data source.
        ///
        ///	For objects of version 2 or older, wl_data_source.cancelled will
        ///	only be emitted if the data source was replaced by another data
        ///	source.
        pub fn cancelled(self, con_fd: RawFd) -> Result<(), NixError> {
            let len = 2;
            let mut buf: [u32; 2] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 2 as u32;

            let mut ind = 2;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: the drag-and-drop operation physically finished
        ///
        /// The user performed the drop action. This event does not indicate
        ///	acceptance, wl_data_source.cancelled may still be emitted afterwards
        ///	if the drop destination does not accept any mimetype.
        ///
        ///	However, this event might however not be received if the compositor
        ///	cancelled the drag-and-drop operation before this event could happen.
        ///
        ///	Note that the data_source may still be used in the future and should
        ///	not be destroyed here.
        pub fn dnd_drop_performed(self, con_fd: RawFd) -> Result<(), NixError> {
            let len = 2;
            let mut buf: [u32; 2] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 3 as u32;

            let mut ind = 2;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: the drag-and-drop operation concluded
        ///
        /// The drop destination finished interoperating with this data
        ///	source, so the client is now free to destroy this data source and
        ///	free all associated data.
        ///
        ///	If the action used to perform the operation was "move", the
        ///	source can now delete the transferred data.
        pub fn dnd_finished(self, con_fd: RawFd) -> Result<(), NixError> {
            let len = 2;
            let mut buf: [u32; 2] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 4 as u32;

            let mut ind = 2;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: notify the selected action
        ///
        /// This event indicates the action selected by the compositor after
        ///	matching the source/destination side actions. Only one action (or
        ///	none) will be offered here.
        ///
        ///	This event can be emitted multiple times during the drag-and-drop
        ///	operation, mainly in response to destination side changes through
        ///	wl_data_offer.set_actions, and as the data device enters/leaves
        ///	surfaces.
        ///
        ///	It is only possible to receive this event after
        ///	wl_data_source.dnd_drop_performed if the drag-and-drop operation
        ///	ended in an "ask" action, in which case the final wl_data_source.action
        ///	event will happen immediately before wl_data_source.dnd_finished.
        ///
        ///	Compositors may also change the selected action on the fly, mainly
        ///	in response to keyboard modifier changes during the drag-and-drop
        ///	operation.
        ///
        ///	The most recent action received is always the valid one. The chosen
        ///	action may change alongside negotiation (e.g. an "ask" action can turn
        ///	into a "move" operation), so the effects of the final action must
        ///	always be applied in wl_data_offer.dnd_finished.
        ///
        ///	Clients can trigger cursor surface changes from this point, so
        ///	they reflect the current action.
        pub fn action(self, con_fd: RawFd, dnd_action: u32) -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 5 as u32;

            let mut ind = 2;

            buf[ind] = dnd_action as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl DataDevice {
        /// Event: introduce a new wl_data_offer
        ///
        /// The data_offer event introduces a new wl_data_offer object,
        ///	which will subsequently be used in either the
        ///	data_device.enter event (for drag-and-drop) or the
        ///	data_device.selection event (for selections).  Immediately
        ///	following the data_device_data_offer event, the new data_offer
        ///	object will send out data_offer.offer events to describe the
        ///	mime types it offers.
        pub fn data_offer(self,
                          con_fd: RawFd,
                          id: New<interfaces::DataOffer>)
                          -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            buf[ind] = (id.0).id();
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: initiate drag-and-drop session
        ///
        /// This event is sent when an active drag-and-drop pointer enters
        ///	a surface owned by the client.  The position of the pointer at
        ///	enter time is provided by the x and y arguments, in surface
        ///	local coordinates.
        pub fn enter(self,
                     con_fd: RawFd,
                     serial: u32,
                     surface: interfaces::Surface,
                     x: Fixed,
                     y: Fixed,
                     id: interfaces::DataOffer)
                     -> Result<(), NixError> {
            let len = 7;
            let mut buf: [u32; 7] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 1 as u32;

            let mut ind = 2;

            buf[ind] = serial as u32;
            ind += 1;

            buf[ind] = surface.id();
            ind += 1;

            buf[ind] = x.0;
            ind += 1;

            buf[ind] = y.0;
            ind += 1;

            buf[ind] = id.id();
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: end drag-and-drop session
        ///
        /// This event is sent when the drag-and-drop pointer leaves the
        ///	surface and the session ends.  The client must destroy the
        ///	wl_data_offer introduced at enter time at this point.
        pub fn leave(self, con_fd: RawFd) -> Result<(), NixError> {
            let len = 2;
            let mut buf: [u32; 2] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 2 as u32;

            let mut ind = 2;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: drag-and-drop session motion
        ///
        /// This event is sent when the drag-and-drop pointer moves within
        ///	the currently focused surface. The new position of the pointer
        ///	is provided by the x and y arguments, in surface local
        ///	coordinates.
        pub fn motion(self, con_fd: RawFd, time: u32, x: Fixed, y: Fixed) -> Result<(), NixError> {
            let len = 5;
            let mut buf: [u32; 5] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 3 as u32;

            let mut ind = 2;

            buf[ind] = time as u32;
            ind += 1;

            buf[ind] = x.0;
            ind += 1;

            buf[ind] = y.0;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: end drag-and-drag session successfully
        ///
        /// The event is sent when a drag-and-drop operation is ended
        ///	because the implicit grab is removed.
        ///
        ///	The drag-and-drop destination is expected to honor the last action
        ///	received through wl_data_offer.action, if the resulting action is
        ///	"copy" or "move", the destination can still perform
        ///	wl_data_offer.receive requests, and is expected to end all
        ///	transfers with a wl_data_offer.finish request.
        ///
        ///	If the resulting action is "ask", the action will not be considered
        ///	final. The drag-and-drop destination is expected to perform one last
        ///	wl_data_offer.set_actions request, or wl_data_offer.destroy in order
        ///	to cancel the operation.
        pub fn drop(self, con_fd: RawFd) -> Result<(), NixError> {
            let len = 2;
            let mut buf: [u32; 2] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 4 as u32;

            let mut ind = 2;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: advertise new selection
        ///
        /// The selection event is sent out to notify the client of a new
        ///	wl_data_offer for the selection for this device.  The
        ///	data_device.data_offer and the data_offer.offer events are
        ///	sent out immediately before this event to introduce the data
        ///	offer object.  The selection event is sent to a client
        ///	immediately before receiving keyboard focus and when a new
        ///	selection is set while the client has keyboard focus.  The
        ///	data_offer is valid until a new data_offer or NULL is received
        ///	or until the client loses keyboard focus.  The client must
        ///	destroy the previous selection data_offer, if any, upon receiving
        ///	this event.
        pub fn selection(self, con_fd: RawFd, id: interfaces::DataOffer) -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 5 as u32;

            let mut ind = 2;

            buf[ind] = id.id();
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl DataDeviceManager {}
    #[allow(unused_mut, unused_variables)]
    impl Shell {}
    #[allow(unused_mut, unused_variables)]
    impl ShellSurface {
        /// Event: ping client
        ///
        /// Ping a client to check if it is receiving events and sending
        ///	requests. A client is expected to reply with a pong request.
        pub fn ping(self, con_fd: RawFd, serial: u32) -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            buf[ind] = serial as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: suggest resize
        ///
        /// The configure event asks the client to resize its surface.
        ///
        ///	The size is a hint, in the sense that the client is free to
        ///	ignore it if it doesn't resize, pick a smaller size (to
        ///	satisfy aspect ratio or resize in steps of NxM pixels).
        ///
        ///	The edges parameter provides a hint about how the surface
        ///	was resized. The client may use this information to decide
        ///	how to adjust its content to the new size (e.g. a scrolling
        ///	area might adjust its content position to leave the viewable
        ///	content unmoved).
        ///
        ///	The client is free to dismiss all but the last configure
        ///	event it received.
        ///
        ///	The width and height arguments specify the size of the window
        ///	in surface local coordinates.
        pub fn configure(self,
                         con_fd: RawFd,
                         edges: ::interfaces::shell_surface::resize,
                         width: i32,
                         height: i32)
                         -> Result<(), NixError> {
            let len = 5;
            let mut buf: [u32; 5] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 1 as u32;

            let mut ind = 2;

            buf[ind] = edges.0 as u32;
            ind += 1;

            buf[ind] = width as u32;
            ind += 1;

            buf[ind] = height as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: popup interaction is done
        ///
        /// The popup_done event is sent out when a popup grab is broken,
        ///	that is, when the user clicks a surface that doesn't belong
        ///	to the client owning the popup surface.
        pub fn popup_done(self, con_fd: RawFd) -> Result<(), NixError> {
            let len = 2;
            let mut buf: [u32; 2] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 2 as u32;

            let mut ind = 2;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl Surface {
        /// Event: surface enters an output
        ///
        /// This is emitted whenever a surface's creation, movement, or resizing
        ///	results in some part of it being within the scanout region of an
        ///	output.
        ///
        ///	Note that a surface may be overlapping with zero or more outputs.
        pub fn enter(self, con_fd: RawFd, output: interfaces::Output) -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            buf[ind] = output.id();
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: surface leaves an output
        ///
        /// This is emitted whenever a surface's creation, movement, or resizing
        ///	results in it no longer having any part of it within the scanout region
        ///	of an output.
        pub fn leave(self, con_fd: RawFd, output: interfaces::Output) -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 1 as u32;

            let mut ind = 2;

            buf[ind] = output.id();
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl Seat {
        /// Event: seat capabilities changed
        ///
        /// This is emitted whenever a seat gains or loses the pointer,
        ///	keyboard or touch capabilities.  The argument is a capability
        ///	enum containing the complete set of capabilities this seat has.
        ///
        ///	When the pointer capability is added, a client may create a
        ///	wl_pointer object using the wl_seat.get_pointer request. This object
        ///	will receive pointer events until the capability is removed in the
        ///	future.
        ///
        ///	When the pointer capability is removed, a client should destroy the
        ///	wl_pointer objects associated with the seat where the capability was
        ///	removed, using the wl_pointer.release request. No further pointer
        ///	events will be received on these objects.
        ///
        ///	In some compositors, if a seat regains the pointer capability and a
        ///	client has a previously obtained wl_pointer object of version 4 or
        ///	less, that object may start sending pointer events again. This
        ///	behavior is considered a misinterpretation of the intended behavior
        ///	and must not be relied upon by the client. wl_pointer objects of
        ///	version 5 or later must not send events if created before the most
        ///	recent event notifying the client of an added pointer capability.
        ///
        ///	The above behavior also applies to wl_keyboard and wl_touch with the
        ///	keyboard and touch capabilities, respectively.
        pub fn capabilities(self,
                            con_fd: RawFd,
                            capabilities: ::interfaces::seat::capability)
                            -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            buf[ind] = capabilities.0 as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: unique identifier for this seat
        ///
        /// In a multiseat configuration this can be used by the client to help
        ///	identify which physical devices the seat represents. Based on
        ///	the seat configuration used by the compositor.
        pub fn name(self,
                    con_fd: RawFd,
                    name: Array /* placeholder for string */)
                    -> Result<(), NixError> {
            let mut len = 3;
            len += if name.contents.len() % 4 == 0 {
                name.contents.len() / 4
            } else {
                1 + (name.contents.len() / 4)
            };
            let mut vec_buf = Vec::with_capacity(len);
            unsafe { vec_buf.set_len(len) };
            let mut buf: Box<[u32]> = vec_buf.into_boxed_slice();

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 1 as u32;

            let mut ind = 2;

            buf[ind] = name.contents.len() as u32;
            unsafe {
                let dst_ptr = buf[ind + 1..].as_mut_ptr() as *mut u8;
                let src_ptr = name.contents.as_ptr();
                ::std::ptr::copy_nonoverlapping(src_ptr, dst_ptr, name.contents.len());
            };
            if name.contents.len() % 4 == 0 {
                ind += 1 + (name.contents.len() / 4);
            } else {
                ind += 2 + (name.contents.len() / 4);
            }

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl Pointer {
        /// Event: enter event
        ///
        /// Notification that this seat's pointer is focused on a certain
        ///	surface.
        ///
        ///	When an seat's focus enters a surface, the pointer image
        ///	is undefined and a client should respond to this event by setting
        ///	an appropriate pointer image with the set_cursor request.
        pub fn enter(self,
                     con_fd: RawFd,
                     serial: u32,
                     surface: interfaces::Surface,
                     surface_x: Fixed,
                     surface_y: Fixed)
                     -> Result<(), NixError> {
            let len = 6;
            let mut buf: [u32; 6] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            buf[ind] = serial as u32;
            ind += 1;

            buf[ind] = surface.id();
            ind += 1;

            buf[ind] = surface_x.0;
            ind += 1;

            buf[ind] = surface_y.0;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: leave event
        ///
        /// Notification that this seat's pointer is no longer focused on
        ///	a certain surface.
        ///
        ///	The leave notification is sent before the enter notification
        ///	for the new focus.
        pub fn leave(self,
                     con_fd: RawFd,
                     serial: u32,
                     surface: interfaces::Surface)
                     -> Result<(), NixError> {
            let len = 4;
            let mut buf: [u32; 4] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 1 as u32;

            let mut ind = 2;

            buf[ind] = serial as u32;
            ind += 1;

            buf[ind] = surface.id();
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: pointer motion event
        ///
        /// Notification of pointer location change. The arguments
        ///	surface_x and surface_y are the location relative to the
        ///	focused surface.
        pub fn motion(self,
                      con_fd: RawFd,
                      time: u32,
                      surface_x: Fixed,
                      surface_y: Fixed)
                      -> Result<(), NixError> {
            let len = 5;
            let mut buf: [u32; 5] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 2 as u32;

            let mut ind = 2;

            buf[ind] = time as u32;
            ind += 1;

            buf[ind] = surface_x.0;
            ind += 1;

            buf[ind] = surface_y.0;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: pointer button event
        ///
        /// Mouse button click and release notifications.
        ///
        ///	The location of the click is given by the last motion or
        ///	enter event.
        ///The time argument is a timestamp with millisecond
        ///granularity, with an undefined base.
        pub fn button(self,
                      con_fd: RawFd,
                      serial: u32,
                      time: u32,
                      button: u32,
                      state: ::interfaces::pointer::button_state)
                      -> Result<(), NixError> {
            let len = 6;
            let mut buf: [u32; 6] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 3 as u32;

            let mut ind = 2;

            buf[ind] = serial as u32;
            ind += 1;

            buf[ind] = time as u32;
            ind += 1;

            buf[ind] = button as u32;
            ind += 1;

            buf[ind] = state.0 as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: axis event
        ///
        /// Scroll and other axis notifications.
        ///
        ///	For scroll events (vertical and horizontal scroll axes), the
        ///	value parameter is the length of a vector along the specified
        ///	axis in a coordinate space identical to those of motion events,
        ///	representing a relative movement along the specified axis.
        ///
        ///	For devices that support movements non-parallel to axes multiple
        ///	axis events will be emitted.
        ///
        ///	When applicable, for example for touch pads, the server can
        ///	choose to emit scroll events where the motion vector is
        ///	equivalent to a motion event vector.
        ///
        ///	When applicable, clients can transform its view relative to the
        ///	scroll distance.
        pub fn axis(self,
                    con_fd: RawFd,
                    time: u32,
                    axis: ::interfaces::pointer::axis,
                    value: Fixed)
                    -> Result<(), NixError> {
            let len = 5;
            let mut buf: [u32; 5] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 4 as u32;

            let mut ind = 2;

            buf[ind] = time as u32;
            ind += 1;

            buf[ind] = axis.0 as u32;
            ind += 1;

            buf[ind] = value.0;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: end of a pointer event sequence
        ///
        /// Indicates the end of a set of events that logically belong together.
        ///	A client is expected to accumulate the data in all events within the
        ///	frame before proceeding.
        ///
        ///	All wl_pointer events before a wl_pointer.frame event belong
        ///	logically together. For example, in a diagonal scroll motion the
        ///	compositor will send an optional wl_pointer.axis_source event, two
        ///	wl_pointer.axis events (horizontal and vertical) and finally a
        ///	wl_pointer.frame event. The client may use this information to
        ///	calculate a diagonal vector for scrolling.
        ///
        ///	When multiple wl_pointer.axis events occur within the same frame,
        ///	the motion vector is the combined motion of all events.
        ///	When a wl_pointer.axis and a wl_pointer.axis_stop event occur within
        ///	the same frame, this indicates that axis movement in one axis has
        ///	stopped but continues in the other axis.
        ///	When multiple wl_pointer.axis_stop events occur within in the same
        ///	frame, this indicates that these axes stopped in the same instance.
        ///
        ///	A wl_pointer.frame event is sent for every logical event group,
        ///	even if the group only contains a single wl_pointer event.
        ///	Specifically, a client may get a sequence: motion, frame, button,
        ///	frame, axis, frame, axis_stop, frame.
        ///
        ///	The wl_pointer.enter and wl_pointer.leave events are logical events
        ///	generated by the compositor and not the hardware. These events are
        ///	also grouped by a wl_pointer.frame. When a pointer moves from one
        ///	surface to the another, a compositor should group the
        ///	wl_pointer.leave event within the same wl_pointer.frame.
        ///	However, a client must not rely on wl_pointer.leave and
        ///	wl_pointer.enter being in the same wl_pointer.frame.
        ///	Compositor-specific policies may require the wl_pointer.leave and
        ///	wl_pointer.enter event being split across multiple wl_pointer.frame
        ///	groups.
        pub fn frame(self, con_fd: RawFd) -> Result<(), NixError> {
            let len = 2;
            let mut buf: [u32; 2] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 5 as u32;

            let mut ind = 2;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: axis source event
        ///
        /// Source information for scroll and other axes.
        ///
        ///	This event does not occur on its own. It is sent before a
        ///	wl_pointer.frame event and carries the source information for
        ///	all events within that frame.
        ///
        ///	The source specifies how this event was generated. If the source is
        ///	wl_pointer.axis_source.finger, a wl_pointer.axis_stop event will be
        ///	sent when the user lifts the finger off the device.
        ///
        ///	If the source is wl_pointer axis_source.wheel or
        ///	wl_pointer.axis_source.continuous, a wl_pointer.axis_stop event may
        ///	or may not be sent. Whether a compositor sends a axis_stop event
        ///	for these sources is hardware-specific and implementation-dependent;
        ///	clients must not rely on receiving an axis_stop event for these
        ///	scroll sources and should treat scroll sequences from these scroll
        ///	sources as unterminated by default.
        ///
        ///	This event is optional. If the source is unknown for a particular
        ///	axis event sequence, no event is sent.
        ///	Only one wl_pointer.axis_source event is permitted per frame.
        ///
        ///	The order of wl_pointer.axis_discrete and wl_pointer.axis_source is
        ///	not guaranteed.
        pub fn axis_source(self,
                           con_fd: RawFd,
                           axis_source: ::interfaces::pointer::axis_source)
                           -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 6 as u32;

            let mut ind = 2;

            buf[ind] = axis_source.0 as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: axis stop event
        ///
        /// Stop notification for scroll and other axes.
        ///
        ///	For some wl_pointer.axis_source types, a wl_pointer.axis_stop event
        ///	is sent to notify a client that the axis sequence has terminated.
        ///	This enables the client to implement kinetic scrolling.
        ///	See the wl_pointer.axis_source documentation for information on when
        ///	this event may be generated.
        ///
        ///	Any wl_pointer.axis events with the same axis_source after this
        ///	event should be considered as the start of a new axis motion.
        ///
        ///	The timestamp is to be interpreted identical to the timestamp in the
        ///	wl_pointer.axis event. The timestamp value may be the same as a
        ///	preceeding wl_pointer.axis event.
        pub fn axis_stop(self,
                         con_fd: RawFd,
                         time: u32,
                         axis: ::interfaces::pointer::axis)
                         -> Result<(), NixError> {
            let len = 4;
            let mut buf: [u32; 4] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 7 as u32;

            let mut ind = 2;

            buf[ind] = time as u32;
            ind += 1;

            buf[ind] = axis.0 as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: axis click event
        ///
        /// Discrete step information for scroll and other axes.
        ///
        ///	This event carries the axis value of the wl_pointer.axis event in
        ///	discrete steps (e.g. mouse wheel clicks).
        ///
        ///	This event does not occur on its own, it is coupled with a
        ///	wl_pointer.axis event that represents this axis value on a
        ///	continuous scale. The protocol guarantees that each axis_discrete
        ///	event is always followed by exactly one axis event with the same
        ///	axis number within the same wl_pointer.frame. Note that the protocol
        ///	allows for other events to occur between the axis_discrete and
        ///	its coupled axis event, including other axis_discrete or axis
        ///	events.
        ///
        ///	This event is optional; continuous scrolling devices
        ///	like two-finger scrolling on touchpads do not have discrete
        ///	steps and do not generate this event.
        ///
        ///	The discrete value carries the directional information. e.g. a value
        ///	of -2 is two steps towards the negative direction of this axis.
        ///
        ///	The axis number is identical to the axis number in the associate
        ///	axis event.
        ///
        ///	The order of wl_pointer.axis_discrete and wl_pointer.axis_source is
        ///	not guaranteed.
        pub fn axis_discrete(self,
                             con_fd: RawFd,
                             axis: ::interfaces::pointer::axis,
                             discrete: i32)
                             -> Result<(), NixError> {
            let len = 4;
            let mut buf: [u32; 4] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 8 as u32;

            let mut ind = 2;

            buf[ind] = axis.0 as u32;
            ind += 1;

            buf[ind] = discrete as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl Keyboard {
        /// Event: keyboard mapping
        ///
        /// This event provides a file descriptor to the client which can be
        ///	memory-mapped to provide a keyboard mapping description.
        pub fn keymap(self,
                      con_fd: RawFd,
                      format: ::interfaces::keyboard::keymap_format,
                      fd: RawFd,
                      size: u32)
                      -> Result<(), NixError> {
            let len = 4;
            let mut buf: [u32; 4] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            buf[ind] = format.0 as u32;
            ind += 1;

            let fds = [fd];
            buf[ind] = size as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            let iov = [IoVec::from_slice(buf_8)];
            let cmsg = ControlMessage::ScmRights(&fds);
            sendmsg(con_fd, &iov, &[cmsg], MsgFlags::empty(), None).map(|x| ())
        }

        /// Event: enter event
        ///
        /// Notification that this seat's keyboard focus is on a certain
        ///	surface.
        pub fn enter(self,
                     con_fd: RawFd,
                     serial: u32,
                     surface: interfaces::Surface,
                     keys: Array)
                     -> Result<(), NixError> {
            let mut len = 5;
            len += if keys.contents.len() % 4 == 0 {
                keys.contents.len() / 4
            } else {
                1 + (keys.contents.len() / 4)
            };
            let mut vec_buf = Vec::with_capacity(len);
            unsafe { vec_buf.set_len(len) };
            let mut buf: Box<[u32]> = vec_buf.into_boxed_slice();

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 1 as u32;

            let mut ind = 2;

            buf[ind] = serial as u32;
            ind += 1;

            buf[ind] = surface.id();
            ind += 1;

            buf[ind] = keys.contents.len() as u32;
            unsafe {
                let dst_ptr = buf[ind + 1..].as_mut_ptr() as *mut u8;
                let src_ptr = keys.contents.as_ptr();
                ::std::ptr::copy_nonoverlapping(src_ptr, dst_ptr, keys.contents.len());
            };
            if keys.contents.len() % 4 == 0 {
                ind += 1 + (keys.contents.len() / 4);
            } else {
                ind += 2 + (keys.contents.len() / 4);
            }

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: leave event
        ///
        /// Notification that this seat's keyboard focus is no longer on
        ///	a certain surface.
        ///
        ///	The leave notification is sent before the enter notification
        ///	for the new focus.
        pub fn leave(self,
                     con_fd: RawFd,
                     serial: u32,
                     surface: interfaces::Surface)
                     -> Result<(), NixError> {
            let len = 4;
            let mut buf: [u32; 4] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 2 as u32;

            let mut ind = 2;

            buf[ind] = serial as u32;
            ind += 1;

            buf[ind] = surface.id();
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: key event
        ///
        /// A key was pressed or released.
        ///The time argument is a timestamp with millisecond
        ///granularity, with an undefined base.
        pub fn key(self,
                   con_fd: RawFd,
                   serial: u32,
                   time: u32,
                   key: u32,
                   state: ::interfaces::keyboard::key_state)
                   -> Result<(), NixError> {
            let len = 6;
            let mut buf: [u32; 6] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 3 as u32;

            let mut ind = 2;

            buf[ind] = serial as u32;
            ind += 1;

            buf[ind] = time as u32;
            ind += 1;

            buf[ind] = key as u32;
            ind += 1;

            buf[ind] = state.0 as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: modifier and group state
        ///
        /// Notifies clients that the modifier and/or group state has
        ///	changed, and it should update its local state.
        pub fn modifiers(self,
                         con_fd: RawFd,
                         serial: u32,
                         mods_depressed: u32,
                         mods_latched: u32,
                         mods_locked: u32,
                         group: u32)
                         -> Result<(), NixError> {
            let len = 7;
            let mut buf: [u32; 7] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 4 as u32;

            let mut ind = 2;

            buf[ind] = serial as u32;
            ind += 1;

            buf[ind] = mods_depressed as u32;
            ind += 1;

            buf[ind] = mods_latched as u32;
            ind += 1;

            buf[ind] = mods_locked as u32;
            ind += 1;

            buf[ind] = group as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: repeat rate and delay
        ///
        /// Informs the client about the keyboard's repeat rate and delay.
        ///
        ///This event is sent as soon as the wl_keyboard object has been created,
        ///and is guaranteed to be received by the client before any key press
        ///event.
        ///
        ///Negative values for either rate or delay are illegal. A rate of zero
        ///will disable any repeating (regardless of the value of delay).
        ///
        ///This event can be sent later on as well with a new value if necessary,
        ///so clients should continue listening for the event past the creation
        ///of wl_keyboard.
        pub fn repeat_info(self, con_fd: RawFd, rate: i32, delay: i32) -> Result<(), NixError> {
            let len = 4;
            let mut buf: [u32; 4] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 5 as u32;

            let mut ind = 2;

            buf[ind] = rate as u32;
            ind += 1;

            buf[ind] = delay as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl Touch {
        /// Event: touch down event and beginning of a touch sequence
        ///
        /// A new touch point has appeared on the surface. This touch point is
        ///	assigned a unique @id. Future events from this touchpoint reference
        ///	this ID. The ID ceases to be valid after a touch up event and may be
        ///	re-used in the future.
        pub fn down(self,
                    con_fd: RawFd,
                    serial: u32,
                    time: u32,
                    surface: interfaces::Surface,
                    id: i32,
                    x: Fixed,
                    y: Fixed)
                    -> Result<(), NixError> {
            let len = 8;
            let mut buf: [u32; 8] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            buf[ind] = serial as u32;
            ind += 1;

            buf[ind] = time as u32;
            ind += 1;

            buf[ind] = surface.id();
            ind += 1;

            buf[ind] = id as u32;
            ind += 1;

            buf[ind] = x.0;
            ind += 1;

            buf[ind] = y.0;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: end of a touch event sequence
        ///
        /// The touch point has disappeared. No further events will be sent for
        ///	this touchpoint and the touch point's ID is released and may be
        ///	re-used in a future touch down event.
        pub fn up(self, con_fd: RawFd, serial: u32, time: u32, id: i32) -> Result<(), NixError> {
            let len = 5;
            let mut buf: [u32; 5] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 1 as u32;

            let mut ind = 2;

            buf[ind] = serial as u32;
            ind += 1;

            buf[ind] = time as u32;
            ind += 1;

            buf[ind] = id as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: update of touch point coordinates
        ///
        /// A touchpoint has changed coordinates.
        pub fn motion(self,
                      con_fd: RawFd,
                      time: u32,
                      id: i32,
                      x: Fixed,
                      y: Fixed)
                      -> Result<(), NixError> {
            let len = 6;
            let mut buf: [u32; 6] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 2 as u32;

            let mut ind = 2;

            buf[ind] = time as u32;
            ind += 1;

            buf[ind] = id as u32;
            ind += 1;

            buf[ind] = x.0;
            ind += 1;

            buf[ind] = y.0;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: end of touch frame event
        ///
        /// Indicates the end of a contact point list.
        pub fn frame(self, con_fd: RawFd) -> Result<(), NixError> {
            let len = 2;
            let mut buf: [u32; 2] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 3 as u32;

            let mut ind = 2;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: touch session cancelled
        ///
        /// Sent if the compositor decides the touch stream is a global
        ///	gesture. No further events are sent to the clients from that
        ///	particular gesture. Touch cancellation applies to all touch points
        ///	currently active on this client's surface. The client is
        ///	responsible for finalizing the touch points, future touch points on
        ///	this surface may re-use the touch point ID.
        pub fn cancel(self, con_fd: RawFd) -> Result<(), NixError> {
            let len = 2;
            let mut buf: [u32; 2] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 4 as u32;

            let mut ind = 2;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl Output {
        /// Event: properties of the output
        ///
        /// The geometry event describes geometric properties of the output.
        ///	The event is sent when binding to the output object and whenever
        ///	any of the properties change.
        pub fn geometry(self,
                        con_fd: RawFd,
                        x: i32,
                        y: i32,
                        physical_width: i32,
                        physical_height: i32,
                        subpixel: ::interfaces::output::subpixel,
                        make: Array, // placeholder for string
                        model: Array, // placeholder for string
                        transform: ::interfaces::output::transform)
                        -> Result<(), NixError> {
            let mut len = 10;
            len += if make.contents.len() % 4 == 0 {
                make.contents.len() / 4
            } else {
                1 + (make.contents.len() / 4)
            };
            len += if model.contents.len() % 4 == 0 {
                model.contents.len() / 4
            } else {
                1 + (model.contents.len() / 4)
            };
            let mut vec_buf = Vec::with_capacity(len);
            unsafe { vec_buf.set_len(len) };
            let mut buf: Box<[u32]> = vec_buf.into_boxed_slice();

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            buf[ind] = x as u32;
            ind += 1;

            buf[ind] = y as u32;
            ind += 1;

            buf[ind] = physical_width as u32;
            ind += 1;

            buf[ind] = physical_height as u32;
            ind += 1;

            buf[ind] = subpixel.0 as u32;
            ind += 1;

            buf[ind] = make.contents.len() as u32;
            unsafe {
                let dst_ptr = buf[ind + 1..].as_mut_ptr() as *mut u8;
                let src_ptr = make.contents.as_ptr();
                ::std::ptr::copy_nonoverlapping(src_ptr, dst_ptr, make.contents.len());
            };
            if make.contents.len() % 4 == 0 {
                ind += 1 + (make.contents.len() / 4);
            } else {
                ind += 2 + (make.contents.len() / 4);
            }

            buf[ind] = model.contents.len() as u32;
            unsafe {
                let dst_ptr = buf[ind + 1..].as_mut_ptr() as *mut u8;
                let src_ptr = model.contents.as_ptr();
                ::std::ptr::copy_nonoverlapping(src_ptr, dst_ptr, model.contents.len());
            };
            if model.contents.len() % 4 == 0 {
                ind += 1 + (model.contents.len() / 4);
            } else {
                ind += 2 + (model.contents.len() / 4);
            }

            buf[ind] = transform.0 as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: advertise available modes for the output
        ///
        /// The mode event describes an available mode for the output.
        ///
        ///	The event is sent when binding to the output object and there
        ///	will always be one mode, the current mode.  The event is sent
        ///	again if an output changes mode, for the mode that is now
        ///	current.  In other words, the current mode is always the last
        ///	mode that was received with the current flag set.
        ///
        ///	The size of a mode is given in physical hardware units of
        ///the output device. This is not necessarily the same as
        ///the output size in the global compositor space. For instance,
        ///the output may be scaled, as described in wl_output.scale,
        ///or transformed , as described in wl_output.transform.
        pub fn mode(self,
                    con_fd: RawFd,
                    flags: ::interfaces::output::mode,
                    width: i32,
                    height: i32,
                    refresh: i32)
                    -> Result<(), NixError> {
            let len = 6;
            let mut buf: [u32; 6] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 1 as u32;

            let mut ind = 2;

            buf[ind] = flags.0 as u32;
            ind += 1;

            buf[ind] = width as u32;
            ind += 1;

            buf[ind] = height as u32;
            ind += 1;

            buf[ind] = refresh as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: sent all information about output
        ///
        /// This event is sent after all other properties has been
        ///sent after binding to the output object and after any
        ///other property changes done after that. This allows
        ///changes to the output properties to be seen as
        ///atomic, even if they happen via multiple events.
        pub fn done(self, con_fd: RawFd) -> Result<(), NixError> {
            let len = 2;
            let mut buf: [u32; 2] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 2 as u32;

            let mut ind = 2;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Event: output scaling properties
        ///
        /// This event contains scaling geometry information
        ///that is not in the geometry event. It may be sent after
        ///binding the output object or if the output scale changes
        ///later. If it is not sent, the client should assume a
        ///	scale of 1.
        ///
        ///	A scale larger than 1 means that the compositor will
        ///	automatically scale surface buffers by this amount
        ///	when rendering. This is used for very high resolution
        ///	displays where applications rendering at the native
        ///	resolution would be too small to be legible.
        ///
        ///	It is intended that scaling aware clients track the
        ///	current output of a surface, and if it is on a scaled
        ///	output it should use wl_surface.set_buffer_scale with
        ///	the scale of the output. That way the compositor can
        ///	avoid scaling the surface, and the client can supply
        ///	a higher detail image.
        pub fn scale(self, con_fd: RawFd, factor: i32) -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 3 as u32;

            let mut ind = 2;

            buf[ind] = factor as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl Region {}
    #[allow(unused_mut, unused_variables)]
    impl Subcompositor {}
    #[allow(unused_mut, unused_variables)]
    impl Subsurface {}
    #[allow(unused_mut, unused_variables)]
    impl Display {
        /// Request: asynchronous roundtrip
        ///
        /// The sync request asks the server to emit the 'done' event
        ///	on the returned wl_callback object.  Since requests are
        ///	handled in-order and events are delivered in-order, this can
        ///	be used as a barrier to ensure all previous requests and the
        ///	resulting events have been handled.
        ///
        ///	The object returned by this request will be destroyed by the
        ///	compositor after the callback is fired and as such the client must not
        ///	attempt to use it after that point.
        ///
        ///	The callback_data passed in the callback is the event serial.
        pub fn sync(self,
                    con_fd: RawFd,
                    callback: New<interfaces::Callback>)
                    -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            buf[ind] = (callback.0).id();
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: get global registry object
        ///
        /// This request creates a registry object that allows the client
        ///	to list and bind the global objects available from the
        ///	compositor.
        pub fn get_registry(self,
                            con_fd: RawFd,
                            registry: New<interfaces::Registry>)
                            -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 1 as u32;

            let mut ind = 2;

            buf[ind] = (registry.0).id();
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl Registry {
        /// Request: bind an object to the display
        ///
        /// Binds a new, client-created object to the server using the
        ///specified name as the identifier.
        pub fn bind(self,
                    con_fd: RawFd,
                    name: u32,
                    interface: Array, // placeholder for string
                    version: u32,
                    id: New<Interface>)
                    -> Result<(), NixError> {
            let mut len = 6;
            len += if interface.contents.len() % 4 == 0 {
                interface.contents.len() / 4
            } else {
                1 + (interface.contents.len() / 4)
            };
            let mut vec_buf = Vec::with_capacity(len);
            unsafe { vec_buf.set_len(len) };
            let mut buf: Box<[u32]> = vec_buf.into_boxed_slice();

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            buf[ind] = name as u32;
            ind += 1;

            buf[ind] = interface.contents.len() as u32;
            unsafe {
                let dst_ptr = buf[ind + 1..].as_mut_ptr() as *mut u8;
                let src_ptr = interface.contents.as_ptr();
                ::std::ptr::copy_nonoverlapping(src_ptr, dst_ptr, interface.contents.len());
            };
            if interface.contents.len() % 4 == 0 {
                ind += 1 + (interface.contents.len() / 4);
            } else {
                ind += 2 + (interface.contents.len() / 4);
            }

            buf[ind] = version as u32;
            ind += 1;

            buf[ind] = (id.0).id();
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl Callback {}
    #[allow(unused_mut, unused_variables)]
    impl Compositor {
        /// Request: create new surface
        ///
        /// Ask the compositor to create a new surface.
        pub fn create_surface(self,
                              con_fd: RawFd,
                              id: New<interfaces::Surface>)
                              -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            buf[ind] = (id.0).id();
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: create new region
        ///
        /// Ask the compositor to create a new region.
        pub fn create_region(self,
                             con_fd: RawFd,
                             id: New<interfaces::Region>)
                             -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 1 as u32;

            let mut ind = 2;

            buf[ind] = (id.0).id();
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl ShmPool {
        /// Request: create a buffer from the pool
        ///
        /// Create a wl_buffer object from the pool.
        ///
        ///	The buffer is created offset bytes into the pool and has
        ///	width and height as specified.  The stride arguments specifies
        ///	the number of bytes from beginning of one row to the beginning
        ///	of the next.  The format is the pixel format of the buffer and
        ///	must be one of those advertised through the wl_shm.format event.
        ///
        ///	A buffer will keep a reference to the pool it was created from
        ///	so it is valid to destroy the pool immediately after creating
        ///	a buffer from it.
        pub fn create_buffer(self,
                             con_fd: RawFd,
                             id: New<interfaces::Buffer>,
                             offset: i32,
                             width: i32,
                             height: i32,
                             stride: i32,
                             format: u32)
                             -> Result<(), NixError> {
            let len = 8;
            let mut buf: [u32; 8] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            buf[ind] = (id.0).id();
            ind += 1;

            buf[ind] = offset as u32;
            ind += 1;

            buf[ind] = width as u32;
            ind += 1;

            buf[ind] = height as u32;
            ind += 1;

            buf[ind] = stride as u32;
            ind += 1;

            buf[ind] = format as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: destroy the pool
        ///
        /// Destroy the shared memory pool.
        ///
        ///	The mmapped memory will be released when all
        ///	buffers that have been created from this pool
        ///	are gone.
        pub fn destroy(self, con_fd: RawFd) -> Result<(), NixError> {
            let len = 2;
            let mut buf: [u32; 2] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 1 as u32;

            let mut ind = 2;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: change the size of the pool mapping
        ///
        /// This request will cause the server to remap the backing memory
        ///	for the pool from the file descriptor passed when the pool was
        ///	created, but using the new size.  This request can only be
        ///	used to make the pool bigger.
        pub fn resize(self, con_fd: RawFd, size: i32) -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 2 as u32;

            let mut ind = 2;

            buf[ind] = size as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl Shm {
        /// Request: create a shm pool
        ///
        /// Create a new wl_shm_pool object.
        ///
        ///	The pool can be used to create shared memory based buffer
        ///	objects.  The server will mmap size bytes of the passed file
        ///descriptor, to use as backing memory for the pool.
        pub fn create_pool(self,
                           con_fd: RawFd,
                           id: New<interfaces::ShmPool>,
                           fd: RawFd,
                           size: i32)
                           -> Result<(), NixError> {
            let len = 4;
            let mut buf: [u32; 4] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            buf[ind] = (id.0).id();
            ind += 1;

            let fds = [fd];
            buf[ind] = size as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            let iov = [IoVec::from_slice(buf_8)];
            let cmsg = ControlMessage::ScmRights(&fds);
            sendmsg(con_fd, &iov, &[cmsg], MsgFlags::empty(), None).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl Buffer {
        /// Request: destroy a buffer
        ///
        /// Destroy a buffer. If and how you need to release the backing
        ///	storage is defined by the buffer factory interface.
        ///
        ///	For possible side-effects to a surface, see wl_surface.attach.
        pub fn destroy(self, con_fd: RawFd) -> Result<(), NixError> {
            let len = 2;
            let mut buf: [u32; 2] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl DataOffer {
        /// Request: accept one of the offered mime types
        ///
        /// Indicate that the client can accept the given mime type, or
        ///	NULL for not accepted.
        ///
        ///	For objects of version 2 or older, this request is used by the
        ///	client to give feedback whether the client can receive the given
        ///	mime type, or NULL if none is accepted; the feedback does not
        ///	determine whether the drag-and-drop operation succeeds or not.
        ///
        ///	For objects of version 3 or newer, this request determines the
        ///	final result of the drag-and-drop operation. If the end result
        ///	is that no mime types were accepted, the drag-and-drop operation
        ///	will be cancelled and the corresponding drag source will receive
        ///	wl_data_source.cancelled. Clients may still use this event in
        ///	conjunction with wl_data_source.action for feedback.
        pub fn accept(self,
                      con_fd: RawFd,
                      serial: u32,
                      mime_type: Array /* placeholder for string */)
                      -> Result<(), NixError> {
            let mut len = 4;
            len += if mime_type.contents.len() % 4 == 0 {
                mime_type.contents.len() / 4
            } else {
                1 + (mime_type.contents.len() / 4)
            };
            let mut vec_buf = Vec::with_capacity(len);
            unsafe { vec_buf.set_len(len) };
            let mut buf: Box<[u32]> = vec_buf.into_boxed_slice();

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            buf[ind] = serial as u32;
            ind += 1;

            buf[ind] = mime_type.contents.len() as u32;
            unsafe {
                let dst_ptr = buf[ind + 1..].as_mut_ptr() as *mut u8;
                let src_ptr = mime_type.contents.as_ptr();
                ::std::ptr::copy_nonoverlapping(src_ptr, dst_ptr, mime_type.contents.len());
            };
            if mime_type.contents.len() % 4 == 0 {
                ind += 1 + (mime_type.contents.len() / 4);
            } else {
                ind += 2 + (mime_type.contents.len() / 4);
            }

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: request that the data is transferred
        ///
        /// To transfer the offered data, the client issues this request
        ///	and indicates the mime type it wants to receive.  The transfer
        ///	happens through the passed file descriptor (typically created
        ///	with the pipe system call).  The source client writes the data
        ///	in the mime type representation requested and then closes the
        ///	file descriptor.
        ///
        ///	The receiving client reads from the read end of the pipe until
        ///	EOF and then closes its end, at which point the transfer is
        ///	complete.
        ///
        ///	This request may happen multiple times for different mimetypes,
        ///	both before and after wl_data_device.drop. Drag-and-drop destination
        ///	clients may preemptively fetch data or examine it more closely to
        ///	determine acceptance.
        pub fn receive(self,
                       con_fd: RawFd,
                       mime_type: Array, // placeholder for string
                       fd: RawFd)
                       -> Result<(), NixError> {
            let mut len = 3;
            len += if mime_type.contents.len() % 4 == 0 {
                mime_type.contents.len() / 4
            } else {
                1 + (mime_type.contents.len() / 4)
            };
            let mut vec_buf = Vec::with_capacity(len);
            unsafe { vec_buf.set_len(len) };
            let mut buf: Box<[u32]> = vec_buf.into_boxed_slice();

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 1 as u32;

            let mut ind = 2;

            buf[ind] = mime_type.contents.len() as u32;
            unsafe {
                let dst_ptr = buf[ind + 1..].as_mut_ptr() as *mut u8;
                let src_ptr = mime_type.contents.as_ptr();
                ::std::ptr::copy_nonoverlapping(src_ptr, dst_ptr, mime_type.contents.len());
            };
            if mime_type.contents.len() % 4 == 0 {
                ind += 1 + (mime_type.contents.len() / 4);
            } else {
                ind += 2 + (mime_type.contents.len() / 4);
            }

            let fds = [fd];
            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            let iov = [IoVec::from_slice(buf_8)];
            let cmsg = ControlMessage::ScmRights(&fds);
            sendmsg(con_fd, &iov, &[cmsg], MsgFlags::empty(), None).map(|x| ())
        }

        /// Request: destroy data offer
        ///
        /// Destroy the data offer.
        pub fn destroy(self, con_fd: RawFd) -> Result<(), NixError> {
            let len = 2;
            let mut buf: [u32; 2] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 2 as u32;

            let mut ind = 2;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: the offer will no longer be used
        ///
        /// Notifies the compositor that the drag destination successfully
        ///	finished the drag-and-drop operation.
        ///
        ///	Upon receiving this request, the compositor will emit
        ///	wl_data_source.dnd_finished on the drag source client.
        ///
        ///	It is a client error to perform other requests than
        ///	wl_data_offer.destroy after this one. It is also an error to perform
        ///	this request after a NULL mime type has been set in
        ///	wl_data_offer.accept or no action was received through
        ///	wl_data_offer.action.
        pub fn finish(self, con_fd: RawFd) -> Result<(), NixError> {
            let len = 2;
            let mut buf: [u32; 2] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 3 as u32;

            let mut ind = 2;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: set the available/preferred drag-and-drop actions
        ///
        /// Sets the actions that the destination side client supports for
        ///	this operation. This request may trigger the emission of
        ///	wl_data_source.action and wl_data_offer.action events if the compositor
        ///	need to change the selected action.
        ///
        ///	This request can be called multiple times throughout the
        ///	drag-and-drop operation, typically in response to wl_data_device.enter
        ///	or wl_data_device.motion events.
        ///
        ///	This request determines the final result of the drag-and-drop
        ///	operation. If the end result is that no action is accepted,
        ///	the drag source will receive wl_drag_source.cancelled.
        ///
        ///	The dnd_actions argument must contain only values expressed in the
        ///	wl_data_device_manager.dnd_actions enum, and the preferred_action
        ///	argument must only contain one of those values set, otherwise it
        ///	will result in a protocol error.
        ///
        ///	While managing an "ask" action, the destination drag-and-drop client
        ///	may perform further wl_data_offer.receive requests, and is expected
        ///	to perform one last wl_data_offer.set_actions request with a preferred
        ///	action other than "ask" (and optionally wl_data_offer.accept) before
        ///	requesting wl_data_offer.finish, in order to convey the action selected
        ///	by the user. If the preferred action is not in the
        ///	wl_data_offer.source_actions mask, an error will be raised.
        ///
        ///	If the "ask" action is dismissed (e.g. user cancellation), the client
        ///	is expected to perform wl_data_offer.destroy right away.
        ///
        ///	This request can only be made on drag-and-drop offers, a protocol error
        ///	will be raised otherwise.
        pub fn set_actions(self,
                           con_fd: RawFd,
                           dnd_actions: u32,
                           preferred_action: u32)
                           -> Result<(), NixError> {
            let len = 4;
            let mut buf: [u32; 4] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 4 as u32;

            let mut ind = 2;

            buf[ind] = dnd_actions as u32;
            ind += 1;

            buf[ind] = preferred_action as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl DataSource {
        /// Request: add an offered mime type
        ///
        /// This request adds a mime type to the set of mime types
        ///	advertised to targets.  Can be called several times to offer
        ///	multiple types.
        pub fn offer(self,
                     con_fd: RawFd,
                     mime_type: Array /* placeholder for string */)
                     -> Result<(), NixError> {
            let mut len = 3;
            len += if mime_type.contents.len() % 4 == 0 {
                mime_type.contents.len() / 4
            } else {
                1 + (mime_type.contents.len() / 4)
            };
            let mut vec_buf = Vec::with_capacity(len);
            unsafe { vec_buf.set_len(len) };
            let mut buf: Box<[u32]> = vec_buf.into_boxed_slice();

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            buf[ind] = mime_type.contents.len() as u32;
            unsafe {
                let dst_ptr = buf[ind + 1..].as_mut_ptr() as *mut u8;
                let src_ptr = mime_type.contents.as_ptr();
                ::std::ptr::copy_nonoverlapping(src_ptr, dst_ptr, mime_type.contents.len());
            };
            if mime_type.contents.len() % 4 == 0 {
                ind += 1 + (mime_type.contents.len() / 4);
            } else {
                ind += 2 + (mime_type.contents.len() / 4);
            }

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: destroy the data source
        ///
        /// Destroy the data source.
        pub fn destroy(self, con_fd: RawFd) -> Result<(), NixError> {
            let len = 2;
            let mut buf: [u32; 2] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 1 as u32;

            let mut ind = 2;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: set the available drag-and-drop actions
        ///
        /// Sets the actions that the source side client supports for this
        ///	operation. This request may trigger wl_data_source.action and
        ///	wl_data_offer.action events if the compositor needs to change the
        ///	selected action.
        ///
        ///	The dnd_actions argument must contain only values expressed in the
        ///	wl_data_device_manager.dnd_actions enum, otherwise it will result
        ///	in a protocol error.
        ///
        ///	This request must be made once only, and can only be made on sources
        ///	used in drag-and-drop, so it must be performed before
        ///	wl_data_device.start_drag. Attempting to use the source other than
        ///	for drag-and-drop will raise a protocol error.
        pub fn set_actions(self, con_fd: RawFd, dnd_actions: u32) -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 2 as u32;

            let mut ind = 2;

            buf[ind] = dnd_actions as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl DataDevice {
        /// Request: start drag-and-drop operation
        ///
        /// This request asks the compositor to start a drag-and-drop
        ///	operation on behalf of the client.
        ///
        ///	The source argument is the data source that provides the data
        ///	for the eventual data transfer. If source is NULL, enter, leave
        ///	and motion events are sent only to the client that initiated the
        ///	drag and the client is expected to handle the data passing
        ///	internally.
        ///
        ///	The origin surface is the surface where the drag originates and
        ///	the client must have an active implicit grab that matches the
        ///	serial.
        ///
        ///	The icon surface is an optional (can be NULL) surface that
        ///	provides an icon to be moved around with the cursor.  Initially,
        ///	the top-left corner of the icon surface is placed at the cursor
        ///	hotspot, but subsequent wl_surface.attach request can move the
        ///	relative position. Attach requests must be confirmed with
        ///	wl_surface.commit as usual. The icon surface is given the role of
        ///	a drag-and-drop icon. If the icon surface already has another role,
        ///	it raises a protocol error.
        ///
        ///	The current and pending input regions of the icon wl_surface are
        ///	cleared, and wl_surface.set_input_region is ignored until the
        ///	wl_surface is no longer used as the icon surface. When the use
        ///	as an icon ends, the current and pending input regions become
        ///	undefined, and the wl_surface is unmapped.
        pub fn start_drag(self,
                          con_fd: RawFd,
                          source: interfaces::DataSource,
                          origin: interfaces::Surface,
                          icon: interfaces::Surface,
                          serial: u32)
                          -> Result<(), NixError> {
            let len = 6;
            let mut buf: [u32; 6] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            buf[ind] = source.id();
            ind += 1;

            buf[ind] = origin.id();
            ind += 1;

            buf[ind] = icon.id();
            ind += 1;

            buf[ind] = serial as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: copy data to the selection
        ///
        /// This request asks the compositor to set the selection
        ///	to the data from the source on behalf of the client.
        ///
        ///	To unset the selection, set the source to NULL.
        pub fn set_selection(self,
                             con_fd: RawFd,
                             source: interfaces::DataSource,
                             serial: u32)
                             -> Result<(), NixError> {
            let len = 4;
            let mut buf: [u32; 4] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 1 as u32;

            let mut ind = 2;

            buf[ind] = source.id();
            ind += 1;

            buf[ind] = serial as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: destroy data device
        ///
        /// This request destroys the data device.
        pub fn release(self, con_fd: RawFd) -> Result<(), NixError> {
            let len = 2;
            let mut buf: [u32; 2] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 2 as u32;

            let mut ind = 2;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl DataDeviceManager {
        /// Request: create a new data source
        ///
        /// Create a new data source.
        pub fn create_data_source(self,
                                  con_fd: RawFd,
                                  id: New<interfaces::DataSource>)
                                  -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            buf[ind] = (id.0).id();
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: create a new data device
        ///
        /// Create a new data device for a given seat.
        pub fn get_data_device(self,
                               con_fd: RawFd,
                               id: New<interfaces::DataDevice>,
                               seat: interfaces::Seat)
                               -> Result<(), NixError> {
            let len = 4;
            let mut buf: [u32; 4] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 1 as u32;

            let mut ind = 2;

            buf[ind] = (id.0).id();
            ind += 1;

            buf[ind] = seat.id();
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl Shell {
        /// Request: create a shell surface from a surface
        ///
        /// Create a shell surface for an existing surface. This gives
        ///	the wl_surface the role of a shell surface. If the wl_surface
        ///	already has another role, it raises a protocol error.
        ///
        ///	Only one shell surface can be associated with a given surface.
        pub fn get_shell_surface(self,
                                 con_fd: RawFd,
                                 id: New<interfaces::ShellSurface>,
                                 surface: interfaces::Surface)
                                 -> Result<(), NixError> {
            let len = 4;
            let mut buf: [u32; 4] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            buf[ind] = (id.0).id();
            ind += 1;

            buf[ind] = surface.id();
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl ShellSurface {
        /// Request: respond to a ping event
        ///
        /// A client must respond to a ping event with a pong request or
        ///	the client may be deemed unresponsive.
        pub fn pong(self, con_fd: RawFd, serial: u32) -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            buf[ind] = serial as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: start an interactive move
        ///
        /// Start a pointer-driven move of the surface.
        ///
        ///	This request must be used in response to a button press event.
        ///	The server may ignore move requests depending on the state of
        ///	the surface (e.g. fullscreen or maximized).
        pub fn move_(self,
                     con_fd: RawFd,
                     seat: interfaces::Seat,
                     serial: u32)
                     -> Result<(), NixError> {
            let len = 4;
            let mut buf: [u32; 4] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 1 as u32;

            let mut ind = 2;

            buf[ind] = seat.id();
            ind += 1;

            buf[ind] = serial as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: start an interactive resize
        ///
        /// Start a pointer-driven resizing of the surface.
        ///
        ///	This request must be used in response to a button press event.
        ///	The server may ignore resize requests depending on the state of
        ///	the surface (e.g. fullscreen or maximized).
        pub fn resize(self,
                      con_fd: RawFd,
                      seat: interfaces::Seat,
                      serial: u32,
                      edges: ::interfaces::shell_surface::resize)
                      -> Result<(), NixError> {
            let len = 5;
            let mut buf: [u32; 5] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 2 as u32;

            let mut ind = 2;

            buf[ind] = seat.id();
            ind += 1;

            buf[ind] = serial as u32;
            ind += 1;

            buf[ind] = edges.0 as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: make the surface a toplevel surface
        ///
        /// Map the surface as a toplevel surface.
        ///
        ///	A toplevel surface is not fullscreen, maximized or transient.
        pub fn set_toplevel(self, con_fd: RawFd) -> Result<(), NixError> {
            let len = 2;
            let mut buf: [u32; 2] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 3 as u32;

            let mut ind = 2;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: make the surface a transient surface
        ///
        /// Map the surface relative to an existing surface.
        ///
        ///	The x and y arguments specify the locations of the upper left
        ///	corner of the surface relative to the upper left corner of the
        ///	parent surface, in surface local coordinates.
        ///
        ///	The flags argument controls details of the transient behaviour.
        pub fn set_transient(self,
                             con_fd: RawFd,
                             parent: interfaces::Surface,
                             x: i32,
                             y: i32,
                             flags: ::interfaces::shell_surface::transient)
                             -> Result<(), NixError> {
            let len = 6;
            let mut buf: [u32; 6] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 4 as u32;

            let mut ind = 2;

            buf[ind] = parent.id();
            ind += 1;

            buf[ind] = x as u32;
            ind += 1;

            buf[ind] = y as u32;
            ind += 1;

            buf[ind] = flags.0 as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: make the surface a fullscreen surface
        ///
        /// Map the surface as a fullscreen surface.
        ///
        ///	If an output parameter is given then the surface will be made
        ///	fullscreen on that output. If the client does not specify the
        ///	output then the compositor will apply its policy - usually
        ///	choosing the output on which the surface has the biggest surface
        ///	area.
        ///
        ///	The client may specify a method to resolve a size conflict
        ///	between the output size and the surface size - this is provided
        ///	through the method parameter.
        ///
        ///	The framerate parameter is used only when the method is set
        ///	to "driver", to indicate the preferred framerate. A value of 0
        ///	indicates that the app does not care about framerate.  The
        ///	framerate is specified in mHz, that is framerate of 60000 is 60Hz.
        ///
        ///	A method of "scale" or "driver" implies a scaling operation of
        ///	the surface, either via a direct scaling operation or a change of
        ///	the output mode. This will override any kind of output scaling, so
        ///	that mapping a surface with a buffer size equal to the mode can
        ///	fill the screen independent of buffer_scale.
        ///
        ///	A method of "fill" means we don't scale up the buffer, however
        ///	any output scale is applied. This means that you may run into
        ///	an edge case where the application maps a buffer with the same
        ///	size of the output mode but buffer_scale 1 (thus making a
        ///	surface larger than the output). In this case it is allowed to
        ///	downscale the results to fit the screen.
        ///
        ///	The compositor must reply to this request with a configure event
        ///	with the dimensions for the output on which the surface will
        ///	be made fullscreen.
        pub fn set_fullscreen(self,
                              con_fd: RawFd,
                              method: ::interfaces::shell_surface::fullscreen_method,
                              framerate: u32,
                              output: interfaces::Output)
                              -> Result<(), NixError> {
            let len = 5;
            let mut buf: [u32; 5] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 5 as u32;

            let mut ind = 2;

            buf[ind] = method.0 as u32;
            ind += 1;

            buf[ind] = framerate as u32;
            ind += 1;

            buf[ind] = output.id();
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: make the surface a popup surface
        ///
        /// Map the surface as a popup.
        ///
        ///	A popup surface is a transient surface with an added pointer
        ///	grab.
        ///
        ///	An existing implicit grab will be changed to owner-events mode,
        ///	and the popup grab will continue after the implicit grab ends
        ///	(i.e. releasing the mouse button does not cause the popup to
        ///	be unmapped).
        ///
        ///	The popup grab continues until the window is destroyed or a
        ///	mouse button is pressed in any other clients window. A click
        ///	in any of the clients surfaces is reported as normal, however,
        ///	clicks in other clients surfaces will be discarded and trigger
        ///	the callback.
        ///
        ///	The x and y arguments specify the locations of the upper left
        ///	corner of the surface relative to the upper left corner of the
        ///	parent surface, in surface local coordinates.
        pub fn set_popup(self,
                         con_fd: RawFd,
                         seat: interfaces::Seat,
                         serial: u32,
                         parent: interfaces::Surface,
                         x: i32,
                         y: i32,
                         flags: ::interfaces::shell_surface::transient)
                         -> Result<(), NixError> {
            let len = 8;
            let mut buf: [u32; 8] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 6 as u32;

            let mut ind = 2;

            buf[ind] = seat.id();
            ind += 1;

            buf[ind] = serial as u32;
            ind += 1;

            buf[ind] = parent.id();
            ind += 1;

            buf[ind] = x as u32;
            ind += 1;

            buf[ind] = y as u32;
            ind += 1;

            buf[ind] = flags.0 as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: make the surface a maximized surface
        ///
        /// Map the surface as a maximized surface.
        ///
        ///	If an output parameter is given then the surface will be
        ///	maximized on that output. If the client does not specify the
        ///	output then the compositor will apply its policy - usually
        ///	choosing the output on which the surface has the biggest surface
        ///	area.
        ///
        ///	The compositor will reply with a configure event telling
        ///	the expected new surface size. The operation is completed
        ///	on the next buffer attach to this surface.
        ///
        ///	A maximized surface typically fills the entire output it is
        ///	bound to, except for desktop element such as panels. This is
        ///	the main difference between a maximized shell surface and a
        ///	fullscreen shell surface.
        ///
        ///	The details depend on the compositor implementation.
        pub fn set_maximized(self,
                             con_fd: RawFd,
                             output: interfaces::Output)
                             -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 7 as u32;

            let mut ind = 2;

            buf[ind] = output.id();
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: set surface title
        ///
        /// Set a short title for the surface.
        ///
        ///	This string may be used to identify the surface in a task bar,
        ///	window list, or other user interface elements provided by the
        ///	compositor.
        ///
        ///	The string must be encoded in UTF-8.
        pub fn set_title(self,
                         con_fd: RawFd,
                         title: Array /* placeholder for string */)
                         -> Result<(), NixError> {
            let mut len = 3;
            len += if title.contents.len() % 4 == 0 {
                title.contents.len() / 4
            } else {
                1 + (title.contents.len() / 4)
            };
            let mut vec_buf = Vec::with_capacity(len);
            unsafe { vec_buf.set_len(len) };
            let mut buf: Box<[u32]> = vec_buf.into_boxed_slice();

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 8 as u32;

            let mut ind = 2;

            buf[ind] = title.contents.len() as u32;
            unsafe {
                let dst_ptr = buf[ind + 1..].as_mut_ptr() as *mut u8;
                let src_ptr = title.contents.as_ptr();
                ::std::ptr::copy_nonoverlapping(src_ptr, dst_ptr, title.contents.len());
            };
            if title.contents.len() % 4 == 0 {
                ind += 1 + (title.contents.len() / 4);
            } else {
                ind += 2 + (title.contents.len() / 4);
            }

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: set surface class
        ///
        /// Set a class for the surface.
        ///
        ///	The surface class identifies the general class of applications
        ///	to which the surface belongs. A common convention is to use the
        ///	file name (or the full path if it is a non-standard location) of
        ///	the application's .desktop file as the class.
        pub fn set_class(self,
                         con_fd: RawFd,
                         class_: Array /* placeholder for string */)
                         -> Result<(), NixError> {
            let mut len = 3;
            len += if class_.contents.len() % 4 == 0 {
                class_.contents.len() / 4
            } else {
                1 + (class_.contents.len() / 4)
            };
            let mut vec_buf = Vec::with_capacity(len);
            unsafe { vec_buf.set_len(len) };
            let mut buf: Box<[u32]> = vec_buf.into_boxed_slice();

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 9 as u32;

            let mut ind = 2;

            buf[ind] = class_.contents.len() as u32;
            unsafe {
                let dst_ptr = buf[ind + 1..].as_mut_ptr() as *mut u8;
                let src_ptr = class_.contents.as_ptr();
                ::std::ptr::copy_nonoverlapping(src_ptr, dst_ptr, class_.contents.len());
            };
            if class_.contents.len() % 4 == 0 {
                ind += 1 + (class_.contents.len() / 4);
            } else {
                ind += 2 + (class_.contents.len() / 4);
            }

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl Surface {
        /// Request: delete surface
        ///
        /// Deletes the surface and invalidates its object ID.
        pub fn destroy(self, con_fd: RawFd) -> Result<(), NixError> {
            let len = 2;
            let mut buf: [u32; 2] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: set the surface contents
        ///
        /// Set a buffer as the content of this surface.
        ///
        ///	The new size of the surface is calculated based on the buffer
        ///	size transformed by the inverse buffer_transform and the
        ///	inverse buffer_scale. This means that the supplied buffer
        ///	must be an integer multiple of the buffer_scale.
        ///
        ///	The x and y arguments specify the location of the new pending
        ///	buffer's upper left corner, relative to the current buffer's upper
        ///	left corner, in surface local coordinates. In other words, the
        ///	x and y, combined with the new surface size define in which
        ///	directions the surface's size changes.
        ///
        ///	Surface contents are double-buffered state, see wl_surface.commit.
        ///
        ///	The initial surface contents are void; there is no content.
        ///	wl_surface.attach assigns the given wl_buffer as the pending
        ///	wl_buffer. wl_surface.commit makes the pending wl_buffer the new
        ///	surface contents, and the size of the surface becomes the size
        ///	calculated from the wl_buffer, as described above. After commit,
        ///	there is no pending buffer until the next attach.
        ///
        ///	Committing a pending wl_buffer allows the compositor to read the
        ///	pixels in the wl_buffer. The compositor may access the pixels at
        ///	any time after the wl_surface.commit request. When the compositor
        ///	will not access the pixels anymore, it will send the
        ///	wl_buffer.release event. Only after receiving wl_buffer.release,
        ///	the client may re-use the wl_buffer. A wl_buffer that has been
        ///	attached and then replaced by another attach instead of committed
        ///	will not receive a release event, and is not used by the
        ///	compositor.
        ///
        ///	Destroying the wl_buffer after wl_buffer.release does not change
        ///	the surface contents. However, if the client destroys the
        ///	wl_buffer before receiving the wl_buffer.release event, the surface
        ///	contents become undefined immediately.
        ///
        ///	If wl_surface.attach is sent with a NULL wl_buffer, the
        ///	following wl_surface.commit will remove the surface content.
        pub fn attach(self,
                      con_fd: RawFd,
                      buffer: interfaces::Buffer,
                      x: i32,
                      y: i32)
                      -> Result<(), NixError> {
            let len = 5;
            let mut buf: [u32; 5] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 1 as u32;

            let mut ind = 2;

            buf[ind] = buffer.id();
            ind += 1;

            buf[ind] = x as u32;
            ind += 1;

            buf[ind] = y as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: mark part of the surface damaged
        ///
        /// This request is used to describe the regions where the pending
        ///	buffer is different from the current surface contents, and where
        ///	the surface therefore needs to be repainted. The compositor
        ///	ignores the parts of the damage that fall outside of the surface.
        ///
        ///	Damage is double-buffered state, see wl_surface.commit.
        ///
        ///	The damage rectangle is specified in surface local coordinates.
        ///
        ///	The initial value for pending damage is empty: no damage.
        ///	wl_surface.damage adds pending damage: the new pending damage
        ///	is the union of old pending damage and the given rectangle.
        ///
        ///	wl_surface.commit assigns pending damage as the current damage,
        ///	and clears pending damage. The server will clear the current
        ///	damage as it repaints the surface.
        ///
        ///	Alternatively, damage can be posted with wl_surface.damage_buffer
        ///	which uses buffer co-ordinates instead of surface co-ordinates,
        ///	and is probably the preferred and intuitive way of doing this.
        pub fn damage(self,
                      con_fd: RawFd,
                      x: i32,
                      y: i32,
                      width: i32,
                      height: i32)
                      -> Result<(), NixError> {
            let len = 6;
            let mut buf: [u32; 6] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 2 as u32;

            let mut ind = 2;

            buf[ind] = x as u32;
            ind += 1;

            buf[ind] = y as u32;
            ind += 1;

            buf[ind] = width as u32;
            ind += 1;

            buf[ind] = height as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: request a frame throttling hint
        ///
        /// Request a notification when it is a good time start drawing a new
        ///	frame, by creating a frame callback. This is useful for throttling
        ///	redrawing operations, and driving animations.
        ///
        ///	When a client is animating on a wl_surface, it can use the 'frame'
        ///	request to get notified when it is a good time to draw and commit the
        ///	next frame of animation. If the client commits an update earlier than
        ///	that, it is likely that some updates will not make it to the display,
        ///	and the client is wasting resources by drawing too often.
        ///
        ///	The frame request will take effect on the next wl_surface.commit.
        ///	The notification will only be posted for one frame unless
        ///	requested again. For a wl_surface, the notifications are posted in
        ///	the order the frame requests were committed.
        ///
        ///	The server must send the notifications so that a client
        ///	will not send excessive updates, while still allowing
        ///	the highest possible update rate for clients that wait for the reply
        ///	before drawing again. The server should give some time for the client
        ///	to draw and commit after sending the frame callback events to let them
        ///	hit the next output refresh.
        ///
        ///	A server should avoid signalling the frame callbacks if the
        ///	surface is not visible in any way, e.g. the surface is off-screen,
        ///	or completely obscured by other opaque surfaces.
        ///
        ///	The object returned by this request will be destroyed by the
        ///	compositor after the callback is fired and as such the client must not
        ///	attempt to use it after that point.
        ///
        ///	The callback_data passed in the callback is the current time, in
        ///	milliseconds, with an undefined base.
        pub fn frame(self,
                     con_fd: RawFd,
                     callback: New<interfaces::Callback>)
                     -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 3 as u32;

            let mut ind = 2;

            buf[ind] = (callback.0).id();
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: set opaque region
        ///
        /// This request sets the region of the surface that contains
        ///	opaque content.
        ///
        ///	The opaque region is an optimization hint for the compositor
        ///	that lets it optimize out redrawing of content behind opaque
        ///	regions.  Setting an opaque region is not required for correct
        ///	behaviour, but marking transparent content as opaque will result
        ///	in repaint artifacts.
        ///
        ///	The opaque region is specified in surface local coordinates.
        ///
        ///	The compositor ignores the parts of the opaque region that fall
        ///	outside of the surface.
        ///
        ///	Opaque region is double-buffered state, see wl_surface.commit.
        ///
        ///	wl_surface.set_opaque_region changes the pending opaque region.
        ///	wl_surface.commit copies the pending region to the current region.
        ///	Otherwise, the pending and current regions are never changed.
        ///
        ///	The initial value for opaque region is empty. Setting the pending
        ///	opaque region has copy semantics, and the wl_region object can be
        ///	destroyed immediately. A NULL wl_region causes the pending opaque
        ///	region to be set to empty.
        pub fn set_opaque_region(self,
                                 con_fd: RawFd,
                                 region: interfaces::Region)
                                 -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 4 as u32;

            let mut ind = 2;

            buf[ind] = region.id();
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: set input region
        ///
        /// This request sets the region of the surface that can receive
        ///	pointer and touch events.
        ///
        ///	Input events happening outside of this region will try the next
        ///	surface in the server surface stack. The compositor ignores the
        ///	parts of the input region that fall outside of the surface.
        ///
        ///	The input region is specified in surface local coordinates.
        ///
        ///	Input region is double-buffered state, see wl_surface.commit.
        ///
        ///	wl_surface.set_input_region changes the pending input region.
        ///	wl_surface.commit copies the pending region to the current region.
        ///	Otherwise the pending and current regions are never changed,
        ///	except cursor and icon surfaces are special cases, see
        ///	wl_pointer.set_cursor and wl_data_device.start_drag.
        ///
        ///	The initial value for input region is infinite. That means the
        ///	whole surface will accept input. Setting the pending input region
        ///	has copy semantics, and the wl_region object can be destroyed
        ///	immediately. A NULL wl_region causes the input region to be set
        ///	to infinite.
        pub fn set_input_region(self,
                                con_fd: RawFd,
                                region: interfaces::Region)
                                -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 5 as u32;

            let mut ind = 2;

            buf[ind] = region.id();
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: commit pending surface state
        ///
        /// Surface state (input, opaque, and damage regions, attached buffers,
        ///	etc.) is double-buffered. Protocol requests modify the pending
        ///	state, as opposed to current state in use by the compositor. Commit
        ///	request atomically applies all pending state, replacing the current
        ///	state. After commit, the new pending state is as documented for each
        ///	related request.
        ///
        ///	On commit, a pending wl_buffer is applied first, all other state
        ///	second. This means that all coordinates in double-buffered state are
        ///	relative to the new wl_buffer coming into use, except for
        ///	wl_surface.attach itself. If there is no pending wl_buffer, the
        ///	coordinates are relative to the current surface contents.
        ///
        ///	All requests that need a commit to become effective are documented
        ///	to affect double-buffered state.
        ///
        ///	Other interfaces may add further double-buffered surface state.
        pub fn commit(self, con_fd: RawFd) -> Result<(), NixError> {
            let len = 2;
            let mut buf: [u32; 2] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 6 as u32;

            let mut ind = 2;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: sets the buffer transformation
        ///
        /// This request sets an optional transformation on how the compositor
        ///	interprets the contents of the buffer attached to the surface. The
        ///	accepted values for the transform parameter are the values for
        ///	wl_output.transform.
        ///
        ///	Buffer transform is double-buffered state, see wl_surface.commit.
        ///
        ///	A newly created surface has its buffer transformation set to normal.
        ///
        ///	wl_surface.set_buffer_transform changes the pending buffer
        ///	transformation. wl_surface.commit copies the pending buffer
        ///	transformation to the current one. Otherwise, the pending and current
        ///	values are never changed.
        ///
        ///	The purpose of this request is to allow clients to render content
        ///	according to the output transform, thus permiting the compositor to
        ///	use certain optimizations even if the display is rotated. Using
        ///	hardware overlays and scanning out a client buffer for fullscreen
        ///	surfaces are examples of such optimizations. Those optimizations are
        ///	highly dependent on the compositor implementation, so the use of this
        ///	request should be considered on a case-by-case basis.
        ///
        ///	Note that if the transform value includes 90 or 270 degree rotation,
        ///	the width of the buffer will become the surface height and the height
        ///	of the buffer will become the surface width.
        ///
        ///	If transform is not one of the values from the
        ///	wl_output.transform enum the invalid_transform protocol error
        ///	is raised.
        pub fn set_buffer_transform(self, con_fd: RawFd, transform: i32) -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 7 as u32;

            let mut ind = 2;

            buf[ind] = transform as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: sets the buffer scaling factor
        ///
        /// This request sets an optional scaling factor on how the compositor
        ///	interprets the contents of the buffer attached to the window.
        ///
        ///	Buffer scale is double-buffered state, see wl_surface.commit.
        ///
        ///	A newly created surface has its buffer scale set to 1.
        ///
        ///	wl_surface.set_buffer_scale changes the pending buffer scale.
        ///	wl_surface.commit copies the pending buffer scale to the current one.
        ///	Otherwise, the pending and current values are never changed.
        ///
        ///	The purpose of this request is to allow clients to supply higher
        ///	resolution buffer data for use on high resolution outputs. Its
        ///	intended that you pick the same	buffer scale as the scale of the
        ///	output that the surface is displayed on.This means the compositor
        ///	can avoid scaling when rendering the surface on that output.
        ///
        ///	Note that if the scale is larger than 1, then you have to attach
        ///	a buffer that is larger (by a factor of scale in each dimension)
        ///	than the desired surface size.
        ///
        ///	If scale is not positive the invalid_scale protocol error is
        ///	raised.
        pub fn set_buffer_scale(self, con_fd: RawFd, scale: i32) -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 8 as u32;

            let mut ind = 2;

            buf[ind] = scale as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: mark part of the surface damaged using buffer co-ordinates
        ///
        /// This request is used to describe the regions where the pending
        ///	buffer is different from the current surface contents, and where
        ///	the surface therefore needs to be repainted. The compositor
        ///	ignores the parts of the damage that fall outside of the surface.
        ///
        ///	Damage is double-buffered state, see wl_surface.commit.
        ///
        ///	The damage rectangle is specified in buffer coordinates.
        ///
        ///	The initial value for pending damage is empty: no damage.
        ///	wl_surface.damage_buffer adds pending damage: the new pending
        ///	damage is the union of old pending damage and the given rectangle.
        ///
        ///	wl_surface.commit assigns pending damage as the current damage,
        ///	and clears pending damage. The server will clear the current
        ///	damage as it repaints the surface.
        ///
        ///	This request differs from wl_surface.damage in only one way - it
        ///	takes damage in buffer co-ordinates instead of surface local
        ///	co-ordinates. While this generally is more intuitive than surface
        ///	co-ordinates, it is especially desirable when using wp_viewport
        ///	or when a drawing library (like EGL) is unaware of buffer scale
        ///	and buffer transform.
        ///
        ///	Note: Because buffer transformation changes and damage requests may
        ///	be interleaved in the protocol stream, It is impossible to determine
        ///	the actual mapping between surface and buffer damage until
        ///	wl_surface.commit time. Therefore, compositors wishing to take both
        ///	kinds of damage into account will have to accumulate damage from the
        ///	two requests separately and only transform from one to the other
        ///	after receiving the wl_surface.commit.
        pub fn damage_buffer(self,
                             con_fd: RawFd,
                             x: i32,
                             y: i32,
                             width: i32,
                             height: i32)
                             -> Result<(), NixError> {
            let len = 6;
            let mut buf: [u32; 6] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 9 as u32;

            let mut ind = 2;

            buf[ind] = x as u32;
            ind += 1;

            buf[ind] = y as u32;
            ind += 1;

            buf[ind] = width as u32;
            ind += 1;

            buf[ind] = height as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl Seat {
        /// Request: return pointer object
        ///
        /// The ID provided will be initialized to the wl_pointer interface
        ///	for this seat.
        ///
        ///	This request only takes effect if the seat has the pointer
        ///	capability, or has had the pointer capability in the past.
        ///	It is a protocol violation to issue this request on a seat that has
        ///	never had the pointer capability.
        pub fn get_pointer(self,
                           con_fd: RawFd,
                           id: New<interfaces::Pointer>)
                           -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            buf[ind] = (id.0).id();
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: return keyboard object
        ///
        /// The ID provided will be initialized to the wl_keyboard interface
        ///	for this seat.
        ///
        ///	This request only takes effect if the seat has the keyboard
        ///	capability, or has had the keyboard capability in the past.
        ///	It is a protocol violation to issue this request on a seat that has
        ///	never had the keyboard capability.
        pub fn get_keyboard(self,
                            con_fd: RawFd,
                            id: New<interfaces::Keyboard>)
                            -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 1 as u32;

            let mut ind = 2;

            buf[ind] = (id.0).id();
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: return touch object
        ///
        /// The ID provided will be initialized to the wl_touch interface
        ///	for this seat.
        ///
        ///	This request only takes effect if the seat has the touch
        ///	capability, or has had the touch capability in the past.
        ///	It is a protocol violation to issue this request on a seat that has
        ///	never had the touch capability.
        pub fn get_touch(self, con_fd: RawFd, id: New<interfaces::Touch>) -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 2 as u32;

            let mut ind = 2;

            buf[ind] = (id.0).id();
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: release the seat object
        ///
        /// Using this request client can tell the server that it is not going to
        ///	use the seat object anymore.
        pub fn release(self, con_fd: RawFd) -> Result<(), NixError> {
            let len = 2;
            let mut buf: [u32; 2] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 3 as u32;

            let mut ind = 2;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl Pointer {
        /// Request: set the pointer surface
        ///
        /// Set the pointer surface, i.e., the surface that contains the
        ///	pointer image (cursor). This request gives the surface the role
        ///	of a cursor. If the surface already has another role, it raises
        ///	a protocol error.
        ///
        ///	The cursor actually changes only if the pointer
        ///	focus for this device is one of the requesting client's surfaces
        ///	or the surface parameter is the current pointer surface. If
        ///	there was a previous surface set with this request it is
        ///	replaced. If surface is NULL, the pointer image is hidden.
        ///
        ///	The parameters hotspot_x and hotspot_y define the position of
        ///	the pointer surface relative to the pointer location. Its
        ///	top-left corner is always at (x, y) - (hotspot_x, hotspot_y),
        ///	where (x, y) are the coordinates of the pointer location, in surface
        ///	local coordinates.
        ///
        ///	On surface.attach requests to the pointer surface, hotspot_x
        ///	and hotspot_y are decremented by the x and y parameters
        ///	passed to the request. Attach must be confirmed by
        ///	wl_surface.commit as usual.
        ///
        ///	The hotspot can also be updated by passing the currently set
        ///	pointer surface to this request with new values for hotspot_x
        ///	and hotspot_y.
        ///
        ///	The current and pending input regions of the wl_surface are
        ///	cleared, and wl_surface.set_input_region is ignored until the
        ///	wl_surface is no longer used as the cursor. When the use as a
        ///	cursor ends, the current and pending input regions become
        ///	undefined, and the wl_surface is unmapped.
        pub fn set_cursor(self,
                          con_fd: RawFd,
                          serial: u32,
                          surface: interfaces::Surface,
                          hotspot_x: i32,
                          hotspot_y: i32)
                          -> Result<(), NixError> {
            let len = 6;
            let mut buf: [u32; 6] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            buf[ind] = serial as u32;
            ind += 1;

            buf[ind] = surface.id();
            ind += 1;

            buf[ind] = hotspot_x as u32;
            ind += 1;

            buf[ind] = hotspot_y as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: release the pointer object
        ///
        /// Using this request client can tell the server that it is not going to
        ///	use the pointer object anymore.
        ///
        ///	This request destroys the pointer proxy object, so user must not call
        ///	wl_pointer_destroy() after using this request.
        pub fn release(self, con_fd: RawFd) -> Result<(), NixError> {
            let len = 2;
            let mut buf: [u32; 2] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 1 as u32;

            let mut ind = 2;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl Keyboard {
        /// Request: release the keyboard object
        ///
        /// 
        pub fn release(self, con_fd: RawFd) -> Result<(), NixError> {
            let len = 2;
            let mut buf: [u32; 2] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl Touch {
        /// Request: release the touch object
        ///
        /// 
        pub fn release(self, con_fd: RawFd) -> Result<(), NixError> {
            let len = 2;
            let mut buf: [u32; 2] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl Output {}
    #[allow(unused_mut, unused_variables)]
    impl Region {
        /// Request: destroy region
        ///
        /// Destroy the region.  This will invalidate the object ID.
        pub fn destroy(self, con_fd: RawFd) -> Result<(), NixError> {
            let len = 2;
            let mut buf: [u32; 2] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: add rectangle to region
        ///
        /// Add the specified rectangle to the region.
        pub fn add(self,
                   con_fd: RawFd,
                   x: i32,
                   y: i32,
                   width: i32,
                   height: i32)
                   -> Result<(), NixError> {
            let len = 6;
            let mut buf: [u32; 6] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 1 as u32;

            let mut ind = 2;

            buf[ind] = x as u32;
            ind += 1;

            buf[ind] = y as u32;
            ind += 1;

            buf[ind] = width as u32;
            ind += 1;

            buf[ind] = height as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: subtract rectangle from region
        ///
        /// Subtract the specified rectangle from the region.
        pub fn subtract(self,
                        con_fd: RawFd,
                        x: i32,
                        y: i32,
                        width: i32,
                        height: i32)
                        -> Result<(), NixError> {
            let len = 6;
            let mut buf: [u32; 6] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 2 as u32;

            let mut ind = 2;

            buf[ind] = x as u32;
            ind += 1;

            buf[ind] = y as u32;
            ind += 1;

            buf[ind] = width as u32;
            ind += 1;

            buf[ind] = height as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl Subcompositor {
        /// Request: unbind from the subcompositor interface
        ///
        /// Informs the server that the client will not be using this
        ///	protocol object anymore. This does not affect any other
        ///	objects, wl_subsurface objects included.
        pub fn destroy(self, con_fd: RawFd) -> Result<(), NixError> {
            let len = 2;
            let mut buf: [u32; 2] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: give a surface the role sub-surface
        ///
        /// Create a sub-surface interface for the given surface, and
        ///	associate it with the given parent surface. This turns a
        ///	plain wl_surface into a sub-surface.
        ///
        ///	The to-be sub-surface must not already have another role, and it
        ///	must not have an existing wl_subsurface object. Otherwise a protocol
        ///	error is raised.
        pub fn get_subsurface(self,
                              con_fd: RawFd,
                              id: New<interfaces::Subsurface>,
                              surface: interfaces::Surface,
                              parent: interfaces::Surface)
                              -> Result<(), NixError> {
            let len = 5;
            let mut buf: [u32; 5] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 1 as u32;

            let mut ind = 2;

            buf[ind] = (id.0).id();
            ind += 1;

            buf[ind] = surface.id();
            ind += 1;

            buf[ind] = parent.id();
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
    #[allow(unused_mut, unused_variables)]
    impl Subsurface {
        /// Request: remove sub-surface interface
        ///
        /// The sub-surface interface is removed from the wl_surface object
        ///	that was turned into a sub-surface with
        ///	wl_subcompositor.get_subsurface request. The wl_surface's association
        ///	to the parent is deleted, and the wl_surface loses its role as
        ///	a sub-surface. The wl_surface is unmapped.
        pub fn destroy(self, con_fd: RawFd) -> Result<(), NixError> {
            let len = 2;
            let mut buf: [u32; 2] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 0 as u32;

            let mut ind = 2;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: reposition the sub-surface
        ///
        /// This schedules a sub-surface position change.
        ///	The sub-surface will be moved so, that its origin (top-left
        ///	corner pixel) will be at the location x, y of the parent surface
        ///	coordinate system. The coordinates are not restricted to the parent
        ///	surface area. Negative values are allowed.
        ///
        ///	The scheduled coordinates will take effect whenever the state of the
        ///	parent surface is applied. When this happens depends on whether the
        ///	parent surface is in synchronized mode or not. See
        ///	wl_subsurface.set_sync and wl_subsurface.set_desync for details.
        ///
        ///	If more than one set_position request is invoked by the client before
        ///	the commit of the parent surface, the position of a new request always
        ///	replaces the scheduled position from any previous request.
        ///
        ///	The initial position is 0, 0.
        pub fn set_position(self, con_fd: RawFd, x: i32, y: i32) -> Result<(), NixError> {
            let len = 4;
            let mut buf: [u32; 4] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 1 as u32;

            let mut ind = 2;

            buf[ind] = x as u32;
            ind += 1;

            buf[ind] = y as u32;
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: restack the sub-surface
        ///
        /// This sub-surface is taken from the stack, and put back just
        ///	above the reference surface, changing the z-order of the sub-surfaces.
        ///	The reference surface must be one of the sibling surfaces, or the
        ///	parent surface. Using any other surface, including this sub-surface,
        ///	will cause a protocol error.
        ///
        ///	The z-order is double-buffered. Requests are handled in order and
        ///	applied immediately to a pending state. The final pending state is
        ///	copied to the active state the next time the state of the parent
        ///	surface is applied. When this happens depends on whether the parent
        ///	surface is in synchronized mode or not. See wl_subsurface.set_sync and
        ///	wl_subsurface.set_desync for details.
        ///
        ///	A new sub-surface is initially added as the top-most in the stack
        ///	of its siblings and parent.
        pub fn place_above(self,
                           con_fd: RawFd,
                           sibling: interfaces::Surface)
                           -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 2 as u32;

            let mut ind = 2;

            buf[ind] = sibling.id();
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: restack the sub-surface
        ///
        /// The sub-surface is placed just below of the reference surface.
        ///	See wl_subsurface.place_above.
        pub fn place_below(self,
                           con_fd: RawFd,
                           sibling: interfaces::Surface)
                           -> Result<(), NixError> {
            let len = 3;
            let mut buf: [u32; 3] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 3 as u32;

            let mut ind = 2;

            buf[ind] = sibling.id();
            ind += 1;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: set sub-surface to synchronized mode
        ///
        /// Change the commit behaviour of the sub-surface to synchronized
        ///	mode, also described as the parent dependent mode.
        ///
        ///	In synchronized mode, wl_surface.commit on a sub-surface will
        ///	accumulate the committed state in a cache, but the state will
        ///	not be applied and hence will not change the compositor output.
        ///	The cached state is applied to the sub-surface immediately after
        ///	the parent surface's state is applied. This ensures atomic
        ///	updates of the parent and all its synchronized sub-surfaces.
        ///	Applying the cached state will invalidate the cache, so further
        ///	parent surface commits do not (re-)apply old state.
        ///
        ///	See wl_subsurface for the recursive effect of this mode.
        pub fn set_sync(self, con_fd: RawFd) -> Result<(), NixError> {
            let len = 2;
            let mut buf: [u32; 2] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 4 as u32;

            let mut ind = 2;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }

        /// Request: set sub-surface to desynchronized mode
        ///
        /// Change the commit behaviour of the sub-surface to desynchronized
        ///	mode, also described as independent or freely running mode.
        ///
        ///	In desynchronized mode, wl_surface.commit on a sub-surface will
        ///	apply the pending state directly, without caching, as happens
        ///	normally with a wl_surface. Calling wl_surface.commit on the
        ///	parent surface has no effect on the sub-surface's wl_surface
        ///	state. This mode allows a sub-surface to be updated on its own.
        ///
        ///	If cached state exists when wl_surface.commit is called in
        ///	desynchronized mode, the pending state is added to the cached
        ///	state, and applied as whole. This invalidates the cache.
        ///
        ///	Note: even if a sub-surface is set to desynchronized, a parent
        ///	sub-surface may override it to behave as synchronized. For details,
        ///	see wl_subsurface.
        ///
        ///	If a surface's parent surface behaves as desynchronized, then
        ///	the cached state is applied on set_desync.
        pub fn set_desync(self, con_fd: RawFd) -> Result<(), NixError> {
            let len = 2;
            let mut buf: [u32; 2] = unsafe { ::std::mem::uninitialized() };

            assert!(len * 4 < ::std::u16::MAX as usize);
            buf[0] = self.0;
            buf[1] = (((len * 4) as u32) << 16) | 5 as u32;

            let mut ind = 2;

            let buf_8 = unsafe {
                ::std::slice::from_raw_parts(buf.as_ptr() as *const u8, buf.len() * 4)
            };
            send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())
        }
    }
}
/// Union type of interfaces
#[derive(Debug, Clone, Copy,PartialEq,Eq)]
pub enum Interface {
    /// core global object
    ///
    /// The core global object.  This is a special singleton object.  It
    ///  is used for internal Wayland protocol features.
    Display(Display),

    /// global registry object
    ///
    /// The global registry object.  The server has a number of global
    ///  objects that are available to all clients.  These objects
    ///  typically represent an actual object in the server (for example,
    ///  an input device) or they are singleton objects that provide
    ///  extension functionality.
    ///
    ///  When a client creates a registry object, the registry object
    ///  will emit a global event for each global currently in the
    ///  registry.  Globals come and go as a result of device or
    ///  monitor hotplugs, reconfiguration or other events, and the
    ///  registry will send out global and global_remove events to
    ///  keep the client up to date with the changes.  To mark the end
    ///  of the initial burst of events, the client can use the
    ///  wl_display.sync request immediately after calling
    ///  wl_display.get_registry.
    ///
    ///  A client can bind to a global object by using the bind
    ///  request.  This creates a client-side handle that lets the object
    ///  emit events to the client and lets the client invoke requests on
    ///  the object.
    Registry(Registry),

    /// callback object
    ///
    /// Clients can handle the 'done' event to get notified when
    ///  the related request is done.
    Callback(Callback),

    /// the compositor singleton
    ///
    /// A compositor.  This object is a singleton global.  The
    ///  compositor is in charge of combining the contents of multiple
    ///  surfaces into one displayable output.
    Compositor(Compositor),

    /// a shared memory pool
    ///
    /// The wl_shm_pool object encapsulates a piece of memory shared
    ///  between the compositor and client.  Through the wl_shm_pool
    ///  object, the client can allocate shared memory wl_buffer objects.
    ///  All objects created through the same pool share the same
    ///  underlying mapped memory. Reusing the mapped memory avoids the
    ///  setup/teardown overhead and is useful when interactively resizing
    ///  a surface or for many small buffers.
    ShmPool(ShmPool),

    /// shared memory support
    ///
    /// A global singleton object that provides support for shared
    ///  memory.
    ///
    ///  Clients can create wl_shm_pool objects using the create_pool
    ///  request.
    ///
    ///  At connection setup time, the wl_shm object emits one or more
    ///  format events to inform clients about the valid pixel formats
    ///  that can be used for buffers.
    Shm(Shm),

    /// content for a wl_surface
    ///
    /// A buffer provides the content for a wl_surface. Buffers are
    ///  created through factory interfaces such as wl_drm, wl_shm or
    ///  similar. It has a width and a height and can be attached to a
    ///  wl_surface, but the mechanism by which a client provides and
    ///  updates the contents is defined by the buffer factory interface.
    Buffer(Buffer),

    /// offer to transfer data
    ///
    /// A wl_data_offer represents a piece of data offered for transfer
    ///  by another client (the source client).  It is used by the
    ///  copy-and-paste and drag-and-drop mechanisms.  The offer
    ///  describes the different mime types that the data can be
    ///  converted to and provides the mechanism for transferring the
    ///  data directly from the source client.
    DataOffer(DataOffer),

    /// offer to transfer data
    ///
    /// The wl_data_source object is the source side of a wl_data_offer.
    ///  It is created by the source client in a data transfer and
    ///  provides a way to describe the offered data and a way to respond
    ///  to requests to transfer the data.
    DataSource(DataSource),

    /// data transfer device
    ///
    /// There is one wl_data_device per seat which can be obtained
    ///  from the global wl_data_device_manager singleton.
    ///
    ///  A wl_data_device provides access to inter-client data transfer
    ///  mechanisms such as copy-and-paste and drag-and-drop.
    DataDevice(DataDevice),

    /// data transfer interface
    ///
    /// The wl_data_device_manager is a singleton global object that
    ///  provides access to inter-client data transfer mechanisms such as
    ///  copy-and-paste and drag-and-drop.  These mechanisms are tied to
    ///  a wl_seat and this interface lets a client get a wl_data_device
    ///  corresponding to a wl_seat.
    ///
    ///  Depending on the version bound, the objects created from the bound
    ///  wl_data_device_manager object will have different requirements for
    ///  functioning properly. See wl_data_source.set_actions,
    ///  wl_data_offer.accept and wl_data_offer.finish for details.
    DataDeviceManager(DataDeviceManager),

    /// create desktop-style surfaces
    ///
    /// This interface is implemented by servers that provide
    ///  desktop-style user interfaces.
    ///
    ///  It allows clients to associate a wl_shell_surface with
    ///  a basic surface.
    Shell(Shell),

    /// desktop-style metadata interface
    ///
    /// An interface that may be implemented by a wl_surface, for
    ///  implementations that provide a desktop-style user interface.
    ///
    ///  It provides requests to treat surfaces like toplevel, fullscreen
    ///  or popup windows, move, resize or maximize them, associate
    ///  metadata like title and class, etc.
    ///
    ///  On the server side the object is automatically destroyed when
    ///  the related wl_surface is destroyed.  On client side,
    ///  wl_shell_surface_destroy() must be called before destroying
    ///  the wl_surface object.
    ShellSurface(ShellSurface),

    /// an onscreen surface
    ///
    /// A surface is a rectangular area that is displayed on the screen.
    ///  It has a location, size and pixel contents.
    ///
    ///  The size of a surface (and relative positions on it) is described
    ///  in surface local coordinates, which may differ from the buffer
    ///  local coordinates of the pixel content, in case a buffer_transform
    ///  or a buffer_scale is used.
    ///
    ///  A surface without a "role" is fairly useless, a compositor does
    ///  not know where, when or how to present it. The role is the
    ///  purpose of a wl_surface. Examples of roles are a cursor for a
    ///  pointer (as set by wl_pointer.set_cursor), a drag icon
    ///  (wl_data_device.start_drag), a sub-surface
    ///  (wl_subcompositor.get_subsurface), and a window as defined by a
    ///  shell protocol (e.g. wl_shell.get_shell_surface).
    ///
    ///  A surface can have only one role at a time. Initially a
    ///  wl_surface does not have a role. Once a wl_surface is given a
    ///  role, it is set permanently for the whole lifetime of the
    ///  wl_surface object. Giving the current role again is allowed,
    ///  unless explicitly forbidden by the relevant interface
    ///  specification.
    ///
    ///  Surface roles are given by requests in other interfaces such as
    ///  wl_pointer.set_cursor. The request should explicitly mention
    ///  that this request gives a role to a wl_surface. Often, this
    ///  request also creates a new protocol object that represents the
    ///  role and adds additional functionality to wl_surface. When a
    ///  client wants to destroy a wl_surface, they must destroy this 'role
    ///  object' before the wl_surface.
    ///
    ///  Destroying the role object does not remove the role from the
    ///  wl_surface, but it may stop the wl_surface from "playing the role".
    ///  For instance, if a wl_subsurface object is destroyed, the wl_surface
    ///  it was created for will be unmapped and forget its position and
    ///  z-order. It is allowed to create a wl_subsurface for the same
    ///  wl_surface again, but it is not allowed to use the wl_surface as
    ///  a cursor (cursor is a different role than sub-surface, and role
    ///  switching is not allowed).
    Surface(Surface),

    /// group of input devices
    ///
    /// A seat is a group of keyboards, pointer and touch devices. This
    ///  object is published as a global during start up, or when such a
    ///  device is hot plugged.  A seat typically has a pointer and
    ///  maintains a keyboard focus and a pointer focus.
    Seat(Seat),

    /// pointer input device
    ///
    /// The wl_pointer interface represents one or more input devices,
    ///  such as mice, which control the pointer location and pointer_focus
    ///  of a seat.
    ///
    ///  The wl_pointer interface generates motion, enter and leave
    ///  events for the surfaces that the pointer is located over,
    ///  and button and axis events for button presses, button releases
    ///  and scrolling.
    Pointer(Pointer),

    /// keyboard input device
    ///
    /// The wl_keyboard interface represents one or more keyboards
    ///  associated with a seat.
    Keyboard(Keyboard),

    /// touchscreen input device
    ///
    /// The wl_touch interface represents a touchscreen
    ///  associated with a seat.
    ///
    ///  Touch interactions can consist of one or more contacts.
    ///  For each contact, a series of events is generated, starting
    ///  with a down event, followed by zero or more motion events,
    ///  and ending with an up event. Events relating to the same
    ///  contact point can be identified by the ID of the sequence.
    Touch(Touch),

    /// compositor output region
    ///
    /// An output describes part of the compositor geometry.  The
    ///  compositor works in the 'compositor coordinate system' and an
    ///  output corresponds to rectangular area in that space that is
    ///  actually visible.  This typically corresponds to a monitor that
    ///  displays part of the compositor space.  This object is published
    ///  as global during start up, or when a monitor is hotplugged.
    Output(Output),

    /// region interface
    ///
    /// A region object describes an area.
    ///
    ///  Region objects are used to describe the opaque and input
    ///  regions of a surface.
    Region(Region),

    /// sub-surface compositing
    ///
    /// The global interface exposing sub-surface compositing capabilities.
    ///  A wl_surface, that has sub-surfaces associated, is called the
    ///  parent surface. Sub-surfaces can be arbitrarily nested and create
    ///  a tree of sub-surfaces.
    ///
    ///  The root surface in a tree of sub-surfaces is the main
    ///  surface. The main surface cannot be a sub-surface, because
    ///  sub-surfaces must always have a parent.
    ///
    ///  A main surface with its sub-surfaces forms a (compound) window.
    ///  For window management purposes, this set of wl_surface objects is
    ///  to be considered as a single window, and it should also behave as
    ///  such.
    ///
    ///  The aim of sub-surfaces is to offload some of the compositing work
    ///  within a window from clients to the compositor. A prime example is
    ///  a video player with decorations and video in separate wl_surface
    ///  objects. This should allow the compositor to pass YUV video buffer
    ///  processing to dedicated overlay hardware when possible.
    Subcompositor(Subcompositor),

    /// sub-surface interface to a wl_surface
    ///
    /// An additional interface to a wl_surface object, which has been
    ///  made a sub-surface. A sub-surface has one parent surface. A
    ///  sub-surface's size and position are not limited to that of the parent.
    ///  Particularly, a sub-surface is not automatically clipped to its
    ///  parent's area.
    ///
    ///  A sub-surface becomes mapped, when a non-NULL wl_buffer is applied
    ///  and the parent surface is mapped. The order of which one happens
    ///  first is irrelevant. A sub-surface is hidden if the parent becomes
    ///  hidden, or if a NULL wl_buffer is applied. These rules apply
    ///  recursively through the tree of surfaces.
    ///
    ///  The behaviour of wl_surface.commit request on a sub-surface
    ///  depends on the sub-surface's mode. The possible modes are
    ///  synchronized and desynchronized, see methods
    ///  wl_subsurface.set_sync and wl_subsurface.set_desync. Synchronized
    ///  mode caches the wl_surface state to be applied when the parent's
    ///  state gets applied, and desynchronized mode applies the pending
    ///  wl_surface state directly. A sub-surface is initially in the
    ///  synchronized mode.
    ///
    ///  Sub-surfaces have also other kind of state, which is managed by
    ///  wl_subsurface requests, as opposed to wl_surface requests. This
    ///  state includes the sub-surface position relative to the parent
    ///  surface (wl_subsurface.set_position), and the stacking order of
    ///  the parent and its sub-surfaces (wl_subsurface.place_above and
    ///  .place_below). This state is applied when the parent surface's
    ///  wl_surface state is applied, regardless of the sub-surface's mode.
    ///  As the exception, set_sync and set_desync are effective immediately.
    ///
    ///  The main surface can be thought to be always in desynchronized mode,
    ///  since it does not have a parent in the sub-surfaces sense.
    ///
    ///  Even if a sub-surface is in desynchronized mode, it will behave as
    ///  in synchronized mode, if its parent surface behaves as in
    ///  synchronized mode. This rule is applied recursively throughout the
    ///  tree of surfaces. This means, that one can set a sub-surface into
    ///  synchronized mode, and then assume that all its child and grand-child
    ///  sub-surfaces are synchronized, too, without explicitly setting them.
    ///
    ///  If the wl_surface associated with the wl_subsurface is destroyed, the
    ///  wl_subsurface object becomes inert. Note, that destroying either object
    ///  takes effect immediately. If you need to synchronize the removal
    ///  of a sub-surface to the parent surface update, unmap the sub-surface
    ///  first by attaching a NULL wl_buffer, update parent, and then destroy
    ///  the sub-surface.
    ///
    ///  If the parent wl_surface object is destroyed, the sub-surface is
    ///  unmapped.
    Subsurface(Subsurface),
}
impl Interface {
    pub fn interface_name(self) -> Array {
        match self {
            Interface::Display(_) => Display::interface_name(),
            Interface::Registry(_) => Registry::interface_name(),
            Interface::Callback(_) => Callback::interface_name(),
            Interface::Compositor(_) => Compositor::interface_name(),
            Interface::ShmPool(_) => ShmPool::interface_name(),
            Interface::Shm(_) => Shm::interface_name(),
            Interface::Buffer(_) => Buffer::interface_name(),
            Interface::DataOffer(_) => DataOffer::interface_name(),
            Interface::DataSource(_) => DataSource::interface_name(),
            Interface::DataDevice(_) => DataDevice::interface_name(),
            Interface::DataDeviceManager(_) => DataDeviceManager::interface_name(),
            Interface::Shell(_) => Shell::interface_name(),
            Interface::ShellSurface(_) => ShellSurface::interface_name(),
            Interface::Surface(_) => Surface::interface_name(),
            Interface::Seat(_) => Seat::interface_name(),
            Interface::Pointer(_) => Pointer::interface_name(),
            Interface::Keyboard(_) => Keyboard::interface_name(),
            Interface::Touch(_) => Touch::interface_name(),
            Interface::Output(_) => Output::interface_name(),
            Interface::Region(_) => Region::interface_name(),
            Interface::Subcompositor(_) => Subcompositor::interface_name(),
            Interface::Subsurface(_) => Subsurface::interface_name(),
        }
    }
    pub fn from_name(name: &str, id: u32) -> Option<Interface> {
        match name {
            "wl_display" => Some(Interface::Display(Display(id))),
            "wl_registry" => Some(Interface::Registry(Registry(id))),
            "wl_callback" => Some(Interface::Callback(Callback(id))),
            "wl_compositor" => Some(Interface::Compositor(Compositor(id))),
            "wl_shm_pool" => Some(Interface::ShmPool(ShmPool(id))),
            "wl_shm" => Some(Interface::Shm(Shm(id))),
            "wl_buffer" => Some(Interface::Buffer(Buffer(id))),
            "wl_data_offer" => Some(Interface::DataOffer(DataOffer(id))),
            "wl_data_source" => Some(Interface::DataSource(DataSource(id))),
            "wl_data_device" => Some(Interface::DataDevice(DataDevice(id))),
            "wl_data_device_manager" => Some(Interface::DataDeviceManager(DataDeviceManager(id))),
            "wl_shell" => Some(Interface::Shell(Shell(id))),
            "wl_shell_surface" => Some(Interface::ShellSurface(ShellSurface(id))),
            "wl_surface" => Some(Interface::Surface(Surface(id))),
            "wl_seat" => Some(Interface::Seat(Seat(id))),
            "wl_pointer" => Some(Interface::Pointer(Pointer(id))),
            "wl_keyboard" => Some(Interface::Keyboard(Keyboard(id))),
            "wl_touch" => Some(Interface::Touch(Touch(id))),
            "wl_output" => Some(Interface::Output(Output(id))),
            "wl_region" => Some(Interface::Region(Region(id))),
            "wl_subcompositor" => Some(Interface::Subcompositor(Subcompositor(id))),
            "wl_subsurface" => Some(Interface::Subsurface(Subsurface(id))),
            _ => None,
        }
    }
    pub fn id(self) -> u32 {
        match self {
            Interface::Display(Display(id)) => id,
            Interface::Registry(Registry(id)) => id,
            Interface::Callback(Callback(id)) => id,
            Interface::Compositor(Compositor(id)) => id,
            Interface::ShmPool(ShmPool(id)) => id,
            Interface::Shm(Shm(id)) => id,
            Interface::Buffer(Buffer(id)) => id,
            Interface::DataOffer(DataOffer(id)) => id,
            Interface::DataSource(DataSource(id)) => id,
            Interface::DataDevice(DataDevice(id)) => id,
            Interface::DataDeviceManager(DataDeviceManager(id)) => id,
            Interface::Shell(Shell(id)) => id,
            Interface::ShellSurface(ShellSurface(id)) => id,
            Interface::Surface(Surface(id)) => id,
            Interface::Seat(Seat(id)) => id,
            Interface::Pointer(Pointer(id)) => id,
            Interface::Keyboard(Keyboard(id)) => id,
            Interface::Touch(Touch(id)) => id,
            Interface::Output(Output(id)) => id,
            Interface::Region(Region(id)) => id,
            Interface::Subcompositor(Subcompositor(id)) => id,
            Interface::Subsurface(Subsurface(id)) => id,
        }
    }
    pub fn set_id(&mut self, id: u32) {
        match *self {
            Interface::Display(ref mut inner) => inner.set_id(id),
            Interface::Registry(ref mut inner) => inner.set_id(id),
            Interface::Callback(ref mut inner) => inner.set_id(id),
            Interface::Compositor(ref mut inner) => inner.set_id(id),
            Interface::ShmPool(ref mut inner) => inner.set_id(id),
            Interface::Shm(ref mut inner) => inner.set_id(id),
            Interface::Buffer(ref mut inner) => inner.set_id(id),
            Interface::DataOffer(ref mut inner) => inner.set_id(id),
            Interface::DataSource(ref mut inner) => inner.set_id(id),
            Interface::DataDevice(ref mut inner) => inner.set_id(id),
            Interface::DataDeviceManager(ref mut inner) => inner.set_id(id),
            Interface::Shell(ref mut inner) => inner.set_id(id),
            Interface::ShellSurface(ref mut inner) => inner.set_id(id),
            Interface::Surface(ref mut inner) => inner.set_id(id),
            Interface::Seat(ref mut inner) => inner.set_id(id),
            Interface::Pointer(ref mut inner) => inner.set_id(id),
            Interface::Keyboard(ref mut inner) => inner.set_id(id),
            Interface::Touch(ref mut inner) => inner.set_id(id),
            Interface::Output(ref mut inner) => inner.set_id(id),
            Interface::Region(ref mut inner) => inner.set_id(id),
            Interface::Subcompositor(ref mut inner) => inner.set_id(id),
            Interface::Subsurface(ref mut inner) => inner.set_id(id),
        }
    }
    pub fn interface_version(self) -> u32 {
        match self {
            Interface::Display(_) => Display::interface_version(),
            Interface::Registry(_) => Registry::interface_version(),
            Interface::Callback(_) => Callback::interface_version(),
            Interface::Compositor(_) => Compositor::interface_version(),
            Interface::ShmPool(_) => ShmPool::interface_version(),
            Interface::Shm(_) => Shm::interface_version(),
            Interface::Buffer(_) => Buffer::interface_version(),
            Interface::DataOffer(_) => DataOffer::interface_version(),
            Interface::DataSource(_) => DataSource::interface_version(),
            Interface::DataDevice(_) => DataDevice::interface_version(),
            Interface::DataDeviceManager(_) => DataDeviceManager::interface_version(),
            Interface::Shell(_) => Shell::interface_version(),
            Interface::ShellSurface(_) => ShellSurface::interface_version(),
            Interface::Surface(_) => Surface::interface_version(),
            Interface::Seat(_) => Seat::interface_version(),
            Interface::Pointer(_) => Pointer::interface_version(),
            Interface::Keyboard(_) => Keyboard::interface_version(),
            Interface::Touch(_) => Touch::interface_version(),
            Interface::Output(_) => Output::interface_version(),
            Interface::Region(_) => Region::interface_version(),
            Interface::Subcompositor(_) => Subcompositor::interface_version(),
            Interface::Subsurface(_) => Subsurface::interface_version(),
        }
    }
}
impl From<Display> for Interface {
    fn from(x: Display) -> Interface {
        Interface::Display(x)
    }
}
impl From<Registry> for Interface {
    fn from(x: Registry) -> Interface {
        Interface::Registry(x)
    }
}
impl From<Callback> for Interface {
    fn from(x: Callback) -> Interface {
        Interface::Callback(x)
    }
}
impl From<Compositor> for Interface {
    fn from(x: Compositor) -> Interface {
        Interface::Compositor(x)
    }
}
impl From<ShmPool> for Interface {
    fn from(x: ShmPool) -> Interface {
        Interface::ShmPool(x)
    }
}
impl From<Shm> for Interface {
    fn from(x: Shm) -> Interface {
        Interface::Shm(x)
    }
}
impl From<Buffer> for Interface {
    fn from(x: Buffer) -> Interface {
        Interface::Buffer(x)
    }
}
impl From<DataOffer> for Interface {
    fn from(x: DataOffer) -> Interface {
        Interface::DataOffer(x)
    }
}
impl From<DataSource> for Interface {
    fn from(x: DataSource) -> Interface {
        Interface::DataSource(x)
    }
}
impl From<DataDevice> for Interface {
    fn from(x: DataDevice) -> Interface {
        Interface::DataDevice(x)
    }
}
impl From<DataDeviceManager> for Interface {
    fn from(x: DataDeviceManager) -> Interface {
        Interface::DataDeviceManager(x)
    }
}
impl From<Shell> for Interface {
    fn from(x: Shell) -> Interface {
        Interface::Shell(x)
    }
}
impl From<ShellSurface> for Interface {
    fn from(x: ShellSurface) -> Interface {
        Interface::ShellSurface(x)
    }
}
impl From<Surface> for Interface {
    fn from(x: Surface) -> Interface {
        Interface::Surface(x)
    }
}
impl From<Seat> for Interface {
    fn from(x: Seat) -> Interface {
        Interface::Seat(x)
    }
}
impl From<Pointer> for Interface {
    fn from(x: Pointer) -> Interface {
        Interface::Pointer(x)
    }
}
impl From<Keyboard> for Interface {
    fn from(x: Keyboard) -> Interface {
        Interface::Keyboard(x)
    }
}
impl From<Touch> for Interface {
    fn from(x: Touch) -> Interface {
        Interface::Touch(x)
    }
}
impl From<Output> for Interface {
    fn from(x: Output) -> Interface {
        Interface::Output(x)
    }
}
impl From<Region> for Interface {
    fn from(x: Region) -> Interface {
        Interface::Region(x)
    }
}
impl From<Subcompositor> for Interface {
    fn from(x: Subcompositor) -> Interface {
        Interface::Subcompositor(x)
    }
}
impl From<Subsurface> for Interface {
    fn from(x: Subsurface) -> Interface {
        Interface::Subsurface(x)
    }
}
pub mod requests {
    pub use interfaces::display::Sync as DisplaySync;
    pub use interfaces::display::GetRegistry as DisplayGetRegistry;
    pub use interfaces::registry::Bind as RegistryBind;
    pub use interfaces::compositor::CreateSurface as CompositorCreateSurface;
    pub use interfaces::compositor::CreateRegion as CompositorCreateRegion;
    pub use interfaces::shm_pool::CreateBuffer as ShmPoolCreateBuffer;
    pub use interfaces::shm_pool::Destroy as ShmPoolDestroy;
    pub use interfaces::shm_pool::Resize as ShmPoolResize;
    pub use interfaces::shm::CreatePool as ShmCreatePool;
    pub use interfaces::buffer::Destroy as BufferDestroy;
    pub use interfaces::data_offer::Accept as DataOfferAccept;
    pub use interfaces::data_offer::Receive as DataOfferReceive;
    pub use interfaces::data_offer::Destroy as DataOfferDestroy;
    pub use interfaces::data_offer::Finish as DataOfferFinish;
    pub use interfaces::data_offer::SetActions as DataOfferSetActions;
    pub use interfaces::data_source::Offer as DataSourceOffer;
    pub use interfaces::data_source::Destroy as DataSourceDestroy;
    pub use interfaces::data_source::SetActions as DataSourceSetActions;
    pub use interfaces::data_device::StartDrag as DataDeviceStartDrag;
    pub use interfaces::data_device::SetSelection as DataDeviceSetSelection;
    pub use interfaces::data_device::Release as DataDeviceRelease;
    pub use interfaces::data_device_manager::CreateDataSource as DataDeviceManagerCreateDataSource;
    pub use interfaces::data_device_manager::GetDataDevice as DataDeviceManagerGetDataDevice;
    pub use interfaces::shell::GetShellSurface as ShellGetShellSurface;
    pub use interfaces::shell_surface::Pong as ShellSurfacePong;
    pub use interfaces::shell_surface::Move as ShellSurfaceMove;
    pub use interfaces::shell_surface::Resize as ShellSurfaceResize;
    pub use interfaces::shell_surface::SetToplevel as ShellSurfaceSetToplevel;
    pub use interfaces::shell_surface::SetTransient as ShellSurfaceSetTransient;
    pub use interfaces::shell_surface::SetFullscreen as ShellSurfaceSetFullscreen;
    pub use interfaces::shell_surface::SetPopup as ShellSurfaceSetPopup;
    pub use interfaces::shell_surface::SetMaximized as ShellSurfaceSetMaximized;
    pub use interfaces::shell_surface::SetTitle as ShellSurfaceSetTitle;
    pub use interfaces::shell_surface::SetClass as ShellSurfaceSetClass;
    pub use interfaces::surface::Destroy as SurfaceDestroy;
    pub use interfaces::surface::Attach as SurfaceAttach;
    pub use interfaces::surface::Damage as SurfaceDamage;
    pub use interfaces::surface::Frame as SurfaceFrame;
    pub use interfaces::surface::SetOpaqueRegion as SurfaceSetOpaqueRegion;
    pub use interfaces::surface::SetInputRegion as SurfaceSetInputRegion;
    pub use interfaces::surface::Commit as SurfaceCommit;
    pub use interfaces::surface::SetBufferTransform as SurfaceSetBufferTransform;
    pub use interfaces::surface::SetBufferScale as SurfaceSetBufferScale;
    pub use interfaces::surface::DamageBuffer as SurfaceDamageBuffer;
    pub use interfaces::seat::GetPointer as SeatGetPointer;
    pub use interfaces::seat::GetKeyboard as SeatGetKeyboard;
    pub use interfaces::seat::GetTouch as SeatGetTouch;
    pub use interfaces::seat::Release as SeatRelease;
    pub use interfaces::pointer::SetCursor as PointerSetCursor;
    pub use interfaces::pointer::Release as PointerRelease;
    pub use interfaces::keyboard::Release as KeyboardRelease;
    pub use interfaces::touch::Release as TouchRelease;
    pub use interfaces::region::Destroy as RegionDestroy;
    pub use interfaces::region::Add as RegionAdd;
    pub use interfaces::region::Subtract as RegionSubtract;
    pub use interfaces::subcompositor::Destroy as SubcompositorDestroy;
    pub use interfaces::subcompositor::GetSubsurface as SubcompositorGetSubsurface;
    pub use interfaces::subsurface::Destroy as SubsurfaceDestroy;
    pub use interfaces::subsurface::SetPosition as SubsurfaceSetPosition;
    pub use interfaces::subsurface::PlaceAbove as SubsurfacePlaceAbove;
    pub use interfaces::subsurface::PlaceBelow as SubsurfacePlaceBelow;
    pub use interfaces::subsurface::SetSync as SubsurfaceSetSync;
    pub use interfaces::subsurface::SetDesync as SubsurfaceSetDesync;
}
/// Enumeration of all possible request types.
#[derive(Debug)]
pub enum Request {
    DisplaySync(Display, display::Sync),
    DisplayGetRegistry(Display, display::GetRegistry),
    RegistryBind(Registry, registry::Bind),
    CompositorCreateSurface(Compositor, compositor::CreateSurface),
    CompositorCreateRegion(Compositor, compositor::CreateRegion),
    ShmPoolCreateBuffer(ShmPool, shm_pool::CreateBuffer),
    ShmPoolDestroy(ShmPool, shm_pool::Destroy),
    ShmPoolResize(ShmPool, shm_pool::Resize),
    ShmCreatePool(Shm, shm::CreatePool),
    BufferDestroy(Buffer, buffer::Destroy),
    DataOfferAccept(DataOffer, data_offer::Accept),
    DataOfferReceive(DataOffer, data_offer::Receive),
    DataOfferDestroy(DataOffer, data_offer::Destroy),
    DataOfferFinish(DataOffer, data_offer::Finish),
    DataOfferSetActions(DataOffer, data_offer::SetActions),
    DataSourceOffer(DataSource, data_source::Offer),
    DataSourceDestroy(DataSource, data_source::Destroy),
    DataSourceSetActions(DataSource, data_source::SetActions),
    DataDeviceStartDrag(DataDevice, data_device::StartDrag),
    DataDeviceSetSelection(DataDevice, data_device::SetSelection),
    DataDeviceRelease(DataDevice, data_device::Release),
    DataDeviceManagerCreateDataSource(DataDeviceManager, data_device_manager::CreateDataSource),
    DataDeviceManagerGetDataDevice(DataDeviceManager, data_device_manager::GetDataDevice),
    ShellGetShellSurface(Shell, shell::GetShellSurface),
    ShellSurfacePong(ShellSurface, shell_surface::Pong),
    ShellSurfaceMove(ShellSurface, shell_surface::Move),
    ShellSurfaceResize(ShellSurface, shell_surface::Resize),
    ShellSurfaceSetToplevel(ShellSurface, shell_surface::SetToplevel),
    ShellSurfaceSetTransient(ShellSurface, shell_surface::SetTransient),
    ShellSurfaceSetFullscreen(ShellSurface, shell_surface::SetFullscreen),
    ShellSurfaceSetPopup(ShellSurface, shell_surface::SetPopup),
    ShellSurfaceSetMaximized(ShellSurface, shell_surface::SetMaximized),
    ShellSurfaceSetTitle(ShellSurface, shell_surface::SetTitle),
    ShellSurfaceSetClass(ShellSurface, shell_surface::SetClass),
    SurfaceDestroy(Surface, surface::Destroy),
    SurfaceAttach(Surface, surface::Attach),
    SurfaceDamage(Surface, surface::Damage),
    SurfaceFrame(Surface, surface::Frame),
    SurfaceSetOpaqueRegion(Surface, surface::SetOpaqueRegion),
    SurfaceSetInputRegion(Surface, surface::SetInputRegion),
    SurfaceCommit(Surface, surface::Commit),
    SurfaceSetBufferTransform(Surface, surface::SetBufferTransform),
    SurfaceSetBufferScale(Surface, surface::SetBufferScale),
    SurfaceDamageBuffer(Surface, surface::DamageBuffer),
    SeatGetPointer(Seat, seat::GetPointer),
    SeatGetKeyboard(Seat, seat::GetKeyboard),
    SeatGetTouch(Seat, seat::GetTouch),
    SeatRelease(Seat, seat::Release),
    PointerSetCursor(Pointer, pointer::SetCursor),
    PointerRelease(Pointer, pointer::Release),
    KeyboardRelease(Keyboard, keyboard::Release),
    TouchRelease(Touch, touch::Release),
    RegionDestroy(Region, region::Destroy),
    RegionAdd(Region, region::Add),
    RegionSubtract(Region, region::Subtract),
    SubcompositorDestroy(Subcompositor, subcompositor::Destroy),
    SubcompositorGetSubsurface(Subcompositor, subcompositor::GetSubsurface),
    SubsurfaceDestroy(Subsurface, subsurface::Destroy),
    SubsurfaceSetPosition(Subsurface, subsurface::SetPosition),
    SubsurfacePlaceAbove(Subsurface, subsurface::PlaceAbove),
    SubsurfacePlaceBelow(Subsurface, subsurface::PlaceBelow),
    SubsurfaceSetSync(Subsurface, subsurface::SetSync),
    SubsurfaceSetDesync(Subsurface, subsurface::SetDesync),
}
/// Enumeration of all possible request types.
#[derive(Debug)]
pub enum Event {
    DisplayError(Display, display::Error),
    DisplayDeleteId(Display, display::DeleteId),
    RegistryGlobal(Registry, registry::Global),
    RegistryGlobalRemove(Registry, registry::GlobalRemove),
    CallbackDone(Callback, callback::Done),
    ShmFormat(Shm, shm::Format),
    BufferRelease(Buffer, buffer::Release),
    DataOfferOffer(DataOffer, data_offer::Offer),
    DataOfferSourceActions(DataOffer, data_offer::SourceActions),
    DataOfferAction(DataOffer, data_offer::Action),
    DataSourceTarget(DataSource, data_source::Target),
    DataSourceSend(DataSource, data_source::Send),
    DataSourceCancelled(DataSource, data_source::Cancelled),
    DataSourceDndDropPerformed(DataSource, data_source::DndDropPerformed),
    DataSourceDndFinished(DataSource, data_source::DndFinished),
    DataSourceAction(DataSource, data_source::Action),
    DataDeviceDataOffer(DataDevice, data_device::DataOffer),
    DataDeviceEnter(DataDevice, data_device::Enter),
    DataDeviceLeave(DataDevice, data_device::Leave),
    DataDeviceMotion(DataDevice, data_device::Motion),
    DataDeviceDrop(DataDevice, data_device::Drop),
    DataDeviceSelection(DataDevice, data_device::Selection),
    ShellSurfacePing(ShellSurface, shell_surface::Ping),
    ShellSurfaceConfigure(ShellSurface, shell_surface::Configure),
    ShellSurfacePopupDone(ShellSurface, shell_surface::PopupDone),
    SurfaceEnter(Surface, surface::Enter),
    SurfaceLeave(Surface, surface::Leave),
    SeatCapabilities(Seat, seat::Capabilities),
    SeatName(Seat, seat::Name),
    PointerEnter(Pointer, pointer::Enter),
    PointerLeave(Pointer, pointer::Leave),
    PointerMotion(Pointer, pointer::Motion),
    PointerButton(Pointer, pointer::Button),
    PointerAxis(Pointer, pointer::Axis),
    PointerFrame(Pointer, pointer::Frame),
    PointerAxisSource(Pointer, pointer::AxisSource),
    PointerAxisStop(Pointer, pointer::AxisStop),
    PointerAxisDiscrete(Pointer, pointer::AxisDiscrete),
    KeyboardKeymap(Keyboard, keyboard::Keymap),
    KeyboardEnter(Keyboard, keyboard::Enter),
    KeyboardLeave(Keyboard, keyboard::Leave),
    KeyboardKey(Keyboard, keyboard::Key),
    KeyboardModifiers(Keyboard, keyboard::Modifiers),
    KeyboardRepeatInfo(Keyboard, keyboard::RepeatInfo),
    TouchDown(Touch, touch::Down),
    TouchUp(Touch, touch::Up),
    TouchMotion(Touch, touch::Motion),
    TouchFrame(Touch, touch::Frame),
    TouchCancel(Touch, touch::Cancel),
    OutputGeometry(Output, output::Geometry),
    OutputMode(Output, output::Mode),
    OutputDone(Output, output::Done),
    OutputScale(Output, output::Scale),
}
/// Fixed width decimal
/// Wire format: 23 bits integer, 8 bits decimal
/// Rust format is identical
# [ repr ( C ) ]
# [ derive ( Debug , PartialEq , Eq , PartialOrd , Ord ) ]
pub struct Fixed(u32);
/// Wayland Array, also used for string

/// Wire format: 32 bit length followed by data. If string null terminated string
/// Rust format differs, strings do not include null terminator.
# [ derive ( Debug ) ]
pub struct Array {
    pub contents: Vec<u8>,
}
impl Array {
    pub fn from_string(s: String) -> Array {
        let mut ret = s.into_bytes();
        ret.push(0);
        Array { contents: ret }
    }
    pub fn as_str(&self) -> Option<&str> {
        std::str::from_utf8(&self.contents[..self.contents.len() - 1]).ok()
    }
}
/// New ID
///

/// Wrapper type indicating the contained item is a new id/object to allow the app
/// to initialized datastructures before use.
# [ derive ( Debug , PartialEq , Eq ) ]
pub struct New<T>(pub T);
pub trait ObjectStore: Sized + Default {
    fn get(&self, id: u32) -> Option<Interface>;
}
pub struct Connection<T: ObjectStore> {
    pub fd: fd,
    pub objects: T,
}
pub enum GetMessagesError {
    Nix(NixError),
    Closed,
}
impl<T: ObjectStore> Connection<T> {
    pub fn new(fd: fd) -> Connection<T> {
        Connection {
            fd: fd,
            objects: T::default(),
        }
    }
}
/// Please note that error types are given on a best effort bases,
/// a malformed request may show as incomplete.
# [ derive ( Debug , Copy , Clone , PartialEq , Eq ) ]
pub enum ReadError {
    Incomplete,
    Malformed,
    NoSuchObject,
    UnkownMethod(Interface, u16),
}
/// Header sent for each request    
# [ repr ( C ) ]
# [ derive ( Debug , Clone , Copy ) ]
struct Header {
    id: u32,
    opcode: u16,
    size: u16,
}
unsafe fn get_request<T: ObjectStore>(objects: &T,
                                      header: Header,
                                      mut data: &[u32],
                                      mut fds: &[fd])
                                      -> Result<Request, throw::Error<ReadError>> {
    // TODO: Gracefully handle broken clients
    let object = up!(objects.get(header.id)
                            .ok_or(throw::Error::new(ReadError::NoSuchObject)));
    match (object, header.opcode) {
        (Interface::Display(object), 0) => {
            let callback_id_rust = *up!(data.get(0)
                                            .ok_or(throw::Error::new(ReadError::Incomplete)));
            let callback = Callback(callback_id_rust);
            let callback = New(callback);
            data = &data[1..];

            Ok(Request::DisplaySync(object, display::Sync { callback: callback }))
        }
        (Interface::Display(object), 1) => {
            let registry_id_rust = *up!(data.get(0)
                                            .ok_or(throw::Error::new(ReadError::Incomplete)));
            let registry = Registry(registry_id_rust);
            let registry = New(registry);
            data = &data[1..];

            Ok(Request::DisplayGetRegistry(object, display::GetRegistry { registry: registry }))
        }
        (Interface::Registry(object), 0) => {
            // not an emum
            let name = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let len = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as usize;
            data = &data[1..];
            if data.len() * 4 < len {
                throw_new!(ReadError::Malformed);
            }
            let interface = Array {
                contents: std::slice::from_raw_parts(data.as_ptr() as *const u8, len as usize)
                              .to_vec(),
            };
            data = &data[len / 4..];
            if len % 4 != 0 {
                data = &data[1..];
            };

            // not an emum
            let version = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let id_id_rust = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            let id = {
                let name_rust = up!(interface.as_str()
                                             .ok_or(throw::Error::new(ReadError::Malformed)));
                Interface::from_name(name_rust, id_id_rust)
            };
            let id = up!(id.ok_or(throw::Error::new(ReadError::Malformed)));
            let id = New(id);
            data = &data[1..];

            Ok(Request::RegistryBind(object,
                                     registry::Bind {
                                         name: name,
                                         interface: interface,
                                         version: version,
                                         id: id,
                                     }))
        }
        (Interface::Compositor(object), 0) => {
            let id_id_rust = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            let id = Surface(id_id_rust);
            let id = New(id);
            data = &data[1..];

            Ok(Request::CompositorCreateSurface(object, compositor::CreateSurface { id: id }))
        }
        (Interface::Compositor(object), 1) => {
            let id_id_rust = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            let id = Region(id_id_rust);
            let id = New(id);
            data = &data[1..];

            Ok(Request::CompositorCreateRegion(object, compositor::CreateRegion { id: id }))
        }
        (Interface::ShmPool(object), 0) => {
            let id_id_rust = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            let id = Buffer(id_id_rust);
            let id = New(id);
            data = &data[1..];

            let offset = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            let width = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            let height = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            let stride = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            // not an emum
            let format = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            Ok(Request::ShmPoolCreateBuffer(object,
                                            shm_pool::CreateBuffer {
                                                id: id,
                                                offset: offset,
                                                width: width,
                                                height: height,
                                                stride: stride,
                                                format: format,
                                            }))
        }
        (Interface::ShmPool(object), 1) => Ok(Request::ShmPoolDestroy(object, shm_pool::Destroy)),
        (Interface::ShmPool(object), 2) => {
            let size = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            Ok(Request::ShmPoolResize(object, shm_pool::Resize { size: size }))
        }
        (Interface::Shm(object), 0) => {
            let id_id_rust = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            let id = ShmPool(id_id_rust);
            let id = New(id);
            data = &data[1..];

            let fd = fds[0];
            fds = &fds[1..];

            let size = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            Ok(Request::ShmCreatePool(object,
                                      shm::CreatePool {
                                          id: id,
                                          fd: fd,
                                          size: size,
                                      }))
        }
        (Interface::Buffer(object), 0) => Ok(Request::BufferDestroy(object, buffer::Destroy)),
        (Interface::DataOffer(object), 0) => {
            // not an emum
            let serial = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let len = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as usize;
            data = &data[1..];
            if data.len() * 4 < len {
                throw_new!(ReadError::Malformed);
            }
            let mime_type = Array {
                contents: std::slice::from_raw_parts(data.as_ptr() as *const u8, len as usize)
                              .to_vec(),
            };
            data = &data[len / 4..];
            if len % 4 != 0 {
                data = &data[1..];
            };

            Ok(Request::DataOfferAccept(object,
                                        data_offer::Accept {
                                            serial: serial,
                                            mime_type: mime_type,
                                        }))
        }
        (Interface::DataOffer(object), 1) => {
            let len = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as usize;
            data = &data[1..];
            if data.len() * 4 < len {
                throw_new!(ReadError::Malformed);
            }
            let mime_type = Array {
                contents: std::slice::from_raw_parts(data.as_ptr() as *const u8, len as usize)
                              .to_vec(),
            };
            data = &data[len / 4..];
            if len % 4 != 0 {
                data = &data[1..];
            };

            let fd = fds[0];
            fds = &fds[1..];

            Ok(Request::DataOfferReceive(object,
                                         data_offer::Receive {
                                             mime_type: mime_type,
                                             fd: fd,
                                         }))
        }
        (Interface::DataOffer(object), 2) => {
            Ok(Request::DataOfferDestroy(object, data_offer::Destroy))
        }
        (Interface::DataOffer(object), 3) => {
            Ok(Request::DataOfferFinish(object, data_offer::Finish))
        }
        (Interface::DataOffer(object), 4) => {
            // not an emum
            let dnd_actions = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            // not an emum
            let preferred_action = *up!(data.get(0)
                                            .ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            Ok(Request::DataOfferSetActions(object,
                                            data_offer::SetActions {
                                                dnd_actions: dnd_actions,
                                                preferred_action: preferred_action,
                                            }))
        }
        (Interface::DataSource(object), 0) => {
            let len = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as usize;
            data = &data[1..];
            if data.len() * 4 < len {
                throw_new!(ReadError::Malformed);
            }
            let mime_type = Array {
                contents: std::slice::from_raw_parts(data.as_ptr() as *const u8, len as usize)
                              .to_vec(),
            };
            data = &data[len / 4..];
            if len % 4 != 0 {
                data = &data[1..];
            };

            Ok(Request::DataSourceOffer(object, data_source::Offer { mime_type: mime_type }))
        }
        (Interface::DataSource(object), 1) => {
            Ok(Request::DataSourceDestroy(object, data_source::Destroy))
        }
        (Interface::DataSource(object), 2) => {
            // not an emum
            let dnd_actions = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            Ok(Request::DataSourceSetActions(object,
                                             data_source::SetActions { dnd_actions: dnd_actions }))
        }
        (Interface::DataDevice(object), 0) => {
            let source = DataSource(*up!(data.get(0)
                                             .ok_or(throw::Error::new(ReadError::Incomplete))));
            data = &data[1..];

            let origin = Surface(*up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))));
            data = &data[1..];

            let icon = Surface(*up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))));
            data = &data[1..];

            // not an emum
            let serial = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            Ok(Request::DataDeviceStartDrag(object,
                                            data_device::StartDrag {
                                                source: source,
                                                origin: origin,
                                                icon: icon,
                                                serial: serial,
                                            }))
        }
        (Interface::DataDevice(object), 1) => {
            let source = DataSource(*up!(data.get(0)
                                             .ok_or(throw::Error::new(ReadError::Incomplete))));
            data = &data[1..];

            // not an emum
            let serial = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            Ok(Request::DataDeviceSetSelection(object,
                                               data_device::SetSelection {
                                                   source: source,
                                                   serial: serial,
                                               }))
        }
        (Interface::DataDevice(object), 2) => {
            Ok(Request::DataDeviceRelease(object, data_device::Release))
        }
        (Interface::DataDeviceManager(object), 0) => {
            let id_id_rust = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            let id = DataSource(id_id_rust);
            let id = New(id);
            data = &data[1..];

            Ok(Request::DataDeviceManagerCreateDataSource(object,
                                                          data_device_manager::CreateDataSource {
                                                              id: id,
                                                          }))
        }
        (Interface::DataDeviceManager(object), 1) => {
            let id_id_rust = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            let id = DataDevice(id_id_rust);
            let id = New(id);
            data = &data[1..];

            let seat = Seat(*up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))));
            data = &data[1..];

            Ok(Request::DataDeviceManagerGetDataDevice(object,
                                                       data_device_manager::GetDataDevice {
                                                           id: id,
                                                           seat: seat,
                                                       }))
        }
        (Interface::Shell(object), 0) => {
            let id_id_rust = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            let id = ShellSurface(id_id_rust);
            let id = New(id);
            data = &data[1..];

            let surface = Surface(*up!(data.get(0)
                                           .ok_or(throw::Error::new(ReadError::Incomplete))));
            data = &data[1..];

            Ok(Request::ShellGetShellSurface(object,
                                             shell::GetShellSurface {
                                                 id: id,
                                                 surface: surface,
                                             }))
        }
        (Interface::ShellSurface(object), 0) => {
            // not an emum
            let serial = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            Ok(Request::ShellSurfacePong(object, shell_surface::Pong { serial: serial }))
        }
        (Interface::ShellSurface(object), 1) => {
            let seat = Seat(*up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))));
            data = &data[1..];

            // not an emum
            let serial = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            Ok(Request::ShellSurfaceMove(object,
                                         shell_surface::Move {
                                             seat: seat,
                                             serial: serial,
                                         }))
        }
        (Interface::ShellSurface(object), 2) => {
            let seat = Seat(*up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))));
            data = &data[1..];

            // not an emum
            let serial = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            // enum!
            let edges = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let edges = ::interfaces::shell_surface::resize(edges);

            Ok(Request::ShellSurfaceResize(object,
                                           shell_surface::Resize {
                                               seat: seat,
                                               serial: serial,
                                               edges: edges,
                                           }))
        }
        (Interface::ShellSurface(object), 3) => {
            Ok(Request::ShellSurfaceSetToplevel(object, shell_surface::SetToplevel))
        }
        (Interface::ShellSurface(object), 4) => {
            let parent = Surface(*up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))));
            data = &data[1..];

            let x = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            let y = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            // enum!
            let flags = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let flags = ::interfaces::shell_surface::transient(flags);

            Ok(Request::ShellSurfaceSetTransient(object,
                                                 shell_surface::SetTransient {
                                                     parent: parent,
                                                     x: x,
                                                     y: y,
                                                     flags: flags,
                                                 }))
        }
        (Interface::ShellSurface(object), 5) => {
            // enum!
            let method = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let method = ::interfaces::shell_surface::fullscreen_method(method);

            // not an emum
            let framerate = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let output = Output(*up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))));
            data = &data[1..];

            Ok(Request::ShellSurfaceSetFullscreen(object,
                                                  shell_surface::SetFullscreen {
                                                      method: method,
                                                      framerate: framerate,
                                                      output: output,
                                                  }))
        }
        (Interface::ShellSurface(object), 6) => {
            let seat = Seat(*up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))));
            data = &data[1..];

            // not an emum
            let serial = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let parent = Surface(*up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))));
            data = &data[1..];

            let x = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            let y = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            // enum!
            let flags = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let flags = ::interfaces::shell_surface::transient(flags);

            Ok(Request::ShellSurfaceSetPopup(object,
                                             shell_surface::SetPopup {
                                                 seat: seat,
                                                 serial: serial,
                                                 parent: parent,
                                                 x: x,
                                                 y: y,
                                                 flags: flags,
                                             }))
        }
        (Interface::ShellSurface(object), 7) => {
            let output = Output(*up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))));
            data = &data[1..];

            Ok(Request::ShellSurfaceSetMaximized(object,
                                                 shell_surface::SetMaximized { output: output }))
        }
        (Interface::ShellSurface(object), 8) => {
            let len = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as usize;
            data = &data[1..];
            if data.len() * 4 < len {
                throw_new!(ReadError::Malformed);
            }
            let title = Array {
                contents: std::slice::from_raw_parts(data.as_ptr() as *const u8, len as usize)
                              .to_vec(),
            };
            data = &data[len / 4..];
            if len % 4 != 0 {
                data = &data[1..];
            };

            Ok(Request::ShellSurfaceSetTitle(object, shell_surface::SetTitle { title: title }))
        }
        (Interface::ShellSurface(object), 9) => {
            let len = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as usize;
            data = &data[1..];
            if data.len() * 4 < len {
                throw_new!(ReadError::Malformed);
            }
            let class_ = Array {
                contents: std::slice::from_raw_parts(data.as_ptr() as *const u8, len as usize)
                              .to_vec(),
            };
            data = &data[len / 4..];
            if len % 4 != 0 {
                data = &data[1..];
            };

            Ok(Request::ShellSurfaceSetClass(object, shell_surface::SetClass { class_: class_ }))
        }
        (Interface::Surface(object), 0) => Ok(Request::SurfaceDestroy(object, surface::Destroy)),
        (Interface::Surface(object), 1) => {
            let buffer = Buffer(*up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))));
            data = &data[1..];

            let x = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            let y = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            Ok(Request::SurfaceAttach(object,
                                      surface::Attach {
                                          buffer: buffer,
                                          x: x,
                                          y: y,
                                      }))
        }
        (Interface::Surface(object), 2) => {
            let x = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            let y = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            let width = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            let height = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            Ok(Request::SurfaceDamage(object,
                                      surface::Damage {
                                          x: x,
                                          y: y,
                                          width: width,
                                          height: height,
                                      }))
        }
        (Interface::Surface(object), 3) => {
            let callback_id_rust = *up!(data.get(0)
                                            .ok_or(throw::Error::new(ReadError::Incomplete)));
            let callback = Callback(callback_id_rust);
            let callback = New(callback);
            data = &data[1..];

            Ok(Request::SurfaceFrame(object, surface::Frame { callback: callback }))
        }
        (Interface::Surface(object), 4) => {
            let region = Region(*up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))));
            data = &data[1..];

            Ok(Request::SurfaceSetOpaqueRegion(object, surface::SetOpaqueRegion { region: region }))
        }
        (Interface::Surface(object), 5) => {
            let region = Region(*up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))));
            data = &data[1..];

            Ok(Request::SurfaceSetInputRegion(object, surface::SetInputRegion { region: region }))
        }
        (Interface::Surface(object), 6) => Ok(Request::SurfaceCommit(object, surface::Commit)),
        (Interface::Surface(object), 7) => {
            let transform =
                *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            Ok(Request::SurfaceSetBufferTransform(object,
                                                  surface::SetBufferTransform {
                                                      transform: transform,
                                                  }))
        }
        (Interface::Surface(object), 8) => {
            let scale = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            Ok(Request::SurfaceSetBufferScale(object, surface::SetBufferScale { scale: scale }))
        }
        (Interface::Surface(object), 9) => {
            let x = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            let y = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            let width = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            let height = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            Ok(Request::SurfaceDamageBuffer(object,
                                            surface::DamageBuffer {
                                                x: x,
                                                y: y,
                                                width: width,
                                                height: height,
                                            }))
        }
        (Interface::Seat(object), 0) => {
            let id_id_rust = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            let id = Pointer(id_id_rust);
            let id = New(id);
            data = &data[1..];

            Ok(Request::SeatGetPointer(object, seat::GetPointer { id: id }))
        }
        (Interface::Seat(object), 1) => {
            let id_id_rust = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            let id = Keyboard(id_id_rust);
            let id = New(id);
            data = &data[1..];

            Ok(Request::SeatGetKeyboard(object, seat::GetKeyboard { id: id }))
        }
        (Interface::Seat(object), 2) => {
            let id_id_rust = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            let id = Touch(id_id_rust);
            let id = New(id);
            data = &data[1..];

            Ok(Request::SeatGetTouch(object, seat::GetTouch { id: id }))
        }
        (Interface::Seat(object), 3) => Ok(Request::SeatRelease(object, seat::Release)),
        (Interface::Pointer(object), 0) => {
            // not an emum
            let serial = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let surface = Surface(*up!(data.get(0)
                                           .ok_or(throw::Error::new(ReadError::Incomplete))));
            data = &data[1..];

            let hotspot_x =
                *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            let hotspot_y =
                *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            Ok(Request::PointerSetCursor(object,
                                         pointer::SetCursor {
                                             serial: serial,
                                             surface: surface,
                                             hotspot_x: hotspot_x,
                                             hotspot_y: hotspot_y,
                                         }))
        }
        (Interface::Pointer(object), 1) => Ok(Request::PointerRelease(object, pointer::Release)),
        (Interface::Keyboard(object), 0) => Ok(Request::KeyboardRelease(object, keyboard::Release)),
        (Interface::Touch(object), 0) => Ok(Request::TouchRelease(object, touch::Release)),
        (Interface::Region(object), 0) => Ok(Request::RegionDestroy(object, region::Destroy)),
        (Interface::Region(object), 1) => {
            let x = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            let y = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            let width = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            let height = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            Ok(Request::RegionAdd(object,
                                  region::Add {
                                      x: x,
                                      y: y,
                                      width: width,
                                      height: height,
                                  }))
        }
        (Interface::Region(object), 2) => {
            let x = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            let y = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            let width = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            let height = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            Ok(Request::RegionSubtract(object,
                                       region::Subtract {
                                           x: x,
                                           y: y,
                                           width: width,
                                           height: height,
                                       }))
        }
        (Interface::Subcompositor(object), 0) => {
            Ok(Request::SubcompositorDestroy(object, subcompositor::Destroy))
        }
        (Interface::Subcompositor(object), 1) => {
            let id_id_rust = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            let id = Subsurface(id_id_rust);
            let id = New(id);
            data = &data[1..];

            let surface = Surface(*up!(data.get(0)
                                           .ok_or(throw::Error::new(ReadError::Incomplete))));
            data = &data[1..];

            let parent = Surface(*up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))));
            data = &data[1..];

            Ok(Request::SubcompositorGetSubsurface(object,
                                                   subcompositor::GetSubsurface {
                                                       id: id,
                                                       surface: surface,
                                                       parent: parent,
                                                   }))
        }
        (Interface::Subsurface(object), 0) => {
            Ok(Request::SubsurfaceDestroy(object, subsurface::Destroy))
        }
        (Interface::Subsurface(object), 1) => {
            let x = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            let y = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            Ok(Request::SubsurfaceSetPosition(object, subsurface::SetPosition { x: x, y: y }))
        }
        (Interface::Subsurface(object), 2) => {
            let sibling = Surface(*up!(data.get(0)
                                           .ok_or(throw::Error::new(ReadError::Incomplete))));
            data = &data[1..];

            Ok(Request::SubsurfacePlaceAbove(object, subsurface::PlaceAbove { sibling: sibling }))
        }
        (Interface::Subsurface(object), 3) => {
            let sibling = Surface(*up!(data.get(0)
                                           .ok_or(throw::Error::new(ReadError::Incomplete))));
            data = &data[1..];

            Ok(Request::SubsurfacePlaceBelow(object, subsurface::PlaceBelow { sibling: sibling }))
        }
        (Interface::Subsurface(object), 4) => {
            Ok(Request::SubsurfaceSetSync(object, subsurface::SetSync))
        }
        (Interface::Subsurface(object), 5) => {
            Ok(Request::SubsurfaceSetDesync(object, subsurface::SetDesync))
        }
        _ => throw_new!(ReadError::UnkownMethod(object, header.opcode)),
    }
}

unsafe fn get_event<T: ObjectStore>(objects: &T,
                                    header: Header,
                                    mut data: &[u32],
                                    mut fds: &[fd])
                                    -> Result<Event, throw::Error<ReadError>> {
    // TODO: Gracefully handle broken clients
    let object = up!(objects.get(header.id)
                            .ok_or(throw::Error::new(ReadError::NoSuchObject)));
    match (object, header.opcode) {
        (Interface::Display(object), 0) => {
            // not an emum
            let object_id = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            // not an emum
            let code = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let len = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as usize;
            data = &data[1..];
            if data.len() * 4 < len {
                throw_new!(ReadError::Malformed);
            }
            let message = Array {
                contents: std::slice::from_raw_parts(data.as_ptr() as *const u8, len as usize)
                              .to_vec(),
            };
            data = &data[len / 4..];
            if len % 4 != 0 {
                data = &data[1..];
            };

            Ok(Event::DisplayError(object,
                                   display::Error {
                                       object_id: object_id,
                                       code: code,
                                       message: message,
                                   }))
        }
        (Interface::Display(object), 1) => {
            // not an emum
            let id = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            Ok(Event::DisplayDeleteId(object, display::DeleteId { id: id }))
        }
        (Interface::Registry(object), 0) => {
            // not an emum
            let name = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let len = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as usize;
            data = &data[1..];
            if data.len() * 4 < len {
                throw_new!(ReadError::Malformed);
            }
            let interface = Array {
                contents: std::slice::from_raw_parts(data.as_ptr() as *const u8, len as usize)
                              .to_vec(),
            };
            data = &data[len / 4..];
            if len % 4 != 0 {
                data = &data[1..];
            };

            // not an emum
            let version = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            Ok(Event::RegistryGlobal(object,
                                     registry::Global {
                                         name: name,
                                         interface: interface,
                                         version: version,
                                     }))
        }
        (Interface::Registry(object), 1) => {
            // not an emum
            let name = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            Ok(Event::RegistryGlobalRemove(object, registry::GlobalRemove { name: name }))
        }
        (Interface::Callback(object), 0) => {
            // not an emum
            let callback_data = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            Ok(Event::CallbackDone(object, callback::Done { callback_data: callback_data }))
        }
        (Interface::Shm(object), 0) => {
            // enum!
            let format = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let format = ::interfaces::shm::format(format);

            Ok(Event::ShmFormat(object, shm::Format { format: format }))
        }
        (Interface::Buffer(object), 0) => Ok(Event::BufferRelease(object, buffer::Release)),
        (Interface::DataOffer(object), 0) => {
            let len = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as usize;
            data = &data[1..];
            if data.len() * 4 < len {
                throw_new!(ReadError::Malformed);
            }
            let mime_type = Array {
                contents: std::slice::from_raw_parts(data.as_ptr() as *const u8, len as usize)
                              .to_vec(),
            };
            data = &data[len / 4..];
            if len % 4 != 0 {
                data = &data[1..];
            };

            Ok(Event::DataOfferOffer(object, data_offer::Offer { mime_type: mime_type }))
        }
        (Interface::DataOffer(object), 1) => {
            // not an emum
            let source_actions = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            Ok(Event::DataOfferSourceActions(object,
                                             data_offer::SourceActions {
                                                 source_actions: source_actions,
                                             }))
        }
        (Interface::DataOffer(object), 2) => {
            // not an emum
            let dnd_action = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            Ok(Event::DataOfferAction(object, data_offer::Action { dnd_action: dnd_action }))
        }
        (Interface::DataSource(object), 0) => {
            let len = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as usize;
            data = &data[1..];
            if data.len() * 4 < len {
                throw_new!(ReadError::Malformed);
            }
            let mime_type = Array {
                contents: std::slice::from_raw_parts(data.as_ptr() as *const u8, len as usize)
                              .to_vec(),
            };
            data = &data[len / 4..];
            if len % 4 != 0 {
                data = &data[1..];
            };

            Ok(Event::DataSourceTarget(object, data_source::Target { mime_type: mime_type }))
        }
        (Interface::DataSource(object), 1) => {
            let len = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as usize;
            data = &data[1..];
            if data.len() * 4 < len {
                throw_new!(ReadError::Malformed);
            }
            let mime_type = Array {
                contents: std::slice::from_raw_parts(data.as_ptr() as *const u8, len as usize)
                              .to_vec(),
            };
            data = &data[len / 4..];
            if len % 4 != 0 {
                data = &data[1..];
            };

            let fd = fds[0];
            fds = &fds[1..];

            Ok(Event::DataSourceSend(object,
                                     data_source::Send {
                                         mime_type: mime_type,
                                         fd: fd,
                                     }))
        }
        (Interface::DataSource(object), 2) => {
            Ok(Event::DataSourceCancelled(object, data_source::Cancelled))
        }
        (Interface::DataSource(object), 3) => {
            Ok(Event::DataSourceDndDropPerformed(object, data_source::DndDropPerformed))
        }
        (Interface::DataSource(object), 4) => {
            Ok(Event::DataSourceDndFinished(object, data_source::DndFinished))
        }
        (Interface::DataSource(object), 5) => {
            // not an emum
            let dnd_action = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            Ok(Event::DataSourceAction(object, data_source::Action { dnd_action: dnd_action }))
        }
        (Interface::DataDevice(object), 0) => {
            let id_id_rust = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            let id = DataOffer(id_id_rust);
            let id = New(id);
            data = &data[1..];

            Ok(Event::DataDeviceDataOffer(object, data_device::DataOffer { id: id }))
        }
        (Interface::DataDevice(object), 1) => {
            // not an emum
            let serial = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let surface = Surface(*up!(data.get(0)
                                           .ok_or(throw::Error::new(ReadError::Incomplete))));
            data = &data[1..];

            let x = unimplemented!(); /*fixed*/
            let y = unimplemented!(); /*fixed*/
            let id = DataOffer(*up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))));
            data = &data[1..];

            Ok(Event::DataDeviceEnter(object,
                                      data_device::Enter {
                                          serial: serial,
                                          surface: surface,
                                          x: x,
                                          y: y,
                                          id: id,
                                      }))
        }
        (Interface::DataDevice(object), 2) => {
            Ok(Event::DataDeviceLeave(object, data_device::Leave))
        }
        (Interface::DataDevice(object), 3) => {
            // not an emum
            let time = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let x = unimplemented!(); /*fixed*/
            let y = unimplemented!(); /*fixed*/
            Ok(Event::DataDeviceMotion(object,
                                       data_device::Motion {
                                           time: time,
                                           x: x,
                                           y: y,
                                       }))
        }
        (Interface::DataDevice(object), 4) => Ok(Event::DataDeviceDrop(object, data_device::Drop)),
        (Interface::DataDevice(object), 5) => {
            let id = DataOffer(*up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))));
            data = &data[1..];

            Ok(Event::DataDeviceSelection(object, data_device::Selection { id: id }))
        }
        (Interface::ShellSurface(object), 0) => {
            // not an emum
            let serial = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            Ok(Event::ShellSurfacePing(object, shell_surface::Ping { serial: serial }))
        }
        (Interface::ShellSurface(object), 1) => {
            // enum!
            let edges = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let edges = ::interfaces::shell_surface::resize(edges);

            let width = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            let height = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            Ok(Event::ShellSurfaceConfigure(object,
                                            shell_surface::Configure {
                                                edges: edges,
                                                width: width,
                                                height: height,
                                            }))
        }
        (Interface::ShellSurface(object), 2) => {
            Ok(Event::ShellSurfacePopupDone(object, shell_surface::PopupDone))
        }
        (Interface::Surface(object), 0) => {
            let output = Output(*up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))));
            data = &data[1..];

            Ok(Event::SurfaceEnter(object, surface::Enter { output: output }))
        }
        (Interface::Surface(object), 1) => {
            let output = Output(*up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))));
            data = &data[1..];

            Ok(Event::SurfaceLeave(object, surface::Leave { output: output }))
        }
        (Interface::Seat(object), 0) => {
            // enum!
            let capabilities = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let capabilities = ::interfaces::seat::capability(capabilities);

            Ok(Event::SeatCapabilities(object, seat::Capabilities { capabilities: capabilities }))
        }
        (Interface::Seat(object), 1) => {
            let len = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as usize;
            data = &data[1..];
            if data.len() * 4 < len {
                throw_new!(ReadError::Malformed);
            }
            let name = Array {
                contents: std::slice::from_raw_parts(data.as_ptr() as *const u8, len as usize)
                              .to_vec(),
            };
            data = &data[len / 4..];
            if len % 4 != 0 {
                data = &data[1..];
            };

            Ok(Event::SeatName(object, seat::Name { name: name }))
        }
        (Interface::Pointer(object), 0) => {
            // not an emum
            let serial = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let surface = Surface(*up!(data.get(0)
                                           .ok_or(throw::Error::new(ReadError::Incomplete))));
            data = &data[1..];

            let surface_x = unimplemented!(); /*fixed*/
            let surface_y = unimplemented!(); /*fixed*/
            Ok(Event::PointerEnter(object,
                                   pointer::Enter {
                                       serial: serial,
                                       surface: surface,
                                       surface_x: surface_x,
                                       surface_y: surface_y,
                                   }))
        }
        (Interface::Pointer(object), 1) => {
            // not an emum
            let serial = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let surface = Surface(*up!(data.get(0)
                                           .ok_or(throw::Error::new(ReadError::Incomplete))));
            data = &data[1..];

            Ok(Event::PointerLeave(object,
                                   pointer::Leave {
                                       serial: serial,
                                       surface: surface,
                                   }))
        }
        (Interface::Pointer(object), 2) => {
            // not an emum
            let time = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let surface_x = unimplemented!(); /*fixed*/
            let surface_y = unimplemented!(); /*fixed*/
            Ok(Event::PointerMotion(object,
                                    pointer::Motion {
                                        time: time,
                                        surface_x: surface_x,
                                        surface_y: surface_y,
                                    }))
        }
        (Interface::Pointer(object), 3) => {
            // not an emum
            let serial = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            // not an emum
            let time = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            // not an emum
            let button = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            // enum!
            let state = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let state = ::interfaces::pointer::button_state(state);

            Ok(Event::PointerButton(object,
                                    pointer::Button {
                                        serial: serial,
                                        time: time,
                                        button: button,
                                        state: state,
                                    }))
        }
        (Interface::Pointer(object), 4) => {
            // not an emum
            let time = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            // enum!
            let axis = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let axis = ::interfaces::pointer::axis(axis);

            let value = unimplemented!(); /*fixed*/
            Ok(Event::PointerAxis(object,
                                  pointer::Axis {
                                      time: time,
                                      axis: axis,
                                      value: value,
                                  }))
        }
        (Interface::Pointer(object), 5) => Ok(Event::PointerFrame(object, pointer::Frame)),
        (Interface::Pointer(object), 6) => {
            // enum!
            let axis_source = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let axis_source = ::interfaces::pointer::axis_source(axis_source);

            Ok(Event::PointerAxisSource(object, pointer::AxisSource { axis_source: axis_source }))
        }
        (Interface::Pointer(object), 7) => {
            // not an emum
            let time = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            // enum!
            let axis = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let axis = ::interfaces::pointer::axis(axis);

            Ok(Event::PointerAxisStop(object,
                                      pointer::AxisStop {
                                          time: time,
                                          axis: axis,
                                      }))
        }
        (Interface::Pointer(object), 8) => {
            // enum!
            let axis = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let axis = ::interfaces::pointer::axis(axis);

            let discrete = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            Ok(Event::PointerAxisDiscrete(object,
                                          pointer::AxisDiscrete {
                                              axis: axis,
                                              discrete: discrete,
                                          }))
        }
        (Interface::Keyboard(object), 0) => {
            // enum!
            let format = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let format = ::interfaces::keyboard::keymap_format(format);

            let fd = fds[0];
            fds = &fds[1..];

            // not an emum
            let size = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            Ok(Event::KeyboardKeymap(object,
                                     keyboard::Keymap {
                                         format: format,
                                         fd: fd,
                                         size: size,
                                     }))
        }
        (Interface::Keyboard(object), 1) => {
            // not an emum
            let serial = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let surface = Surface(*up!(data.get(0)
                                           .ok_or(throw::Error::new(ReadError::Incomplete))));
            data = &data[1..];

            let len = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as usize;
            data = &data[1..];
            if data.len() * 4 < len {
                throw_new!(ReadError::Malformed);
            }
            let keys = Array {
                contents: std::slice::from_raw_parts(data.as_ptr() as *const u8, len as usize)
                              .to_vec(),
            };
            data = &data[len / 4..];
            if len % 4 != 0 {
                data = &data[1..];
            };

            Ok(Event::KeyboardEnter(object,
                                    keyboard::Enter {
                                        serial: serial,
                                        surface: surface,
                                        keys: keys,
                                    }))
        }
        (Interface::Keyboard(object), 2) => {
            // not an emum
            let serial = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let surface = Surface(*up!(data.get(0)
                                           .ok_or(throw::Error::new(ReadError::Incomplete))));
            data = &data[1..];

            Ok(Event::KeyboardLeave(object,
                                    keyboard::Leave {
                                        serial: serial,
                                        surface: surface,
                                    }))
        }
        (Interface::Keyboard(object), 3) => {
            // not an emum
            let serial = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            // not an emum
            let time = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            // not an emum
            let key = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            // enum!
            let state = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let state = ::interfaces::keyboard::key_state(state);

            Ok(Event::KeyboardKey(object,
                                  keyboard::Key {
                                      serial: serial,
                                      time: time,
                                      key: key,
                                      state: state,
                                  }))
        }
        (Interface::Keyboard(object), 4) => {
            // not an emum
            let serial = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            // not an emum
            let mods_depressed = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            // not an emum
            let mods_latched = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            // not an emum
            let mods_locked = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            // not an emum
            let group = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            Ok(Event::KeyboardModifiers(object,
                                        keyboard::Modifiers {
                                            serial: serial,
                                            mods_depressed: mods_depressed,
                                            mods_latched: mods_latched,
                                            mods_locked: mods_locked,
                                            group: group,
                                        }))
        }
        (Interface::Keyboard(object), 5) => {
            let rate = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            let delay = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            Ok(Event::KeyboardRepeatInfo(object,
                                         keyboard::RepeatInfo {
                                             rate: rate,
                                             delay: delay,
                                         }))
        }
        (Interface::Touch(object), 0) => {
            // not an emum
            let serial = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            // not an emum
            let time = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let surface = Surface(*up!(data.get(0)
                                           .ok_or(throw::Error::new(ReadError::Incomplete))));
            data = &data[1..];

            let id = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            let x = unimplemented!(); /*fixed*/
            let y = unimplemented!(); /*fixed*/
            Ok(Event::TouchDown(object,
                                touch::Down {
                                    serial: serial,
                                    time: time,
                                    surface: surface,
                                    id: id,
                                    x: x,
                                    y: y,
                                }))
        }
        (Interface::Touch(object), 1) => {
            // not an emum
            let serial = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            // not an emum
            let time = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let id = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            Ok(Event::TouchUp(object,
                              touch::Up {
                                  serial: serial,
                                  time: time,
                                  id: id,
                              }))
        }
        (Interface::Touch(object), 2) => {
            // not an emum
            let time = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let id = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            let x = unimplemented!(); /*fixed*/
            let y = unimplemented!(); /*fixed*/
            Ok(Event::TouchMotion(object,
                                  touch::Motion {
                                      time: time,
                                      id: id,
                                      x: x,
                                      y: y,
                                  }))
        }
        (Interface::Touch(object), 3) => Ok(Event::TouchFrame(object, touch::Frame)),
        (Interface::Touch(object), 4) => Ok(Event::TouchCancel(object, touch::Cancel)),
        (Interface::Output(object), 0) => {
            let x = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            let y = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            let physical_width =
                *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            let physical_height =
                *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            // enum!
            let subpixel = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let subpixel = ::interfaces::output::subpixel(subpixel);

            let len = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as usize;
            data = &data[1..];
            if data.len() * 4 < len {
                throw_new!(ReadError::Malformed);
            }
            let make = Array {
                contents: std::slice::from_raw_parts(data.as_ptr() as *const u8, len as usize)
                              .to_vec(),
            };
            data = &data[len / 4..];
            if len % 4 != 0 {
                data = &data[1..];
            };

            let len = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as usize;
            data = &data[1..];
            if data.len() * 4 < len {
                throw_new!(ReadError::Malformed);
            }
            let model = Array {
                contents: std::slice::from_raw_parts(data.as_ptr() as *const u8, len as usize)
                              .to_vec(),
            };
            data = &data[len / 4..];
            if len % 4 != 0 {
                data = &data[1..];
            };

            // enum!
            let transform = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let transform = ::interfaces::output::transform(transform);

            Ok(Event::OutputGeometry(object,
                                     output::Geometry {
                                         x: x,
                                         y: y,
                                         physical_width: physical_width,
                                         physical_height: physical_height,
                                         subpixel: subpixel,
                                         make: make,
                                         model: model,
                                         transform: transform,
                                     }))
        }
        (Interface::Output(object), 1) => {
            // enum!
            let flags = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));
            data = &data[1..];

            let flags = ::interfaces::output::mode(flags);

            let width = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            let height = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            let refresh = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            Ok(Event::OutputMode(object,
                                 output::Mode {
                                     flags: flags,
                                     width: width,
                                     height: height,
                                     refresh: refresh,
                                 }))
        }
        (Interface::Output(object), 2) => Ok(Event::OutputDone(object, output::Done)),
        (Interface::Output(object), 3) => {
            let factor = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;
            data = &data[1..];

            Ok(Event::OutputScale(object, output::Scale { factor: factor }))
        }
        _ => throw_new!(ReadError::UnkownMethod(object, header.opcode)),
    }
}


impl<T: ObjectStore> Connection<T> {
    pub fn get_events(&mut self) -> Result<Events, GetMessagesError> {
        use nix::sys::uio::IoVec;
        use nix::sys::socket::{recvmsg, CmsgSpace, MsgFlags, ControlMessage};

        let mut ret_fds = vec![];


        // Borrowed from mio::sys::unix::uds;
        // (Then altered)

        // Buf shenanigans needed for alignment!
        let mut buf_32: [u32; 1024] = unsafe { std::mem::uninitialized() };
        let len = {
            let buf_8: &mut [u8; 4096] = unsafe { std::mem::transmute(&mut buf_32) };
            let buf: &mut [u8] = &mut *buf_8;


            let iov = [IoVec::from_mut_slice(buf)];
            let mut cmsgspace: CmsgSpace<[fd; 1024]> = CmsgSpace::new();
            let msg = try!(recvmsg(self.fd, &iov, Some(&mut cmsgspace), MsgFlags::empty())
                               .map_err(|x| GetMessagesError::Nix(x)));
            for cmsg in msg.cmsgs() {
                if let ControlMessage::ScmRights(fds) = cmsg {
                    ret_fds = fds.to_vec();
                }
            }

            if msg.bytes == 0 {
                return Err(GetMessagesError::Closed);
            }

            assert!(msg.bytes % 4 == 0);
            msg.bytes / 4
        };

        let data = buf_32[..len].to_vec();

        // Debugging
        print!("Recieved[ ");
        for i in &data {
            print!("{:0>8x} ", i);
        }
        println!("]");

        Ok(Events {
            data: data,
            fds: ret_fds,
            start: 0,
        })
    }
}
/// Iterator over requests in read data
pub struct Events {
    data: Vec<u32>,
    fds: Vec<fd>,
    start: usize,
}

impl Events {
    pub fn next<T: ObjectStore>(&mut self,
                                objects: &T)
                                -> Option<Result<Event, throw::Error<ReadError>>> {
        if self.start == self.data.len() {
            return None;
        }

        if self.data.len() < self.start + 2 {
            return Some(err!(ReadError::Incomplete));
        }

        let data = &self.data[self.start..];
        let header = unsafe { *(data.as_ptr() as *const Header) };

        if header.size % 4 != 0 {
            return Some(err!(ReadError::Malformed));
        }

        let len = header.size as usize / 4;

        if self.start + len > self.data.len() {
            return Some(err!(ReadError::Incomplete));
        }

        // Debugging
        print!("data[ ");
        for x in &data[..len] {
            print!("{:x} ", x);
        }
        println!("]");


        self.start += len;
        Some(unsafe { get_event(objects, header, &data[2..len], &self.fds) })
    }
}

impl<T: ObjectStore> Connection<T> {
    pub fn get_requests(&mut self) -> Result<Requests, GetMessagesError> {
        use nix::sys::uio::IoVec;
        use nix::sys::socket::{recvmsg, CmsgSpace, MsgFlags, ControlMessage};

        let mut ret_fds = vec![];


        // Borrowed from mio::sys::unix::uds;
        // (Then altered)

        // Buf shenanigans needed for alignment!
        let mut buf_32: [u32; 1024] = unsafe { std::mem::uninitialized() };
        let len = {
            let buf_8: &mut [u8; 4096] = unsafe { std::mem::transmute(&mut buf_32) };
            let buf: &mut [u8] = &mut *buf_8;


            let iov = [IoVec::from_mut_slice(buf)];
            let mut cmsgspace: CmsgSpace<[fd; 1024]> = CmsgSpace::new();
            let msg = try!(recvmsg(self.fd, &iov, Some(&mut cmsgspace), MsgFlags::empty())
                               .map_err(|x| GetMessagesError::Nix(x)));
            for cmsg in msg.cmsgs() {
                if let ControlMessage::ScmRights(fds) = cmsg {
                    ret_fds = fds.to_vec();
                }
            }

            if msg.bytes == 0 {
                return Err(GetMessagesError::Closed);
            }

            assert!(msg.bytes % 4 == 0);
            msg.bytes / 4
        };

        let data = buf_32[..len].to_vec();

        // Debugging
        print!("Recieved[ ");
        for i in &data {
            print!("{:0>8x} ", i);
        }
        println!("]");

        Ok(Requests {
            data: data,
            fds: ret_fds,
            start: 0,
        })
    }
}
/// Iterator over requests in read data
pub struct Requests {
    data: Vec<u32>,
    fds: Vec<fd>,
    start: usize,
}

impl Requests {
    pub fn next<T: ObjectStore>(&mut self,
                                objects: &T)
                                -> Option<Result<Request, throw::Error<ReadError>>> {
        if self.start == self.data.len() {
            return None;
        }

        if self.data.len() < self.start + 2 {
            return Some(err!(ReadError::Incomplete));
        }

        let data = &self.data[self.start..];
        let header = unsafe { *(data.as_ptr() as *const Header) };

        if header.size % 4 != 0 {
            return Some(err!(ReadError::Malformed));
        }

        let len = header.size as usize / 4;

        if self.start + len > self.data.len() {
            return Some(err!(ReadError::Incomplete));
        }

        // Debugging
        print!("data[ ");
        for x in &data[..len] {
            print!("{:x} ", x);
        }
        println!("]");


        self.start += len;
        Some(unsafe { get_request(objects, header, &data[2..len], &self.fds) })
    }
}
